# Mathezirkel App Client - notes on Docker

## Contanier for Version validation

Build and push the container

```cmd
docker build . --no-cache -t registry.gitlab.com/mathezirkel/mathezirkel-app/client/mathezirkel-app-client-version-check -f docker/version/Dockerfile
docker login registry.gitlab.com
docker push registry.gitlab.com/mathezirkel/mathezirkel-app/client/mathezirkel-app-client-version-check
```

(If using 2FA, you need a Token to log in. The token needs `write_api` and `write_registry` access.)
