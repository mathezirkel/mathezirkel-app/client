# Mathezirkel App Client

[![pipeline status](https://gitlab.com/mathezirkel/mathezirkel-app/client/badges/main/pipeline.svg)](https://gitlab.com/mathezirkel/mathezirkel-app/client/-/commits/main)
[![latest tag](https://img.shields.io/gitlab/v/tag/mathezirkel/mathezirkel-app/client?label=version)](https://gitlab.com/mathezirkel/mathezirkel-app/client/-/tags)

<!-- [![coverage report](https://gitlab.com/mathezirkel/mathezirkel-app/client/badges/main/coverage.svg)](https://gitlab.com/mathezirkel/mathezirkel-app/client/badges/-/commits/main) -->

This library is the backend for all future scripts and GUI tools related to the organization of the `Mathezirkel Augsburg` and the `Mathecamp`.

## Tech Stack and hosted Version

Frontend is configured with [Vue](https://v3.vuejs.org/) built by [Vite](https://vitejs.dev/) with [VUETIFY](https://vuetifyjs.com/) and [Material Icons](https://fonts.google.com/icons).

Since the decommissioning of the native app, the application solely works as a [PWA](https://web.dev/explore/progressive-web-apps).
The PWA can be accessed directly HERE: [https://app.mathezirkel-augsburg.de](https://app.mathezirkel-augsburg.de).

![QR](qr.png)

As the project used to be written in [Tauri](https://tauri.app/) which provides [cross-plattform-building scripts and support](https://tauri.app/v1/guides/building/cross-platform) only for GitHub actions, this repository leveraged the convenient and powerful GitHub-Actions capabilities to build the project for all the platforms and release the compiled results.
As the project moved to PWA-only as of now, it is hosted only on GitLab-Pages.

## Build the PWA Version

Requirements:

-   `docker` and `docker compose` installed [See here](https://docs.docker.com/desktop/install/ubuntu/).
-   More Docker-Documentation [Here](./DOCKER.md) for version checker scripts.
-   Created the file `generated.json` of the exports locally, see [here]([./exports/](https://gitlab.com/mathezirkel/mathezirkel-app/exports/-/blob/main/README.md)) for details.

Run the dev version of the PWA:

```shell
docker compose up
```

Make sure

## Seed with info for local testing

```cmd
docker compose exec vite_docker npm run seed
```

## Graphql schema synchronization

When tha backend Graphql-API changes, make sure to update the frontend schema (requires the backend repo to be running in dev-mode, where Bearer-Authentication with `{"Authorization": "Bearer development_token"}` is allowed.)
Because of this, it is too complicated to be done in the pipeline and requires manual running and committing of the schema to version control.

```cmd
npm run codegen
```

## Release

A new version can be released by adding a Git tag of format like `v1.2.3` and pushing to Gitlab with tags.

```cmd
git tag -a vX.X.X -m "<Things that changed this release.>"
git push && git push --tags
```

Make sure to correctly set the version string in the following file: `/package.json` and `/package-lock.json` (gets updated automatically when running any npm command like `npm install`).

## License

The client app is published under GPL 3, see [LICENSE](LICENSE).

<!--
Icons:
https://tools.crawlink.com/tools/pwa-icon-generator/


Favicons:
https://realfavicongenerator.net/
 -->
