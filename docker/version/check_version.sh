#!/bin/bash

if [[ "$VERSION_NODE" == "$VERSION_NODE_LOCK" ]]; then
    echo "All code versions are the same: $VERSION_NODE. Just make sure to set the next tag also to this value."
else
    echo "Versions mismatch. Make sure to set the correct version in 'package.json' and 'package-lock.json'"
    exit 1
fi

if [ $VERSION_TAG ]
then
    # a version tag is set
    echo "Version Tag provided. Comparing"

    if [ "$VERSION_NODE" == "$VERSION_TAG" ]; then
        echo "All code versions are the same as Tag version: $VERSION_NODE"
        exit 0
    else
        echo "Versions mismatch. Make sure to set the tag name to the same version as the code ('package.json', 'package-lock.json')"
        exit 1
    fi

else
    # no version tag is set
    echo "No version Tag provided, analyze only the versions set in the repo"
    exit 0

fi