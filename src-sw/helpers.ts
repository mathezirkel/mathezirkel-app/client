import { readFromIndexedDB, writeToIndexedDB } from "./database";

export async function probeBackendForToken(backendBaseURL: string, token: string) {
    const probeURL = backendBaseURL + "redeem_hash?token=" + token;
    let success = false;
    let bearer = "";
    let refresh_token = null as null | string;
    let response = await fetch(probeURL).catch((err) => {
        console.error("Service worker request failed", err);
    });

    if (response) {
        if (response.status == 200) {
            const json = await response.json();

            if (json.success) {
                success = true;
                bearer = json.token;

                if (json.refresh_token && json.refresh_token != null && json.refresh_token != undefined) {
                    refresh_token = json.refresh_token;
                }
            }
        } else {
            console.error("Service worker received unexpected response", response);
        }
    }

    return {
        success,
        bearer,
        refresh_token,
    };
}

export function sleep(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

export async function answerClient(event: any, key: string, payload: any) {
    const id = (event.source as any).id;
    const client = await (self as any).clients.get(id);
    client?.postMessage({ key, payload });
}

export async function attemptSwappingTokensRefresh() {
    let backendBaseURL = "";
    try {
        backendBaseURL = await readFromIndexedDB("refresh_token_backend_base_url");
    } catch (error) {
        console.error("No backend base url set but attempted refresh");
    }
    let refreshToken = "";
    try {
        refreshToken = await readFromIndexedDB("refresh_token");
    } catch (error) {
        console.error("No refresh token set but attempted refresh");
    }

    if (refreshToken != "" && backendBaseURL != "") {
        console.log("Attempting to refresh token");

        const refreshURL = backendBaseURL + "refresh_token?refresh_token=" + refreshToken;

        let bearer = null as null | string;
        let refresh_token = null as null | string;
        let response = await fetch(refreshURL).catch((err) => {
            console.error("Service worker request for token refresh failed", err);
        });

        if (response) {
            if (response.status == 200) {
                const json = await response.json();

                if (json.success) {
                    bearer = json.token;

                    if (json.refresh_token && json.refresh_token != null && json.refresh_token != undefined) {
                        refresh_token = json.refresh_token;
                    }
                }
            } else {
                console.error("Service worker request for token refresh received unexpected response", response);
            }
        }

        if (bearer != null && bearer != "") {
            console.log("Refreshed Bearer token!!");
            await writeToIndexedDB("token", bearer);
            await writeToIndexedDB("last_bearer_update", String(Date.now()));
            if (refresh_token != null) {
                console.log("Refreshed Refresh Token!!");
                await writeToIndexedDB("refresh_token", refresh_token);
            }
        }
    } else {
        console.log("Refresh not possible: probably not logged in");
    }
}
