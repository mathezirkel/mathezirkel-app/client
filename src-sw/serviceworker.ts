import { cryptographicallyRandomToken, hashSHA256 } from "./hashing";
import { writeToIndexedDB, readFromIndexedDB } from "./database";
import { answerClient, probeBackendForToken, sleep, attemptSwappingTokensRefresh } from "./helpers";

/// <reference no-default-lib="true"/>
/// <reference lib="esnext" />
/// <reference lib="webworker" />

// This code executes in its own worker or thread
self.addEventListener("install", () => {
    console.log("Service worker installed");

    // on update instantly replace
    (self as any).skipWaiting();
});
self.addEventListener("activate", async () => {
    console.log("Service worker activated");

    // write at least once to make sure the database objects exist
    await writeToIndexedDB("extrakey", "");
});

self.addEventListener("message", async (event) => {
    switch ((event as any).data.key) {
        case "request_token":
            const backendBaseURL = (event as any).data.payload.backendBaseURL;
            const secondsToWaitForToken = (event as any).data.payload.secondsToWaitForToken;

            console.log("Service Worker was asked to request a token");

            const token = cryptographicallyRandomToken(64);
            const hash = await hashSHA256(token);

            await writeToIndexedDB("currently_probing_token_hash", hash);

            const requestURL = backendBaseURL + "request_token?hash=" + hash;
            await answerClient(event, "show-hash", { hash: hash, requestURL: requestURL });
            await answerClient(event, "open-window", requestURL);

            let bearer = null as string | null;
            for (let index = 0; index < secondsToWaitForToken; index++) {
                const tokenTest = await readFromIndexedDB("currently_probing_token_hash");
                if (tokenTest != hash) {
                    console.log("Aborted probing because newer request");
                    break;
                }
                await sleep(1000);

                console.log("Try Probing Backend for Token");
                const tryProbe = await probeBackendForToken(backendBaseURL, token);
                if (tryProbe.success) {
                    console.log("Successful gotten bearer from backend");
                    bearer = tryProbe.bearer;

                    // store refresh token for later
                    if (tryProbe.refresh_token) {
                        await writeToIndexedDB("refresh_token", tryProbe.refresh_token);
                        await writeToIndexedDB("refresh_token_backend_base_url", backendBaseURL);
                    } else {
                        await writeToIndexedDB("refresh_token", "");
                        await writeToIndexedDB("refresh_token_backend_base_url", "");
                    }
                    break;
                }
            }

            if (bearer != null) {
                // store token
                await writeToIndexedDB("last_bearer_update", String(Date.now()));
                await writeToIndexedDB("token", bearer);

                console.log("Service Worker notifies client of new token");
                await answerClient(event, "new-bearer-token", {});
            } else {
                console.error("Probing for token failed: Timeout or backend didn't return");
            }

            break;
        case "get-bearer":
            let readToken = "EMPTYREPLACEMENTTOKEN"; // if the header is malformed, because empty string on live SOMETIMES the Extractor fails spectacularily....
            // get token
            try {
                readToken = await readFromIndexedDB("token");
            } catch (error) {
                await writeToIndexedDB("token", "EMPTYREPLACEMENTTOKEN"); // if never written, getting fails
            }

            // give to client tabs
            await answerClient(event, "current-bearer-token", readToken);

            break;
        case "trigger-logout":
            await writeToIndexedDB("token", "EMPTYREPLACEMENTTOKEN");
            await writeToIndexedDB("refresh_token", "");

            await answerClient(event, "handle-logout", {});

            break;
        case "_vue-devtools-send-message":
            break;
        default:
            // deactivated, as browser extension communication spams console
            // console.log("Service Worker got unparsable request message", event);
            break;
    }
});

setInterval(async () => {
    console.log("Token refreshing mechanism engaged");

    let lastBearerUpdate = Date.now() + 1000000000;
    try {
        lastBearerUpdate = parseInt(await readFromIndexedDB("last_bearer_update"));
    } catch (error) {
        console.error("No Bearer Timestamp", error);
    }

    if (lastBearerUpdate < Date.now() - 1000 * 60 * 4) {
        // all 5 minutes
        console.log("attempt to refresh token");
        await attemptSwappingTokensRefresh();
    }
}, 5000);

export default import.meta.url;
