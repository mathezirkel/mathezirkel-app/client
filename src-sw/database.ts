// INDEX DB IO IMPLEMENTATION

const indexDbVersion = 1;
const indexDbName = "TokenDatabase";
const indexDbObjectStoreName = "TokenObjectStore";

// Function to store a string in IndexedDB
export function writeToIndexedDB(key: string, value: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        const request = indexedDB.open(indexDbName, indexDbVersion);

        request.onupgradeneeded = function () {
            const db = request.result;
            db.createObjectStore(indexDbObjectStoreName, { keyPath: "id" });
        };

        request.onsuccess = function () {
            const db = request.result;
            const transaction = db.transaction([indexDbObjectStoreName], "readwrite");
            const objectStore = transaction.objectStore(indexDbObjectStoreName);

            const putRequest = objectStore.put({ id: key, text: value });
            putRequest.onsuccess = function () {
                resolve();
            };
            putRequest.onerror = function () {
                reject("Error storing value in IndexedDB");
            };
        };

        request.onerror = function () {
            reject("Error opening IndexedDB");
        };
    });
}

// Function to read a string from IndexedDB
export function readFromIndexedDB(key: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        const request = indexedDB.open(indexDbName, indexDbVersion);

        request.onsuccess = function () {
            const db = request.result;
            const transaction = db.transaction([indexDbObjectStoreName], "readonly");
            const objectStore = transaction.objectStore(indexDbObjectStoreName);

            const getRequest = objectStore.get(key);
            getRequest.onsuccess = function (event) {
                const data = (event.target as any).result;
                if (data) {
                    resolve(data.text);
                } else {
                    reject("Key not found in IndexedDB");
                }
            };
        };

        request.onerror = function () {
            reject("Error opening IndexedDB");
        };
    });
}
// INDEX DB IO IMPLEMENTATION
