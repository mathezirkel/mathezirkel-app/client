// HASH CRYPTOGRAPHY IMPLEMENTATION

// @ts-ignore
export function cryptographicallyRandomToken(length: number): string {
    const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    const array = new Uint32Array(length);
    crypto.getRandomValues(array);

    const randomChars = Array.from(array, (num) => charset[num % charset.length]);
    return randomChars.join("");
}

// @ts-ignore
export async function hashSHA256(input: string): Promise<string> {
    var enc = new TextEncoder();
    const hash = await crypto.subtle.digest("SHA-256", enc.encode(input));
    const arr = new Uint8Array(hash);
    let hexString = "";
    for (let i = 0; i < arr.length; i++) {
        let hex = arr[i].toString(16);
        hex = hex.length === 1 ? "0" + hex : hex; // pad single digit hex values with 0
        hexString += hex;
    }
    return hexString;
}

// HASH CRYPTOGRAPHY IMPLEMENTATION
