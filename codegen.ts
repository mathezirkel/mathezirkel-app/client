import type { CodegenConfig } from "@graphql-codegen/cli";

const config: CodegenConfig = {
    overwrite: true,
    schema: [
        {
            "http://localhost:8080/graphql": {
                headers: {
                    Authorization: "Bearer development_token",
                },
            },
        },
    ],
    documents: "src/**/*.{vue,ts}",
    generates: {
        "src/gql/": {
            preset: "client",
        },
        "./graphql.schema.json": {
            plugins: ["introspection"],
        },
    },
};

export default config;
