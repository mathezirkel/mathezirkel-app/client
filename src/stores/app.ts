import { defineStore } from "pinia";
import { nextTick, ref, watch } from "vue";

export default defineStore("app", () => {
    const darkMode = ref(localStorage.getItem("dark") == "true");
    const sidebarCollapsed = ref(localStorage.getItem("collapsed") == "true");
    const renderSidebar = ref(true);

    watch(darkMode, () => {
        localStorage.setItem("dark", String(darkMode.value));
    });
    watch(sidebarCollapsed, () => {
        localStorage.setItem("collapsed", String(sidebarCollapsed.value));
    });

    function triggerRerenderSidebar() {
        renderSidebar.value = false;
        nextTick(() => {
            renderSidebar.value = true;
        });
    }

    return {
        triggerRerenderSidebar,
        darkMode,
        sidebarCollapsed,
        renderSidebar,
    };
});
