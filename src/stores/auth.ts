import { defineStore } from "pinia";
import { User, checkAuthenticated } from "./../functions/check-auth";
import { getBearer, sendLogoutRequest } from "./../functions/bearer-auth";
import { computed, ref, watch } from "vue";
import { useRouter } from "vue-router";

export default defineStore("auth", () => {
    const authenticated = ref(false);
    const user = ref(undefined as User | undefined);
    const token = ref("EMPTYREPLACEMENTTOKEN" as string);
    const redirectHrefCache = ref(null as null | string);
    const router = useRouter();

    async function updateAuth(): Promise<boolean> {
        token.value = await getBearer();

        let res = [false, undefined] as [boolean, User | undefined];

        // only query if possibly successful
        if (token.value != "EMPTYREPLACEMENTTOKEN") {
            res = await checkAuthenticated();
        }

        if (res[0] && res[1] != null && res[1] != undefined) {
            if (authenticated.value == false) {
                // just unauthenticated, but now authenticated
                console.log("Fresh authentication");

                if (redirectHrefCache.value != null) {
                    const target = redirectHrefCache.value;
                    console.log("Redirecting after fresh authentication to " + target);

                    redirectHrefCache.value = null;
                    if (target == "/") {
                        // default navigate to home only after short delay
                        setTimeout(() => {
                            router.push(target);
                        }, 1000);
                    } else {
                        router.push(target);
                    }
                }
            }

            authenticated.value = true;
            user.value = res[1];
            return true;
        } else {
            authenticated.value = false;
            user.value = undefined;

            if (token.value != "EMPTYREPLACEMENTTOKEN") {
                // failiure, even though there is a token. make sure to trigger logout
                sendLogoutRequest();
            }

            return false;
        }
    }

    const hasNotForfeitWriteParticipantStorageKey = "has_not_forfeit_hasWriteParticipant";
    const hasNotForfeitWriteInstructorStorageKey = "has_not_forfeit_hasWriteInstructor";
    const hasNotForfeitDeleteRecordsStorageKey = "has_not_forfeit_hasDeleteRecords";
    const hasNotForfeitExportStorageKey = "has_not_forfeit_hasExport";
    const hasNotForfeitManageSignupStorageKey = "has_not_forfeit_hasManageSignup";
    const hasNotForfeitWriteEventsStorageKey = "has_not_forfeit_hasWriteEvents";
    const hasNotForfeitManageZirkelPlanStorageKey = "has_not_forfeit_hasManageZirkelPlan";
    const hasForfeitDistributeFractivitiesStorageKey = "has_not_forfeit_hasDistributeFractivities";
    const hasNotForfeitWriteParticipant = ref(
        (localStorage.getItem(hasNotForfeitWriteParticipantStorageKey) ?? "true") == "true"
    );
    const hasNotForfeitWriteInstructor = ref((localStorage.getItem(hasNotForfeitWriteInstructorStorageKey) ?? "true") == "true");
    const hasNotForfeitDeleteRecords = ref((localStorage.getItem(hasNotForfeitDeleteRecordsStorageKey) ?? "true") == "true");
    const hasNotForfeitExport = ref((localStorage.getItem(hasNotForfeitExportStorageKey) ?? "true") == "true");
    const hasNotForfeitManageSignup = ref((localStorage.getItem(hasNotForfeitManageSignupStorageKey) ?? "true") == "true");
    const hasNotForfeitWriteEvents = ref((localStorage.getItem(hasNotForfeitWriteEventsStorageKey) ?? "true") == "true");
    const hasNotForfeitManageZirkelPlan = ref(
        (localStorage.getItem(hasNotForfeitManageZirkelPlanStorageKey) ?? "true") == "true"
    );
    const hasForfeitDistributeFractivities = ref(
        (localStorage.getItem(hasForfeitDistributeFractivitiesStorageKey) ?? "true") == "true"
    );
    watch(hasNotForfeitWriteParticipant, () => {
        localStorage.setItem(hasNotForfeitWriteParticipantStorageKey, hasNotForfeitWriteParticipant.value ? "true" : "false");
    });
    watch(hasNotForfeitWriteInstructor, () => {
        localStorage.setItem(hasNotForfeitWriteInstructorStorageKey, hasNotForfeitWriteInstructor.value ? "true" : "false");
    });
    watch(hasNotForfeitDeleteRecords, () => {
        localStorage.setItem(hasNotForfeitDeleteRecordsStorageKey, hasNotForfeitDeleteRecords.value ? "true" : "false");
    });
    watch(hasNotForfeitExport, () => {
        localStorage.setItem(hasNotForfeitExportStorageKey, hasNotForfeitExport.value ? "true" : "false");
    });
    watch(hasNotForfeitManageSignup, () => {
        localStorage.setItem(hasNotForfeitManageSignupStorageKey, hasNotForfeitManageSignup.value ? "true" : "false");
    });
    watch(hasNotForfeitWriteEvents, () => {
        localStorage.setItem(hasNotForfeitWriteEventsStorageKey, hasNotForfeitWriteEvents.value ? "true" : "false");
    });
    watch(hasNotForfeitManageZirkelPlan, () => {
        localStorage.setItem(hasNotForfeitManageZirkelPlanStorageKey, hasNotForfeitManageZirkelPlan.value ? "true" : "false");
    });
    watch(hasForfeitDistributeFractivities, () => {
        localStorage.setItem(
            hasForfeitDistributeFractivitiesStorageKey,
            hasForfeitDistributeFractivities.value ? "true" : "false"
        );
    });
    const hasNotForfeitDistributeFractivities = computed({
        get: () => {
            return !hasForfeitDistributeFractivities.value;
        },
        set: (val) => {
            hasForfeitDistributeFractivities.value = !val;
        },
    });

    function hasRoleString(role: string) {
        return (user.value ?? false) && user.value != undefined && user.value.roles.includes(role);
    }

    const hasReadParticipant = computed(() => {
        return hasRoleString("readparticipant");
    });
    const hasWriteParticipant = computed(() => {
        return hasRoleString("writeparticipant") && hasNotForfeitWriteParticipant.value;
    });
    const hasReadInstructor = computed(() => {
        return hasRoleString("readinstructor");
    });
    const hasWriteInstructor = computed(() => {
        return hasRoleString("writeinstructor") && hasNotForfeitWriteInstructor.value;
    });
    const hasDeleteRecords = computed(() => {
        return hasRoleString("deleterecords") && hasNotForfeitDeleteRecords.value;
    });
    const hasExport = computed(() => {
        return hasRoleString("export") && hasNotForfeitExport.value;
    });
    const hasManageSignup = computed(() => {
        return hasRoleString("managesignup") && hasNotForfeitManageSignup.value;
    });
    const hasWriteEvents = computed(() => {
        return hasRoleString("writeevents") && hasNotForfeitWriteEvents.value;
    });
    const hasManageZirkelPlan = computed(() => {
        return hasRoleString("managezirkelplan") && hasNotForfeitManageZirkelPlan.value;
    });
    const hasDistributeFractivities = computed(() => {
        return !hasForfeitDistributeFractivities.value;
    });
    const ownInstructorUuid = computed(() => {
        return user.value && user.value != undefined ? user.value.instructor_uuid : "";
    });
    const hasSomeAdvancedAdminRights = computed(() => {
        return (
            hasManageZirkelPlan.value ||
            hasDeleteRecords.value ||
            hasExport.value ||
            hasWriteEvents.value ||
            hasWriteInstructor.value ||
            hasWriteParticipant.value
        );
    });

    return {
        hasRoleString,
        user,
        authenticated,
        updateAuth,
        token,
        hasReadParticipant,
        hasWriteParticipant,
        hasReadInstructor,
        hasWriteInstructor,
        hasDeleteRecords,
        hasExport,
        hasManageSignup,
        hasWriteEvents,
        hasManageZirkelPlan,
        hasDistributeFractivities,
        ownInstructorUuid,
        hasSomeAdvancedAdminRights,
        hasNotForfeitWriteParticipant,
        hasNotForfeitWriteInstructor,
        hasNotForfeitDeleteRecords,
        hasNotForfeitExport,
        hasNotForfeitManageSignup,
        hasNotForfeitWriteEvents,
        hasNotForfeitManageZirkelPlan,
        hasNotForfeitDistributeFractivities,
        redirectHrefCache,
    };
});
