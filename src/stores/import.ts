import { defineStore } from "pinia";
import { computed, nextTick, ref, watch } from "vue";
import { success } from "../plugins/toast";
import { NutritionEnum, SexEnum, ClassyearEnum, TraveltypeEnum } from "../gql/graphql";

export interface ImportInterface {
    familyName?: string;
    givenName?: string;
    callName?: string;
    birthDate?: string; // yyyy-mm-dd
    sex?: SexEnum; // "MALE", "FEMALE", "DIVERSE"
    gender?: string;
    street?: string;
    streetNumber?: string;
    postalCode?: string;
    city?: string;
    country?: string;
    emailSelf?: string;
    additionalEmails?: string[];
    telephone?: string;
    classYear?: ClassyearEnum; // "CLASS5", "CLASS6", "CLASS7", "CLASS8", "CLASS9", "CLASS10", "CLASS11", "CLASS12", "CLASS13", "CLASS_INFO"
    contacts?: {
        name?: string;
        telephone?: string;
    }[];
    nutrition?: NutritionEnum; // "OMNIVORE", "VEGAN", "VEGETARIAN"
    foodRestriction?: string;
    arrival?: TraveltypeEnum; // "BUS", "PRIVATE", "BICYCLE"
    arrivalNotes?: string;
    departure?: TraveltypeEnum; // "BUS", "PRIVATE", "BICYCLE"
    departureNotes?: string;
    roomPartnerWishes?: string[];
    zirkelPartnerWishes?: string[];
    medicalNotes?: string[];
    topicWishes?: string[];
    fractivityWishes?: string[];
    leavingPremise?: boolean;
    carpoolDataSharing?: boolean;
    removeTicks?: boolean;
    instruments?: string[];
    pool?: boolean;
    registrationTimestamp?: string; // TIMESTAMP like "2023-02-02 17:25:33"
    certificate?: boolean;
    participates?: boolean; // at the closing event
}

export type AutofillState = "equal" | "different" | "insertable";
type AutofillInstance = {
    insertCallback: () => void;
    updateCallback: (howManyRecent: number) => void;
    state: AutofillState;
    fieldForTab: string;
    updateShouldBeAlwaysCalled: boolean;
};

export default defineStore("import", () => {
    const inputCache = ref(null as string | null);
    const autofillTracker = ref(
        {} as {
            [key: string]: AutofillInstance;
        }
    );

    const clearedKeys = ref([] as string[]);
    const clearedArrayEntries = ref({} as { [key: string]: string[] });

    function setInput(input: string) {
        inputCache.value = input;
    }
    function clearInput() {
        inputCache.value = null;
        clearedKeys.value = [];
        clearedArrayEntries.value = {};
    }

    function registerAutofillTracker(uuid: string, instance: AutofillInstance) {
        autofillTracker.value[uuid] = instance;
    }
    function unregisterAutofillTracker(uuid: string) {
        delete autofillTracker.value[uuid];
    }
    function updateAutofillTrackerState(uuid: string, state: AutofillState) {
        if (autofillTracker.value[uuid] != undefined) {
            autofillTracker.value[uuid].state = state;
        }
    }
    function insertAllInsertable(forTab: string) {
        let lastUpdate: (num: number) => void = (_a) => {};
        let alwaysUpdate: ((num: number) => void)[] = [];

        let count = 0;
        for (const instance of Object.values(autofillTracker.value)) {
            if (instance.state == "insertable" && instance.fieldForTab == forTab) {
                count++;
                instance.insertCallback();

                if (instance.updateShouldBeAlwaysCalled) {
                    alwaysUpdate.push(instance.updateCallback);
                } else {
                    lastUpdate = (num) => instance.updateCallback(num);
                }
            }
        }

        // call updates at the end
        // wait for the changes of selects to have propagated.
        nextTick(() => {
            setTimeout(() => {
                lastUpdate(count);
                alwaysUpdate.forEach((updateFunction) => {
                    updateFunction(1);
                });
            }, 300);
        });
    }

    const importedStructure = computed((): ImportInterface | null => {
        if (inputCache.value != null) {
            const parsedObject = JSON.parse(inputCache.value);

            if (parsedObject != null && parsedObject != undefined) {
                return parsedObject as ImportInterface;
            }
        }
        return null;
    });

    watch(importedStructure, () => {
        if (importedStructure.value != null) {
            success("Neue Importdatei eingelesen");
        }
    });

    // explicit exports for binding into hash-match stuff
    const birthDate = computed((): string => {
        if (importedStructure.value == null) {
            return "0001-01-01";
        } else {
            importedStructure.value.birthDate; // cache busting
            return getParsedValueOrDefault("birthDate");
        }
    });
    const callName = computed((): string => {
        if (importedStructure.value == null) {
            return "";
        } else {
            importedStructure.value.callName; // cache busting
            return getParsedValueOrDefault("callName");
        }
    });
    const familyName = computed((): string => {
        if (importedStructure.value == null) {
            return "";
        } else {
            importedStructure.value.familyName; // cache busting
            return getParsedValueOrDefault("familyName");
        }
    });
    const givenName = computed((): string => {
        if (importedStructure.value == null) {
            return "";
        } else {
            importedStructure.value.givenName; // cache busting
            return getParsedValueOrDefault("givenName");
        }
    });

    function getParsedValueOrDefault(key: string): any {
        switch (key) {
            case "birthDate":
                return importedStructure?.value?.birthDate ?? "0001-01-01";
            case "callName":
                return importedStructure?.value?.callName ?? "";
            case "familyName":
                return importedStructure?.value?.familyName ?? "";
            case "givenName":
                return importedStructure?.value?.givenName ?? "";
            case "sex":
                return importedStructure?.value?.sex ?? "REDACTED";
            case "gender":
                return importedStructure?.value?.gender ?? "";
            case "street":
                return importedStructure?.value?.street ?? "";
            case "streetNumber":
                return importedStructure?.value?.streetNumber ?? "";
            case "postalCode":
                return importedStructure?.value?.postalCode ?? "";
            case "city":
                return importedStructure?.value?.city ?? "";
            case "country":
                return importedStructure?.value?.country ?? "Deutschland";
            case "emailSelf":
                return importedStructure?.value?.emailSelf ?? "";
            case "additionalEmails":
                return importedStructure?.value?.additionalEmails ?? [];
            case "telephone":
                return importedStructure?.value?.telephone ?? "";
            case "classYear":
                return importedStructure?.value?.classYear ?? "CLASS_INFO";
            case "contacts":
                const contactsImport = importedStructure?.value?.contacts ?? [];
                let out = [] as { name: string; telephone: string }[];
                contactsImport.forEach((contact) => {
                    if (
                        contact.name != undefined &&
                        contact.telephone != undefined &&
                        (contact.name ?? "") != "" &&
                        (contact.telephone ?? "") != ""
                    ) {
                        out.push({
                            name: contact.name ?? "",
                            telephone: contact.telephone ?? "",
                        });
                    }
                });
                return out;
            case "nutrition":
                return importedStructure?.value?.nutrition ?? "REDACTED";
            case "foodRestriction":
                return importedStructure?.value?.foodRestriction ?? "";
            case "arrival":
                return importedStructure?.value?.arrival ?? "PRIVATE";
            case "arrivalNotes":
                return importedStructure?.value?.arrivalNotes ?? "";
            case "departure":
                return importedStructure?.value?.departure ?? "PRIVATE";
            case "departureNotes":
                return importedStructure?.value?.departureNotes ?? "";
            case "roomPartnerWishes":
                return importedStructure?.value?.roomPartnerWishes ?? [];
            case "zirkelPartnerWishes":
                return importedStructure?.value?.zirkelPartnerWishes ?? [];
            case "medicalNotes":
                return importedStructure?.value?.medicalNotes ?? [];
            case "topicWishes":
                return importedStructure?.value?.topicWishes ?? [];
            case "fractivityWishes":
                return importedStructure?.value?.fractivityWishes ?? [];
            case "leavingPremise":
                return importedStructure?.value?.leavingPremise ?? false;
            case "carpoolDataSharing":
                return importedStructure?.value?.carpoolDataSharing ?? false;
            case "removeTicks":
                return importedStructure?.value?.removeTicks ?? false;
            case "instruments":
                return importedStructure?.value?.instruments ?? [];
            case "pool":
                return importedStructure?.value?.pool ?? false;
            case "participates":
                return importedStructure?.value?.participates ?? false;
            case "certificate":
                return importedStructure?.value?.certificate ?? false;
            case "registrationTimestamp":
                return importedStructure?.value?.registrationTimestamp ?? null;
        }

        return null;
    }

    function clearFromHelper(key: string) {
        clearedKeys.value.push(key);
    }

    function clearFromHelperArray(key: string, val: string) {
        if (clearedArrayEntries.value[key] == undefined) {
            clearedArrayEntries.value[key] = [];
        }

        clearedArrayEntries.value[key].push(val);
    }

    function entryActive(key: string, val: string = "", overwriteVal: any = undefined) {
        if (clearedKeys.value.includes(key)) {
            return false;
        }
        if (Object.keys(clearedArrayEntries.value).includes(key)) {
            if (clearedArrayEntries.value[key].includes(val)) {
                return false;
            }

            const clearedArrayLength = clearedArrayEntries.value[key].length;
            const overwriteArrayLength = overwriteVal != undefined && Array.isArray(overwriteVal) ? overwriteVal.length : -1;
            let compareArrayLength = getParsedValueOrDefault(key).length;
            if (overwriteArrayLength > compareArrayLength) {
                compareArrayLength = overwriteArrayLength;
            }
            if (clearedArrayLength == overwriteArrayLength) {
                // array where all entries removed
                return false;
            }
        }

        return true;
    }

    return {
        setInput,
        clearInput,
        importedStructure,
        birthDate,
        callName,
        familyName,
        givenName,
        getParsedValueOrDefault,
        autofillTracker,
        unregisterAutofillTracker,
        registerAutofillTracker,
        updateAutofillTrackerState,
        insertAllInsertable,
        clearFromHelper,
        clearFromHelperArray,
        entryActive,
    };
});
