import { defineStore } from "pinia";
import { ref, watch } from "vue";

export const emptyDefaultId = "00000000-0000-0000-0000-000000000000";
export const defaultTabBase = "base";

export default defineStore("participant", () => {
    const participantId = ref(sessionStorage.getItem("participant_id") ?? emptyDefaultId);
    const participantViewTab = ref(sessionStorage.getItem("participant_tab") ?? defaultTabBase);
    const instructorId = ref(sessionStorage.getItem("instructor_id") ?? emptyDefaultId);
    const instructorViewTab = ref(sessionStorage.getItem("instructor_tab") ?? defaultTabBase);
    const instructorSubTab = ref((sessionStorage.getItem("instructor_sub_tab") ?? defaultTabBase) as "base" | "zirkel");
    const eventId = ref(sessionStorage.getItem("event_id") ?? emptyDefaultId);
    const zirkelId = ref(sessionStorage.getItem("zirkel_id") ?? emptyDefaultId);
    const eventZirkelViewTab = ref(sessionStorage.getItem("event_zirkel_tab") ?? defaultTabBase);

    watch(participantId, () => {
        sessionStorage.setItem("participant_id", participantId.value);
    });

    watch(participantViewTab, () => {
        sessionStorage.setItem("participant_tab", participantViewTab.value);
        if (
            participantViewTab.value != defaultTabBase &&
            participantViewTab.value != "add" &&
            participantViewTab.value != "zirkel"
        ) {
            eventId.value = participantViewTab.value;
        }
    });

    watch(instructorId, () => {
        sessionStorage.setItem("instructor_id", instructorId.value);
    });

    watch(instructorViewTab, () => {
        sessionStorage.setItem("instructor_tab", instructorViewTab.value);
        if (
            instructorViewTab.value != defaultTabBase &&
            instructorViewTab.value != "add" &&
            instructorViewTab.value != "zirkel"
        ) {
            eventId.value = instructorViewTab.value;
        }
    });

    watch(instructorSubTab, () => {
        sessionStorage.setItem("instructor_sub_tab", instructorSubTab.value);
    });

    watch(eventId, () => {
        sessionStorage.setItem("event_id", eventId.value);

        if (eventId.value != emptyDefaultId) {
            zirkelId.value = emptyDefaultId;

            participantViewTab.value = eventId.value;
            instructorViewTab.value = eventId.value;
        }
    });

    watch(zirkelId, () => {
        sessionStorage.setItem("zirkel_id", zirkelId.value);

        if (zirkelId.value != emptyDefaultId) {
            eventId.value = emptyDefaultId;
        }
    });

    watch(eventZirkelViewTab, () => {
        sessionStorage.setItem("event_zirkel_tab", eventZirkelViewTab.value);
    });

    return {
        participantId,
        participantViewTab,
        instructorId,
        instructorViewTab,
        instructorSubTab,
        eventId,
        zirkelId,
        eventZirkelViewTab,
    };
});
