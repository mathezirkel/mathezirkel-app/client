import { createRouter, createWebHashHistory } from "vue-router";
import useAppStore from "../stores/app";
import useNavigationStore, { defaultTabBase } from "../stores/navigation";
import { emptyDefaultId } from "../stores/navigation";

import Auth from "../components/Auth.vue";
import Graphiql from "../components/Graphiql.vue";
import Logtrigger from "../components/Logtrigger.vue";
import Home from "../components/Home.vue";
import ParticipantSearch from "../components/ParticipantSearch.vue";
import Participant from "../components/Participant.vue";
import InstructorSearch from "../components/InstructorSearch.vue";
import Instructor from "../components/Instructor.vue";
import CreateParticipant from "../components/CreateParticipant.vue";
import Events from "../components/Events.vue";
import EventAndZirkelDetails from "../components/EventAndZirkelDetails.vue";
import EventAndZirkelExport from "../components/EventAndZirkelExport.vue";
import ImportInterface from "../components/ImportInterface.vue";
import EventSignup from "../components/EventSignup.vue";
import ZirkelPlan from "../components/ZirkelPlan.vue";
import FractivityPlan from "../components/FractivityPlan.vue";

export enum DynamicRoutePaths {
    ParticipantSearch = "participants",
    ParticipantDetails = "participant",
    InstructorSearch = "instructors",
    InstructorDetails = "instructor",
    EventAndZirkelSearch = "events",
    EventAndZirkelDetails = "event_and_zirkel_detail",
    EventSignup = "event_signup",
    EventAndZirkelExports = "event_and_zirkel_exports",
    FractivityPlan = "fractivity_plan",
    ZirkelPlan = "zirkel_plan",
}
export enum StaticRoutePaths {
    BasePath = "basepath",
    Auth = "auth",
    AuthLogin = "auth_login",
    AuthLogout = "auth_logout",
    Logtrigger = "logtrigger",
    CreateParticipant = "create_participant",
    ImportInterface = "import_interface",
    Graphiql = "graphiql",
}

const participantIdKey = "participant_id";
const instructorIdKey = "instructor_id";
const participantTabKey = "participant_tab";
const instructorTabKey = "instructor_tab";
const instructorInnerTabKey = "instructor_inner_tab";
const eventIdKey = "event_id";
const eventOrZirkelIdKey = "event_or_zirkel_id";
const eventOrZirkelTypeKey = "event_or_zirkel_type";
const eventOrZirkelTabKey = "event_or_zirkel_tab";

const routes = [
    { path: "/", component: Home, name: StaticRoutePaths.BasePath },
    {
        path: "/" + StaticRoutePaths.AuthLogin,
        name: StaticRoutePaths.AuthLogin,
        redirect: (_to: any) => {
            return { name: StaticRoutePaths.Logtrigger, query: { login: true } };
        },
    },
    {
        path: "/" + StaticRoutePaths.AuthLogout,
        name: StaticRoutePaths.AuthLogout,
        redirect: (_to: any) => {
            return { name: StaticRoutePaths.Logtrigger, query: { logout: true } };
        },
    },
    {
        path: "/" + StaticRoutePaths.Logtrigger,
        component: Logtrigger,
        props: (route: any) => ({ login: route.query.login == "true", logout: route.query.logout == "true" }),
        name: StaticRoutePaths.Logtrigger,
    },
    {
        path: "/" + StaticRoutePaths.Auth,
        component: Auth,
        name: StaticRoutePaths.Auth,
    },
    {
        path: "/" + DynamicRoutePaths.ParticipantSearch,
        component: ParticipantSearch,
        name: DynamicRoutePaths.ParticipantSearch,
    },
    {
        path: "/" + DynamicRoutePaths.ParticipantDetails + `/:${participantIdKey}?` + `/:${participantTabKey}?`,
        component: Participant,
        name: DynamicRoutePaths.ParticipantDetails,
    },
    {
        path: "/" + DynamicRoutePaths.InstructorSearch,
        component: InstructorSearch,
        name: DynamicRoutePaths.InstructorSearch,
    },
    {
        path:
            "/" +
            DynamicRoutePaths.InstructorDetails +
            `/:${instructorIdKey}?` +
            `/:${instructorTabKey}?` +
            `/:${instructorInnerTabKey}?`,
        component: Instructor,
        name: DynamicRoutePaths.InstructorDetails,
    },
    {
        path: "/" + StaticRoutePaths.CreateParticipant,
        component: CreateParticipant,
        name: StaticRoutePaths.CreateParticipant,
    },
    {
        path: "/" + StaticRoutePaths.ImportInterface,
        component: ImportInterface,
        name: StaticRoutePaths.ImportInterface,
    },
    {
        path: "/" + DynamicRoutePaths.EventAndZirkelSearch,
        component: Events,
        name: DynamicRoutePaths.EventAndZirkelSearch,
    },
    {
        path:
            "/" +
            DynamicRoutePaths.EventAndZirkelDetails +
            `/:${eventOrZirkelTypeKey}?` +
            `/:${eventOrZirkelIdKey}?` +
            `/:${eventOrZirkelTabKey}?`,
        component: EventAndZirkelDetails,
        name: DynamicRoutePaths.EventAndZirkelDetails,
    },
    {
        path: "/" + DynamicRoutePaths.EventSignup + `/:${eventIdKey}?`,
        component: EventSignup,
        name: DynamicRoutePaths.EventSignup,
    },
    {
        path: "/" + DynamicRoutePaths.EventAndZirkelExports + `/:${eventIdKey}?`,
        component: EventAndZirkelExport,
        name: DynamicRoutePaths.EventAndZirkelExports,
    },
    {
        path: "/" + DynamicRoutePaths.ZirkelPlan + `/:${eventIdKey}?`,
        component: ZirkelPlan,
        name: DynamicRoutePaths.ZirkelPlan,
    },
    {
        path: "/" + DynamicRoutePaths.FractivityPlan + `/:${eventIdKey}?`,
        component: FractivityPlan,
        name: DynamicRoutePaths.FractivityPlan,
    },
    {
        path: "/" + StaticRoutePaths.Graphiql,
        component: Graphiql,
        name: StaticRoutePaths.Graphiql,
    },
] as any;

const router = createRouter({
    history: createWebHashHistory(""),
    routes,
    scrollBehavior(_to, _from, _savedPosition) {
        return { top: 0, behavior: "smooth" };
    },
});

function defaultizeId(id: any): string {
    if (id == null) {
        return emptyDefaultId;
    }
    if (id == undefined) {
        return emptyDefaultId;
    }
    if (id == "") {
        return emptyDefaultId;
    }

    return String(id);
}
const emptyDefaultTab = "empty_tab_value";
function defaultizeTab(tab: any): string {
    if (tab == null) {
        return emptyDefaultTab;
    }
    if (tab == undefined) {
        return emptyDefaultTab;
    }
    if (tab == "") {
        return emptyDefaultTab;
    }

    return String(tab);
}

// set ids from and to url if applicable
router.beforeEach((to, _from) => {
    const routeName = to.name;

    // auth redirect is handled in auth store I believe
    // IMPORTANT: "/" is handled here to resolve to true
    if (Object.values(StaticRoutePaths).includes(routeName as StaticRoutePaths)) {
        return true;
    }

    const navigationStore = useNavigationStore();

    switch (routeName) {
        case DynamicRoutePaths.ParticipantSearch:
            return true;
        case DynamicRoutePaths.ParticipantDetails:
            const routeParticipantId = defaultizeId(to.params[participantIdKey]);
            const storeParticipantId = defaultizeId(navigationStore.participantId);

            if (routeParticipantId == emptyDefaultId && storeParticipantId == emptyDefaultId) {
                // no ids specified, go to search
                return { name: DynamicRoutePaths.ParticipantSearch };
            }
            if (routeParticipantId == emptyDefaultId) {
                // route has no id specified, overwrite from store
                return { name: DynamicRoutePaths.ParticipantDetails, params: { [participantIdKey]: storeParticipantId } };
            }
            // route id is now definitely specified and the one to use, but may be unequal to the store one
            if (storeParticipantId != routeParticipantId) {
                navigationStore.participantId = routeParticipantId;
            }

            const routeParticipantTab = defaultizeTab(to.params[participantTabKey]);
            const storeParticipantTab = defaultizeTab(navigationStore.participantViewTab);
            if (routeParticipantTab == emptyDefaultTab) {
                // route has no value set for this. Insert from store (may be default)
                return {
                    name: DynamicRoutePaths.ParticipantDetails,
                    params: { [participantIdKey]: routeParticipantId, [participantTabKey]: storeParticipantTab },
                };
            }
            if (routeParticipantTab != storeParticipantTab) {
                // route has differing value set for this, overwrite the store
                navigationStore.participantViewTab = routeParticipantTab;
            }

            return true;
        case DynamicRoutePaths.InstructorSearch:
            return true;
        case DynamicRoutePaths.InstructorDetails:
            const routeInstructorId = defaultizeId(to.params[instructorIdKey]);
            const storeInstructorId = defaultizeId(navigationStore.instructorId);

            if (routeInstructorId == emptyDefaultId && storeInstructorId == emptyDefaultId) {
                // no ids specified, go to search
                return { name: DynamicRoutePaths.InstructorSearch };
            }
            if (routeInstructorId == emptyDefaultId) {
                // route has no id specified, overwrite from store
                return { name: DynamicRoutePaths.InstructorDetails, params: { [instructorIdKey]: storeInstructorId } };
            }
            // route id is now definitely specified and the one to use, but may be unequal to the store one
            if (storeInstructorId != routeInstructorId) {
                navigationStore.instructorId = routeInstructorId;
            }

            const routeInstructorTab = defaultizeTab(to.params[instructorTabKey]);
            const storeInstructorTab = defaultizeTab(navigationStore.instructorViewTab);
            if (routeInstructorTab == emptyDefaultTab) {
                // route has no value set for this. Insert from store (may be default)
                return {
                    name: DynamicRoutePaths.InstructorDetails,
                    params: { [instructorIdKey]: routeInstructorId, [instructorTabKey]: storeInstructorTab },
                };
            }
            if (routeInstructorTab != storeInstructorTab) {
                // route has differing value set for this, overwrite the store
                navigationStore.instructorViewTab = routeInstructorTab;
            }

            let routeInstructorSubTab = defaultizeTab(to.params[instructorInnerTabKey]);
            if (
                routeInstructorSubTab != "zirkel" &&
                routeInstructorSubTab != defaultTabBase &&
                routeInstructorSubTab != emptyDefaultTab
            ) {
                routeInstructorSubTab = emptyDefaultTab;
            }
            const storeInstructorSubTab = defaultizeTab(navigationStore.instructorSubTab);
            if (
                routeInstructorSubTab == emptyDefaultTab &&
                storeInstructorTab != defaultTabBase &&
                storeInstructorTab != "add" &&
                storeInstructorTab != "zirkel"
            ) {
                // route has no value set for this. Insert from store (may be default)
                return {
                    name: DynamicRoutePaths.InstructorDetails,
                    params: {
                        [instructorIdKey]: routeInstructorId,
                        [instructorTabKey]: storeInstructorTab,
                        [instructorInnerTabKey]: storeInstructorSubTab,
                    },
                };
            }
            if (routeInstructorSubTab != storeInstructorSubTab && routeInstructorSubTab != emptyDefaultTab) {
                // route has differing value set for this, overwrite the store
                navigationStore.instructorSubTab = routeInstructorSubTab as "zirkel" | "base";
            }

            return true;
        case DynamicRoutePaths.EventAndZirkelSearch:
            return true;
        case DynamicRoutePaths.EventSignup:
        case DynamicRoutePaths.EventAndZirkelExports:
        case DynamicRoutePaths.FractivityPlan:
        case DynamicRoutePaths.ZirkelPlan:
            const routeEventId = defaultizeId(to.params[eventIdKey]);
            const storeEventId = defaultizeId(navigationStore.eventId);

            if (routeEventId == emptyDefaultId && storeEventId == emptyDefaultId) {
                // no ids specified, go to search
                return { name: DynamicRoutePaths.EventAndZirkelSearch };
            }
            if (routeEventId == emptyDefaultId) {
                // route has no id specified, overwrite from store
                return { name: routeName, params: { [eventIdKey]: storeEventId } };
            }
            // route id is now definitely specified and the one to use, but may be unequal to the store one
            if (storeEventId != routeEventId) {
                navigationStore.eventId = routeEventId;
            }

            return true;
        case DynamicRoutePaths.EventAndZirkelDetails:
            const routeEventOrZirkelId = defaultizeId(to.params[eventOrZirkelIdKey]);
            const routeEventOrZirkelTypeString = to.params[eventOrZirkelTypeKey];
            let routeIsZirkel = false;
            if (routeEventOrZirkelTypeString == "zirkel") {
                routeIsZirkel = true;
            }
            const detailsStoreEventId = defaultizeId(navigationStore.eventId);
            const detailsStoreZirkelId = defaultizeId(navigationStore.zirkelId);

            if (
                routeEventOrZirkelId == emptyDefaultId &&
                detailsStoreEventId == emptyDefaultId &&
                detailsStoreZirkelId == emptyDefaultId
            ) {
                // no ids specified, go to search
                return { name: DynamicRoutePaths.EventAndZirkelSearch };
            }

            if (routeEventOrZirkelTypeString != "zirkel" && routeEventOrZirkelTypeString != "event") {
                // introducing the type specifying slug if not present
                if (detailsStoreEventId != emptyDefaultId && detailsStoreZirkelId == emptyDefaultId) {
                    console.log("introduce event slug");
                    return { name: DynamicRoutePaths.EventAndZirkelDetails, params: { [eventOrZirkelTypeKey]: "event" } };
                }
                if (detailsStoreEventId == emptyDefaultId && detailsStoreZirkelId != emptyDefaultId) {
                    console.log("introduce zirkel slug");
                    return {
                        name: DynamicRoutePaths.EventAndZirkelDetails,
                        params: {
                            [eventOrZirkelTypeKey]: "zirkel",
                        },
                    };
                }
                console.error(
                    "This navigation should not happen in normal operation (If a url is provided, it must be assumed that it is correctly structured)"
                );
                return { name: DynamicRoutePaths.EventAndZirkelSearch };
            }

            if (routeEventOrZirkelId == emptyDefaultId) {
                // route has no id specified, overwrite from store
                if (detailsStoreEventId != emptyDefaultId) {
                    return {
                        name: DynamicRoutePaths.EventAndZirkelDetails,
                        params: {
                            [eventOrZirkelTypeKey]: "event",
                            [eventOrZirkelIdKey]: detailsStoreEventId,
                        },
                    };
                }
                if (detailsStoreZirkelId != emptyDefaultId) {
                    return {
                        name: DynamicRoutePaths.EventAndZirkelDetails,
                        params: {
                            [eventOrZirkelTypeKey]: "zirkel",
                            [eventOrZirkelIdKey]: detailsStoreZirkelId,
                        },
                    };
                }
                console.error("This navigation should not happen in normal operation");
                return { name: DynamicRoutePaths.EventAndZirkelSearch };
            }

            // route id is now definitely specified and the one to use, but may be unequal to the store one
            if (routeIsZirkel) {
                if (routeEventOrZirkelId != detailsStoreZirkelId) {
                    navigationStore.zirkelId = routeEventOrZirkelId;
                    navigationStore.eventId = emptyDefaultId;
                }
            } else {
                if (routeEventOrZirkelId != detailsStoreEventId) {
                    navigationStore.zirkelId = emptyDefaultId;
                    navigationStore.eventId = routeEventOrZirkelId;
                }
            }

            const routeEventOrZirkelTab = defaultizeTab(to.params[eventOrZirkelTabKey]);
            const storeEventOrZirkelTab = defaultizeTab(navigationStore.eventZirkelViewTab);
            if (routeEventOrZirkelTab == emptyDefaultTab) {
                // route has no value set for this. Insert from store (may be default)
                return {
                    name: DynamicRoutePaths.EventAndZirkelDetails,
                    params: {
                        [eventOrZirkelTypeKey]: routeIsZirkel ? "zirkel" : "event",
                        [eventOrZirkelIdKey]: routeEventOrZirkelId,
                        [eventOrZirkelTabKey]: storeEventOrZirkelTab,
                    },
                };
            }
            if (routeEventOrZirkelTab != storeEventOrZirkelTab) {
                // route has differing value set for this, overwrite the store
                navigationStore.eventZirkelViewTab = routeEventOrZirkelTab;
            }

            return true;
        default:
            return { name: StaticRoutePaths.BasePath };
    }
});

// TOUCH CONTROL
let touched = false;
document.addEventListener("touchstart", () => {
    touched = true;
});
router.beforeEach(() => {
    // close menu, only necessary on touch devices. Looks proper on mouse devices
    if (touched) {
        useAppStore().triggerRerenderSidebar();
    }
});

export default router;
