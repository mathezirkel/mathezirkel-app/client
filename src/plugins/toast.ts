import { toast } from "vuetify-sonner";

export function success(message: string) {
    toast(message, {
        cardProps: {
            color: "success",
        },
        action: {
            label: "close",
            onClick: () => {},
            buttonProps: {
                class: "material-symbols-outlined",
            },
        },
    } as any);
}

export function error(message: string) {
    toast(message, {
        cardProps: {
            color: "error",
        },
        action: {
            label: "close",
            onClick: () => {},
            buttonProps: {
                class: "material-symbols-outlined",
            },
        },
    } as any);
}
