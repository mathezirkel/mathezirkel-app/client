let listeningCallbacks = {} as {
    [key: string]: (payload: any) => void;
};

export function addServiceWorkerListener(key: string, callback: (payload: any) => void) {
    listeningCallbacks[key] = callback;
}

if (!(process.env.VITE_RUNNING_TESTS ?? false)) {
    navigator.serviceWorker.addEventListener("message", (event) => {
        Object.keys(listeningCallbacks).forEach((key) => {
            if (key == event.data.key) {
                listeningCallbacks[key](event.data.payload);
            }
        });
    });
}

let worker: ServiceWorker | null = null;

// service worker on pwa builds
if ((process.env.VITE_BUILD_PWA ?? false) && "serviceWorker" in navigator) {
    let swURL = (process as any).env.VITE_BASE_STRING + process.env.VITE_SERVICEWORKER_FILENAME;

    if (!(process.env.VITE_RUNNING_TESTS ?? false)) {
        // tests do never need the sw registered

        if (process.env.DEV) {
            const swPathDev = (await import("../../src-sw/serviceworker")).default;
            swURL = swPathDev;
            console.log("Service-Worker in development Mode");
        }
    }

    navigator.serviceWorker
        .register(swURL, {
            type: "module",
        })
        .then((reg) => {
            const sw = reg.installing || reg.waiting || reg.active;
            worker = sw;
            console.log("Service Worker ready");
            addServiceWorkerListener("open-window", (url) => {
                window.open(url, "_blank");
            });
        })
        .catch(() => console.error("registration failed"));
}

export function postMessageToServiceWorker(key: string, payload: any) {
    if (worker != null) {
        worker.postMessage({ key, payload });
    }
}

export default worker;
