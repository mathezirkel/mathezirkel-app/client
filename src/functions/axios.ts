import { getBearer } from "./bearer-auth";
import axios from "axios";
import { baseURL } from "./env";

const axiosInstance = axios.create();

// re-calculate the auth-bearer token to use
axiosInstance.interceptors.request.use(
    async (config) => {
        const auth = `Bearer ${await getBearer()}`;

        config.baseURL = baseURL();
        config.headers["Authorization"] = auth;
        return config;
    },
    (error) => Promise.reject(error)
);

export default axiosInstance;
