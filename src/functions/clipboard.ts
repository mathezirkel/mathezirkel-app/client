import { success } from "./../plugins/toast";

export function pushToClipboard(text: string, message = "") {
    navigator.clipboard.writeText(text);

    if (message != "") {
        success(message);
    }
}
