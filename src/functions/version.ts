export async function getVersion() {
    return (process as any).env.PACKAGE_APP_VERSION;
}
