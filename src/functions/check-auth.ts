import axios from "./axios";
import { getBearer } from "./bearer-auth";

export interface User {
    sub: string;
    name: string;
    preferred_username: string;
    given_name: string;
    family_name: string;
    email_verified: boolean;
    roles: string[];
    instructor_uuid: string | null;
}

export async function checkAuthenticated(): Promise<[boolean, User | undefined]> {
    const token = await getBearer();
    if (token == "") {
        return [false, undefined];
    }

    try {
        const response = await axios.get("/user_info");

        if (response.data.access) {
            return [true, response.data.user];
        }
    } catch (error) {
        return [false, undefined];
    }

    return [false, undefined];
}
