import { ApolloClient, HttpLink, InMemoryCache, from } from "@apollo/client/core";
import { setContext } from "@apollo/client/link/context";
import { onError } from "@apollo/client/link/error";
import { RetryLink } from "@apollo/client/link/retry";
import { getBearer } from "./bearer-auth";
import { baseURL } from "./env";
import { error } from "./../plugins/toast";

const httpLink = new HttpLink({ uri: "http://test.test" }); // should be overwritten by next link in chain

const paramsLink = setContext(async () => {
    return {
        uri: baseURL() + "graphql",
        headers: {
            Authorization: `Bearer ${await getBearer()}`,
        },
    };
});

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
        graphQLErrors.forEach((gqlError) => {
            let message = `[Graphql error]: ${JSON.stringify(gqlError)}`;
            console.log(message);
            error(message);
        });
    }

    if (networkError) {
        let message = `[Network error]: ${networkError}`;
        console.log(message);
        error(message);
    }
});

const retryLink = new RetryLink({
    delay: {
        initial: 100,
        max: Infinity,
        jitter: true,
    },
    attempts: {
        max: 4,
    },
});

// Cache implementation
const cache = new InMemoryCache();

// Create the apollo client
const apolloClient = new ApolloClient({
    link: from([paramsLink, errorLink, retryLink, httpLink]),
    cache,
});

export default apolloClient;
