export function compareZirkelNames(a: string, b: string): number {
    const [numa, resta] = extractNumbersAndRemaining(a);
    const [numb, restb] = extractNumbersAndRemaining(b);

    if (numa.length < 1 || numb.length < 1) {
        return a.localeCompare(b);
    }

    if (numa.includes(NACHMITTAG_NUMBER) && !numb.includes(NACHMITTAG_NUMBER)) {
        return 1;
    }
    if (!numa.includes(NACHMITTAG_NUMBER) && numb.includes(NACHMITTAG_NUMBER)) {
        return -1;
    }

    if (numa[0] != numb[0]) {
        return numa[0] - numb[0];
    }

    if (numa.length != numb.length) {
        return numa.length - numb.length;
    }

    return resta.localeCompare(restb);
}

const NACHMITTAG_NUMBER = 9999;

export function extractNumbersAndRemaining(input: string): [number[], string] {
    const regex = /(5|6|7|8|9|10|11|12|13|Q|N)/g;

    const numbers = (input.match(regex) || [])
        .map((num) => num.replace("N", String(NACHMITTAG_NUMBER)))
        .map((num) => num.replace("Q", "16"))
        .map((num) => parseInt(num))
        .sort((a, b) => a - b);

    const remaining = input.replace(regex, "").replace(/\s+/g, " ").trim();

    return [numbers, remaining];
}
