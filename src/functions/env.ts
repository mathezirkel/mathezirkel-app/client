export function baseURL(): string {
    return (process as any).env.VITE_BACKEND_BASE_URL;
}

export function secondsToWaitForToken() {
    return parseInt((process as any).env.VITE_SECONDS_TO_WAIT_FOR_TOKEN_REDEEM);
}
