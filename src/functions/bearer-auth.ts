import { openBrowser } from "./open";
import { baseURL, secondsToWaitForToken } from "./env";
import { postMessageToServiceWorker, addServiceWorkerListener } from "../plugins/serviceWorker";

function sleep(milliseconds: number) {
    return new Promise((resolve) => setTimeout(resolve, milliseconds));
}

let currentBearer = "EMPTYREPLACEMENTTOKEN"; // if the header is malformed, because empty string on live SOMETIMES the Extractor fails spectacularily....
let whenBearerLastUpdated = Date.now();
addServiceWorkerListener("current-bearer-token", (payload: string) => {
    currentBearer = payload;
    whenBearerLastUpdated = Date.now();
});
export async function getBearer() {
    const startedRequestTime = Date.now();
    postMessageToServiceWorker("get-bearer", {});

    while (startedRequestTime > whenBearerLastUpdated) {
        await sleep(5);
    }

    return currentBearer;
}

export function sendLogoutRequest() {
    postMessageToServiceWorker("trigger-logout", {});
}

export async function triggerLogout() {
    sendLogoutRequest();
    await openBrowser((baseURL() ?? "") + "logout");
}

export async function triggerRequestToken() {
    postMessageToServiceWorker("request_token", {
        backendBaseURL: baseURL() ?? "",
        secondsToWaitForToken: secondsToWaitForToken(),
    });
}
