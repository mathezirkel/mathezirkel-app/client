import axios from "./../functions/axios";
import { success } from "./../plugins/toast";

export type ExportKinds = "camps" | "info" | "preparation_weekend" | "math_days" | "zirkel" | "playground";
export type ExportListType = "zirkel" | "event";
export type ExportJobDescription = {
    input: any;
    required_webdav_folders: any;
    stages: any;
    type: ExportListType;
    key: string;
    name: string;
    display: string;
};
export type GroupedStructuralExportJobDescription = {
    pathSegment: string;
    entry: ExportJobDescription | null;
    children: GroupedStructuralExportJobDescription[];
};
export type UnHydratedExportJobDescription = {
    input: any;
    required_webdav_folders: any;
    stages: any;
    type: ExportListType;
    key: string;
};
export type ExportControlStruct = {
    [key in ExportKinds]: {
        [key: string]: UnHydratedExportJobDescription;
    };
};

const exportsjsonimport = await import("./../../exports/generated.json");
export const exportJSON = exportsjsonimport as unknown as ExportControlStruct; // direct typing makes editor unhappy, even if it is entirely correct

export function hydrateConfigs(
    eventUUID: string,
    eventDisplay: string,
    zirkelIdentifiers: {
        zirkelUUID: string;
        zirkelDisplay: string;
    }[],
    input: UnHydratedExportJobDescription
): {
    updateStatusKey: string;
    eventExports: ExportJobDescription[];
    zirkelExports: ExportJobDescription[];
} {
    let eventExports = [] as ExportJobDescription[];
    let zirkelExports = [] as ExportJobDescription[];

    const eventFolderName = eventUUID + "--" + eventDisplay;

    let configStringToHydrate = JSON.stringify(input);

    if (input.type == "event") {
        const hydratedString = configStringToHydrate
            .replace(/%%EVENT_UUID%%/g, eventUUID)
            .replace(/%%EVENT_WEBDAV_FOLDER%%/g, eventFolderName);
        const hydratedConfig = JSON.parse(hydratedString) as ExportJobDescription;

        hydratedConfig.name = hydratedConfig.key;
        hydratedConfig.key = eventFolderName + "--" + hydratedConfig.key;
        hydratedConfig.display = eventDisplay;

        eventExports.push(hydratedConfig);
    } else if (input.type == "zirkel") {
        zirkelIdentifiers.forEach((zirkel) => {
            const zirkelFolderName = eventFolderName + "/" + zirkel.zirkelUUID + "--" + zirkel.zirkelDisplay;

            const hydratedString = configStringToHydrate
                .replace(/%%EVENT_UUID%%/g, eventUUID)
                .replace(/%%EVENT_WEBDAV_FOLDER%%/g, eventFolderName)
                .replace(/%%ZIRKEL_UUID%%/g, zirkel.zirkelUUID)
                .replace(/%%ZIRKEL_WEBDAV_FOLDER%%/g, zirkelFolderName);
            const hydratedConfig = JSON.parse(hydratedString) as ExportJobDescription;

            hydratedConfig.name = hydratedConfig.key;
            hydratedConfig.key = zirkelFolderName + "--" + hydratedConfig.key;
            hydratedConfig.display = zirkel.zirkelDisplay;

            zirkelExports.push(hydratedConfig);
        });
    }

    return {
        updateStatusKey: eventFolderName,
        eventExports,
        zirkelExports,
    };
}

export interface ExportRequestPostVariables {
    asynchronous: boolean; // on false, wait for the job to finish AND return the intermediate results
    jobs: ExportJobDescription[];
}

export async function processQueries(postData: ExportRequestPostVariables): Promise<string | null> {
    return await axios<{
        asynchronous: boolean;
        answers: {
            finished: boolean;
            key: string;
            response: {
                files: [];
                std_err: string;
                std_out: string;
                success: boolean;
            };
        }[];
    }>({
        url: "export",
        method: "post",
        data: postData,
    })
        .then(async (res) => {
            if (res) {
                success("Job dispatched");

                if (!res.data.asynchronous) {
                    const answer = res.data.answers[0];
                    if (answer) {
                        if (answer.finished) {
                            if (answer.response.success) {
                                return answer.response.std_out;
                            } else {
                                return answer.response.std_err;
                            }
                        }
                    }
                }
            }
            return null;
        })
        .catch((err) => {
            console.error(err);
            return null;
        });
}
