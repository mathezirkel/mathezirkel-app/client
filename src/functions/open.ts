import { baseURL } from "./env";

export async function openBrowser(url: string) {
    window.open(url, "_blank");
}

export async function openGraphql() {
    await openBrowser((baseURL() ?? "") + "graphiql");
}
