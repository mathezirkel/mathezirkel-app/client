import axios from "./axios";
import { error, success } from "./../plugins/toast";

export async function upload(path: string, content: string, notifySuccess: boolean = false) {
    let worked = false;

    await axios<{ success: boolean }>({
        url: "webdav_put",
        method: "post",
        data: {
            path: String(path),
            content: String(content),
        },
    })
        .then((res) => {
            if (res.data.success) {
                if (notifySuccess) {
                    success("Stored output to webdav target");
                }
                worked = true;
            } else {
                error("Webdav error while uploading");
            }
        })
        .catch((err) => {
            console.error(err);
            error("General webdav server error while uploading, see log");
        });

    return worked;
}

export async function remove(path: string, notifySuccess: boolean = false) {
    let worked = false;

    await axios<{ success: boolean }>({
        url: "webdav_delete",
        method: "post",
        data: {
            path: String(path),
        },
    })
        .then((res) => {
            if (res.data.success) {
                if (notifySuccess) {
                    success("Removed resource from webdav target");
                }
                worked = true;
            } else {
                error("Webdav error while deleting");
            }
        })
        .catch((err) => {
            console.error(err);
            error("General webdav server error while deleting, see log");
        });

    return worked;
}

export async function mkdir(path: string, notifySuccess: boolean = false) {
    let worked = false;

    await axios<{ success: boolean }>({
        url: "webdav_mkdir",
        method: "post",
        data: {
            path: String(path),
        },
    })
        .then((res) => {
            if (res.data.success) {
                if (notifySuccess) {
                    success("Created folder on webdav target");
                }
                worked = true;
            } else {
                // Nextcloud fails when folder is already present.
                // To avoid screen spam, do not report this in this specific instance
                // error("Webdav error while creating directory");
            }
        })
        .catch((err) => {
            console.error(err);
            error("General webdav server error while creating dir, see log");
        });

    return worked;
}
