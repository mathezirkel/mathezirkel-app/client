import worker from "./plugins/serviceWorker";
worker;

import { createApp } from "vue";
import "./styles/styles.scss";
import App from "./App.vue";

// sidebar
import vueAwesomeSidebar from "vue-awesome-sidebar";

// vuetify (component framework)
import vuetify from "./plugins/vuetify.ts";
import router from "./plugins/router.ts";

// store
import { createPinia } from "pinia";
const pinia = createPinia();

// apollo client
import { DefaultApolloClient } from "@vue/apollo-composable";
import apolloClient from "./functions/apollo-client";

// create app
createApp(App)
    .provide(DefaultApolloClient, apolloClient)
    .use(router)
    .use(vuetify)
    .use(vueAwesomeSidebar)
    .use(pinia)
    .mount("#app");
