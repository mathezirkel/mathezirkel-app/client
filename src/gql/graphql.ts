/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /**
   * Date in the proleptic Gregorian calendar (without time zone).
   *
   * Represents a description of the date (as used for birthdays, for example).
   * It cannot represent an instant on the time-line.
   *
   * [`Date` scalar][1] compliant.
   *
   * See also [`chrono::NaiveDate`][2] for details.
   *
   * [1]: https://graphql-scalars.dev/docs/scalars/date
   * [2]: https://docs.rs/chrono/latest/chrono/naive/struct.NaiveDate.html
   */
  Date: { input: any; output: any; }
  /**
   * Combined date and time (without time zone) in `yyyy-MM-dd HH:mm:ss` format.
   *
   * See also [`chrono::NaiveDateTime`][1] for details.
   *
   * [1]: https://docs.rs/chrono/latest/chrono/naive/struct.NaiveDateTime.html
   */
  LocalDateTime: { input: any; output: any; }
  Uuid: { input: any; output: any; }
};

export type BlockedSlotInput = {
  endBlock: Scalars['Int']['input'];
  nameBlock: Scalars['String']['input'];
  startBlock: Scalars['Int']['input'];
};

/** An enum-value that describes the year a pupil or teacher is in (Jahrgangsstufe) */
export enum ClassyearEnum {
  /** Pupil in Grade 5 */
  Class5 = 'CLASS5',
  /** Pupil in Grade 6 */
  Class6 = 'CLASS6',
  /** Pupil in Grade 7 */
  Class7 = 'CLASS7',
  /** Pupil in Grade 8 */
  Class8 = 'CLASS8',
  /** Pupil in Grade 9 */
  Class9 = 'CLASS9',
  /** Pupil in Grade 10 */
  Class10 = 'CLASS10',
  /** Pupil in Grade 11 */
  Class11 = 'CLASS11',
  /** Pupil in Grade 12 */
  Class12 = 'CLASS12',
  /** Pupil in Grade 13 */
  Class13 = 'CLASS13',
  /** Other person that is listed in the DB, because they want to receive Mails but from all Grades */
  ClassInfo = 'CLASS_INFO'
}

export type CreateContact = {
  extensionId: Scalars['Uuid']['input'];
  name: Scalars['String']['input'];
  telephone: Scalars['String']['input'];
};

export type CreateEvent = {
  additionalCosts?: InputMaybe<Scalars['Float']['input']>;
  blockedFractivitySlots?: InputMaybe<Array<BlockedSlotInput>>;
  budgetItem?: InputMaybe<Scalars['String']['input']>;
  budgetSection?: InputMaybe<Scalars['String']['input']>;
  certificateDefaultSignature?: InputMaybe<Scalars['String']['input']>;
  certificateSignatureDate?: InputMaybe<Scalars['Date']['input']>;
  costCenterBudget?: InputMaybe<Scalars['String']['input']>;
  costCenterCostAndActivityAccounting?: InputMaybe<Scalars['String']['input']>;
  costObject?: InputMaybe<Scalars['String']['input']>;
  costType?: InputMaybe<Scalars['String']['input']>;
  defaultFee: Scalars['Int']['input'];
  endDate: Scalars['Date']['input'];
  firstFractivityTime?: InputMaybe<Scalars['Int']['input']>;
  giveInstructorsFullReadAccess?: InputMaybe<Scalars['Boolean']['input']>;
  lastFractivityTime?: InputMaybe<Scalars['Int']['input']>;
  minIntervalsFractivity?: InputMaybe<Scalars['Int']['input']>;
  name: Scalars['String']['input'];
  numberOfAvailableBeamers?: InputMaybe<Scalars['Int']['input']>;
  overnightCosts?: InputMaybe<Scalars['Float']['input']>;
  purposeOfBusinessTrip?: InputMaybe<Scalars['String']['input']>;
  reasonOfAdditionalCosts?: InputMaybe<Scalars['String']['input']>;
  startDate: Scalars['Date']['input'];
  type: EventtypeEnum;
  year: Scalars['Int']['input'];
};

export type CreateExtension = {
  additionalEmails?: InputMaybe<Array<Scalars['String']['input']>>;
  arrival?: InputMaybe<TraveltypeEnum>;
  arrivalNotes?: InputMaybe<Scalars['String']['input']>;
  carpoolDataSharing?: InputMaybe<Scalars['Boolean']['input']>;
  certificate?: InputMaybe<Scalars['Boolean']['input']>;
  classYear?: InputMaybe<ClassyearEnum>;
  confirmed?: InputMaybe<Scalars['Boolean']['input']>;
  departure?: InputMaybe<TraveltypeEnum>;
  departureNotes?: InputMaybe<Scalars['String']['input']>;
  emailSelf?: InputMaybe<Scalars['String']['input']>;
  eventId: Scalars['Uuid']['input'];
  fee?: InputMaybe<Scalars['Int']['input']>;
  foodRestriction?: InputMaybe<Scalars['String']['input']>;
  fractivityWishes?: InputMaybe<Array<Scalars['String']['input']>>;
  hasSignedUp?: InputMaybe<Scalars['Boolean']['input']>;
  instruments?: InputMaybe<Array<Scalars['String']['input']>>;
  leavingPremise?: InputMaybe<Scalars['Boolean']['input']>;
  medicalNotes?: InputMaybe<Array<Scalars['String']['input']>>;
  notes?: InputMaybe<Scalars['String']['input']>;
  nutrition?: InputMaybe<NutritionEnum>;
  participantId: Scalars['Uuid']['input'];
  participates?: InputMaybe<Scalars['Boolean']['input']>;
  pool?: InputMaybe<Scalars['Boolean']['input']>;
  reasonOfSignoff?: InputMaybe<Scalars['String']['input']>;
  removeTicks?: InputMaybe<Scalars['Boolean']['input']>;
  roomNumber?: InputMaybe<Scalars['String']['input']>;
  roomPartnerWishes?: InputMaybe<Array<Scalars['String']['input']>>;
  telephone?: InputMaybe<Scalars['String']['input']>;
  timeOfSignoff?: InputMaybe<Scalars['LocalDateTime']['input']>;
  topicWishes?: InputMaybe<Array<Scalars['String']['input']>>;
  zirkelIds?: InputMaybe<Array<Scalars['Uuid']['input']>>;
  zirkelPartnerWishes?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type CreateFractivityDistribution = {
  fractivityEntryId: Scalars['Uuid']['input'];
  roomId: Scalars['Uuid']['input'];
  startTime: Scalars['Int']['input'];
};

export type CreateFractivityEntry = {
  allowedRooms: Array<Scalars['Uuid']['input']>;
  allowedStarts: Array<Scalars['Int']['input']>;
  duration: Scalars['Int']['input'];
  eventId: Scalars['Uuid']['input'];
  followUpTime?: InputMaybe<Scalars['Int']['input']>;
  instructorExtensionUuids: Array<Scalars['Uuid']['input']>;
  preparationTime?: InputMaybe<Scalars['Int']['input']>;
  startDay: Scalars['Date']['input'];
  topic: Scalars['String']['input'];
};

export type CreateFractivityRoom = {
  eventId: Scalars['Uuid']['input'];
  name: Scalars['String']['input'];
  priority?: InputMaybe<Scalars['Int']['input']>;
  roomTypes: Array<RoomEnum>;
};

export type CreateInstructorExtension = {
  accommodationCosts?: InputMaybe<Scalars['Float']['input']>;
  arrival?: InputMaybe<TraveltypeEnum>;
  arrivalAtBusinessLocation?: InputMaybe<Scalars['LocalDateTime']['input']>;
  arrivalNotes?: InputMaybe<Scalars['String']['input']>;
  chair?: InputMaybe<Scalars['String']['input']>;
  contractWith?: InputMaybe<Scalars['String']['input']>;
  departure?: InputMaybe<TraveltypeEnum>;
  departureNotes?: InputMaybe<Scalars['String']['input']>;
  emailWork?: InputMaybe<Scalars['String']['input']>;
  endOfBusinessActivities?: InputMaybe<Scalars['LocalDateTime']['input']>;
  endOfTravel?: InputMaybe<Scalars['LocalDateTime']['input']>;
  eventId: Scalars['Uuid']['input'];
  faculty?: InputMaybe<Scalars['String']['input']>;
  foodRestriction?: InputMaybe<Scalars['String']['input']>;
  hasContract?: InputMaybe<Scalars['Boolean']['input']>;
  instructorId: Scalars['Uuid']['input'];
  jobTitle?: InputMaybe<Scalars['String']['input']>;
  nutrition?: InputMaybe<NutritionEnum>;
  personnelNumber?: InputMaybe<Scalars['String']['input']>;
  roomNumber?: InputMaybe<Scalars['String']['input']>;
  startOfBusinessActivities?: InputMaybe<Scalars['LocalDateTime']['input']>;
  startOfReturnJourney?: InputMaybe<Scalars['LocalDateTime']['input']>;
  startOfTravel?: InputMaybe<Scalars['LocalDateTime']['input']>;
  telephoneWork?: InputMaybe<Scalars['String']['input']>;
};

export type CreateParticipant = {
  birthDate: Scalars['Date']['input'];
  callName?: InputMaybe<Scalars['String']['input']>;
  city: Scalars['String']['input'];
  country: Scalars['String']['input'];
  familyName: Scalars['String']['input'];
  gender?: InputMaybe<Scalars['String']['input']>;
  givenName: Scalars['String']['input'];
  notes?: InputMaybe<Scalars['String']['input']>;
  postalCode: Scalars['String']['input'];
  sex: SexEnum;
  street: Scalars['String']['input'];
  streetNumber: Scalars['String']['input'];
};

export type CreateZirkel = {
  eventId: Scalars['Uuid']['input'];
  name: Scalars['String']['input'];
  room?: InputMaybe<Scalars['String']['input']>;
  topics?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type CreateZirkelPlanEntry = {
  beamer?: InputMaybe<Scalars['Boolean']['input']>;
  notes?: InputMaybe<Scalars['String']['input']>;
  topic?: InputMaybe<Scalars['String']['input']>;
  zirkelId: Scalars['Uuid']['input'];
  zirkelPlanSlotId: Scalars['Uuid']['input'];
};

export type CreateZirkelPlanSlot = {
  beamer?: InputMaybe<Scalars['Boolean']['input']>;
  certificate?: InputMaybe<Scalars['Boolean']['input']>;
  endTime: Scalars['LocalDateTime']['input'];
  eventId: Scalars['Uuid']['input'];
  name: Scalars['String']['input'];
  startTime: Scalars['LocalDateTime']['input'];
};

/** An enum-value that describes the type of a Mathezirkel event */
export enum EventtypeEnum {
  /** People added here only want info mails and are not (forced) to also be participating at any event for real */
  Info = 'INFO',
  /** The original Mathecamp established 2014 */
  MathCamp = 'MATH_CAMP',
  /** A special day with specialize math-events */
  MathDay = 'MATH_DAY',
  /** Productive time for instructors to plan the camp */
  PreparationWeekend = 'PREPARATION_WEEKEND',
  /** The Oberstufen-Wintercamp event */
  WinterCamp = 'WINTER_CAMP',
  /** A regular during the math-year running zirkel, that may be of the Sub-Types 'Präsenz', 'Korrespondenz-Mail' or 'Korrespondenz-Brief' */
  Zirkel = 'ZIRKEL'
}

/** An enum-value that describes different nutritional orientations */
export enum NutritionEnum {
  /** Someone whose diet allows them to eat everything */
  Omnivore = 'OMNIVORE',
  /** The nutrition once was set. But as it is a sensitive information, in this case the data was purged from the System */
  Redacted = 'REDACTED',
  /** Someone whose diet allows them to eat no products made from animals */
  Vegan = 'VEGAN',
  /** Someone whose diet allows them not to eat meat */
  Vegetarian = 'VEGETARIAN'
}

/** An enum-value that describes the purpose of a room */
export enum RoomEnum {
  /** Needs the Craft Rooms */
  Crafts = 'CRAFTS',
  /** Takes place inside */
  Inside = 'INSIDE',
  /** Location describes only the take-off üoint of the fractivity */
  Meetingpoint = 'MEETINGPOINT',
  /** A Place for Music */
  Music = 'MUSIC',
  /** Takes place outside */
  Outside = 'OUTSIDE',
  /** Programming related fractivity (needs the room with laptop support possibly) */
  Programming = 'PROGRAMMING',
  /** Requires the gymnasium or possibly outside */
  Sport = 'SPORT',
  /** Needs a talk room (possibly furnished and enough space) */
  Talk = 'TALK'
}

/** An enum-value that describes the sexuality by-law */
export enum SexEnum {
  /** The sexuality is considered diverse */
  Diverse = 'DIVERSE',
  /** The sexuality is considered female */
  Female = 'FEMALE',
  /** The sexuality is considered male */
  Male = 'MALE',
  /** The sexuality is forced to be set and once was. But as it is a sensitive information, in this case the data was purged from the System */
  Redacted = 'REDACTED'
}

/** An enum-value that describes different ways a person can arrive at the event */
export enum TraveltypeEnum {
  /** The person rides a bike */
  Bicycle = 'BICYCLE',
  /** The person arrives by a bus that we manage */
  Bus = 'BUS',
  /** The person arrives with private means like walking of by personal car */
  Private = 'PRIVATE'
}

export type UpdateContact = {
  extensionId?: InputMaybe<Scalars['Uuid']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  telephone?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateEvent = {
  additionalCosts?: InputMaybe<Scalars['Float']['input']>;
  blockedFractivitySlots?: InputMaybe<Array<BlockedSlotInput>>;
  budgetItem?: InputMaybe<Scalars['String']['input']>;
  budgetSection?: InputMaybe<Scalars['String']['input']>;
  certificateDefaultSignature?: InputMaybe<Scalars['String']['input']>;
  certificateSignatureDate?: InputMaybe<Scalars['Date']['input']>;
  costCenterBudget?: InputMaybe<Scalars['String']['input']>;
  costCenterCostAndActivityAccounting?: InputMaybe<Scalars['String']['input']>;
  costObject?: InputMaybe<Scalars['String']['input']>;
  costType?: InputMaybe<Scalars['String']['input']>;
  defaultFee?: InputMaybe<Scalars['Int']['input']>;
  endDate?: InputMaybe<Scalars['Date']['input']>;
  firstFractivityTime?: InputMaybe<Scalars['Int']['input']>;
  giveInstructorsFullReadAccess?: InputMaybe<Scalars['Boolean']['input']>;
  lastFractivityTime?: InputMaybe<Scalars['Int']['input']>;
  minIntervalsFractivity?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  numberOfAvailableBeamers?: InputMaybe<Scalars['Int']['input']>;
  overnightCosts?: InputMaybe<Scalars['Float']['input']>;
  purposeOfBusinessTrip?: InputMaybe<Scalars['String']['input']>;
  reasonOfAdditionalCosts?: InputMaybe<Scalars['String']['input']>;
  startDate?: InputMaybe<Scalars['Date']['input']>;
  type?: InputMaybe<EventtypeEnum>;
  year?: InputMaybe<Scalars['Int']['input']>;
};

export type UpdateExtension = {
  additionalEmails?: InputMaybe<Array<Scalars['String']['input']>>;
  arrival?: InputMaybe<TraveltypeEnum>;
  arrivalNotes?: InputMaybe<Scalars['String']['input']>;
  carpoolDataSharing?: InputMaybe<Scalars['Boolean']['input']>;
  certificate?: InputMaybe<Scalars['Boolean']['input']>;
  classYear?: InputMaybe<ClassyearEnum>;
  confirmed?: InputMaybe<Scalars['Boolean']['input']>;
  departure?: InputMaybe<TraveltypeEnum>;
  departureNotes?: InputMaybe<Scalars['String']['input']>;
  emailSelf?: InputMaybe<Scalars['String']['input']>;
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  fee?: InputMaybe<Scalars['Int']['input']>;
  foodRestriction?: InputMaybe<Scalars['String']['input']>;
  fractivityWishes?: InputMaybe<Array<Scalars['String']['input']>>;
  hasSignedUp?: InputMaybe<Scalars['Boolean']['input']>;
  instruments?: InputMaybe<Array<Scalars['String']['input']>>;
  leavingPremise?: InputMaybe<Scalars['Boolean']['input']>;
  medicalNotes?: InputMaybe<Array<Scalars['String']['input']>>;
  notes?: InputMaybe<Scalars['String']['input']>;
  nutrition?: InputMaybe<NutritionEnum>;
  participantId?: InputMaybe<Scalars['Uuid']['input']>;
  participates?: InputMaybe<Scalars['Boolean']['input']>;
  pool?: InputMaybe<Scalars['Boolean']['input']>;
  reasonOfSignoff?: InputMaybe<Scalars['String']['input']>;
  registrationTimestamp?: InputMaybe<Scalars['LocalDateTime']['input']>;
  removeTicks?: InputMaybe<Scalars['Boolean']['input']>;
  roomNumber?: InputMaybe<Scalars['String']['input']>;
  roomPartnerWishes?: InputMaybe<Array<Scalars['String']['input']>>;
  telephone?: InputMaybe<Scalars['String']['input']>;
  timeOfSignoff?: InputMaybe<Scalars['LocalDateTime']['input']>;
  topicWishes?: InputMaybe<Array<Scalars['String']['input']>>;
  zirkelIds?: InputMaybe<Array<Scalars['Uuid']['input']>>;
  zirkelPartnerWishes?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type UpdateFractivityDistribution = {
  fractivityEntryId?: InputMaybe<Scalars['Uuid']['input']>;
  roomId?: InputMaybe<Scalars['Uuid']['input']>;
  startTime?: InputMaybe<Scalars['Int']['input']>;
};

export type UpdateFractivityEntry = {
  allowedRooms?: InputMaybe<Array<Scalars['Uuid']['input']>>;
  allowedStarts?: InputMaybe<Array<Scalars['Int']['input']>>;
  duration?: InputMaybe<Scalars['Int']['input']>;
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  followUpTime?: InputMaybe<Scalars['Int']['input']>;
  instructorExtensionUuids?: InputMaybe<Array<Scalars['Uuid']['input']>>;
  preparationTime?: InputMaybe<Scalars['Int']['input']>;
  startDay?: InputMaybe<Scalars['Date']['input']>;
  topic?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateFractivityRoom = {
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  priority?: InputMaybe<Scalars['Int']['input']>;
  roomTypes?: InputMaybe<Array<RoomEnum>>;
};

export type UpdateInstructorExtension = {
  accommodationCosts?: InputMaybe<Scalars['Float']['input']>;
  arrival?: InputMaybe<TraveltypeEnum>;
  arrivalAtBusinessLocation?: InputMaybe<Scalars['LocalDateTime']['input']>;
  arrivalNotes?: InputMaybe<Scalars['String']['input']>;
  chair?: InputMaybe<Scalars['String']['input']>;
  contractWith?: InputMaybe<Scalars['String']['input']>;
  departure?: InputMaybe<TraveltypeEnum>;
  departureNotes?: InputMaybe<Scalars['String']['input']>;
  emailWork?: InputMaybe<Scalars['String']['input']>;
  endOfBusinessActivities?: InputMaybe<Scalars['LocalDateTime']['input']>;
  endOfTravel?: InputMaybe<Scalars['LocalDateTime']['input']>;
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  faculty?: InputMaybe<Scalars['String']['input']>;
  foodRestriction?: InputMaybe<Scalars['String']['input']>;
  hasContract?: InputMaybe<Scalars['Boolean']['input']>;
  instructorId?: InputMaybe<Scalars['Uuid']['input']>;
  jobTitle?: InputMaybe<Scalars['String']['input']>;
  nutrition?: InputMaybe<NutritionEnum>;
  personnelNumber?: InputMaybe<Scalars['String']['input']>;
  roomNumber?: InputMaybe<Scalars['String']['input']>;
  startOfBusinessActivities?: InputMaybe<Scalars['LocalDateTime']['input']>;
  startOfReturnJourney?: InputMaybe<Scalars['LocalDateTime']['input']>;
  startOfTravel?: InputMaybe<Scalars['LocalDateTime']['input']>;
  telephoneWork?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateParticipant = {
  birthDate?: InputMaybe<Scalars['Date']['input']>;
  callName?: InputMaybe<Scalars['String']['input']>;
  city?: InputMaybe<Scalars['String']['input']>;
  country?: InputMaybe<Scalars['String']['input']>;
  familyName?: InputMaybe<Scalars['String']['input']>;
  gender?: InputMaybe<Scalars['String']['input']>;
  givenName?: InputMaybe<Scalars['String']['input']>;
  notes?: InputMaybe<Scalars['String']['input']>;
  postalCode?: InputMaybe<Scalars['String']['input']>;
  sex?: InputMaybe<SexEnum>;
  street?: InputMaybe<Scalars['String']['input']>;
  streetNumber?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateSignupExtension = {
  hasSignedUp?: InputMaybe<Scalars['Boolean']['input']>;
  signupPhoto?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateZirkel = {
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  room?: InputMaybe<Scalars['String']['input']>;
  topics?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type UpdateZirkelPlanEntry = {
  beamer?: InputMaybe<Scalars['Boolean']['input']>;
  notes?: InputMaybe<Scalars['String']['input']>;
  topic?: InputMaybe<Scalars['String']['input']>;
  zirkelId?: InputMaybe<Scalars['Uuid']['input']>;
  zirkelPlanSlotId?: InputMaybe<Scalars['Uuid']['input']>;
};

export type UpdateZirkelPlanSlot = {
  beamer?: InputMaybe<Scalars['Boolean']['input']>;
  certificate?: InputMaybe<Scalars['Boolean']['input']>;
  endTime?: InputMaybe<Scalars['LocalDateTime']['input']>;
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  startTime?: InputMaybe<Scalars['LocalDateTime']['input']>;
};

export type CreateParticipantMutationVariables = Exact<{
  sex?: InputMaybe<SexEnum>;
  street?: InputMaybe<Scalars['String']['input']>;
  streetNumber?: InputMaybe<Scalars['String']['input']>;
  postalCode?: InputMaybe<Scalars['String']['input']>;
  givenName?: InputMaybe<Scalars['String']['input']>;
  gender?: InputMaybe<Scalars['String']['input']>;
  familyName?: InputMaybe<Scalars['String']['input']>;
  country?: InputMaybe<Scalars['String']['input']>;
  city?: InputMaybe<Scalars['String']['input']>;
  callName?: InputMaybe<Scalars['String']['input']>;
  birthDate?: InputMaybe<Scalars['Date']['input']>;
}>;


export type CreateParticipantMutation = { __typename?: 'Mutation', createParticipant?: { __typename?: 'Participant', id: any } | null };

export type SignupExtensionsQueryQueryVariables = Exact<{
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type SignupExtensionsQueryQuery = { __typename?: 'Query', signupExtensions?: Array<{ __typename?: 'ExtensionSignup', participantId: any, id: any, hasSignedUp: boolean, familyName: string, callName: string, hasSignupPhoto: boolean }> | null };

export type EventNameQueryQueryVariables = Exact<{
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type EventNameQueryQuery = { __typename?: 'Query', event?: { __typename?: 'Event', name: string, year: number } | null };

export type SignupSelectionQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type SignupSelectionQueryQuery = { __typename?: 'Query', signupExtension?: { __typename?: 'ExtensionSignup', hasSignedUp: boolean, familyName: string, callName: string, hasSignupPhoto: boolean, signupPhoto?: string | null, roomNumber?: string | null } | null };

export type UpdateSignupMutationMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
  hasSignedUp?: InputMaybe<Scalars['Boolean']['input']>;
  signupPhoto?: InputMaybe<Scalars['String']['input']>;
}>;


export type UpdateSignupMutationMutation = { __typename?: 'Mutation', updateParticipantSignupExtension?: { __typename?: 'ExtensionSignup', signupPhoto?: string | null, participantId: any, id: any, hasSignedUp: boolean, familyName: string, callName: string, hasSignupPhoto: boolean, roomNumber?: string | null } | null };

export type EventsAndZirkelNamesQueryVariables = Exact<{ [key: string]: never; }>;


export type EventsAndZirkelNamesQuery = { __typename?: 'Query', allEvents?: Array<{ __typename?: 'Event', id: any, name: string, year: number, startDate: any, zirkel?: Array<{ __typename?: 'Zirkel', id: any, name: string }> | null }> | null };

export type FractivityPlanMetaQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type FractivityPlanMetaQueryQuery = { __typename?: 'Query', event?: { __typename?: 'Event', id: any, startDate: any, endDate: any, firstFractivityTime: number, lastFractivityTime: number, minIntervalsFractivity: number, blockedFractivitySlots: Array<{ __typename?: 'BlockedSlot', startBlock: number, endBlock: number, nameBlock: string }>, instructorExtensionsPublic?: Array<{ __typename?: 'InstructorExtensionPublic', id: any, name: string, instructorId: any }> | null, fractivityRooms?: Array<{ __typename?: 'FractivityRoom', name: string, id: any, roomTypes: Array<RoomEnum> }> | null } | null };

export type FractivityPlanQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type FractivityPlanQueryQuery = { __typename?: 'Query', event?: { __typename?: 'Event', id: any, fractivityEntries?: Array<{ __typename?: 'FractivityEntry', topic: string, startDay: any, preparationTime: number, id: any, followUpTime: number, eventId: any, duration: number, allowedStarts: Array<number>, instructorExtensionsPublic?: Array<{ __typename?: 'InstructorExtensionPublic', id: any, name: string, instructorId: any }> | null, fractivityDistribution?: { __typename?: 'FractivityDistribution', id: any, roomId: any, startTime: number, color: string, room: { __typename?: 'FractivityRoom', name: string, id: any, roomTypes: Array<RoomEnum> } } | null, allowedRooms: Array<{ __typename?: 'FractivityRoom', id: any, roomTypes: Array<RoomEnum>, name: string }> }> | null, fractivityDistributions?: Array<{ __typename?: 'FractivityDistribution', id: any, roomId: any, startTime: number, color: string, room: { __typename?: 'FractivityRoom', name: string, id: any, roomTypes: Array<RoomEnum> }, fractivityEntry: { __typename?: 'FractivityEntry', topic: string, startDay: any, preparationTime: number, id: any, followUpTime: number, eventId: any, duration: number, allowedStarts: Array<number>, instructorExtensionsPublic?: Array<{ __typename?: 'InstructorExtensionPublic', id: any, name: string, instructorId: any }> | null } }> | null } | null };

export type CreateFractivityEntryMutationVariables = Exact<{
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  preparationTime?: InputMaybe<Scalars['Int']['input']>;
  followUpTime?: InputMaybe<Scalars['Int']['input']>;
  topic?: InputMaybe<Scalars['String']['input']>;
  startDay?: InputMaybe<Scalars['Date']['input']>;
  instructorExtensionUuids?: InputMaybe<Array<Scalars['Uuid']['input']> | Scalars['Uuid']['input']>;
  duration?: InputMaybe<Scalars['Int']['input']>;
  allowedStarts?: InputMaybe<Array<Scalars['Int']['input']> | Scalars['Int']['input']>;
  allowedRooms?: InputMaybe<Array<Scalars['Uuid']['input']> | Scalars['Uuid']['input']>;
}>;


export type CreateFractivityEntryMutation = { __typename?: 'Mutation', createFractivityEntry?: { __typename?: 'FractivityEntry', topic: string, startDay: any, preparationTime: number, id: any, followUpTime: number, eventId: any, duration: number, allowedStarts: Array<number>, instructorExtensionsPublic?: Array<{ __typename?: 'InstructorExtensionPublic', id: any, name: string, instructorId: any }> | null, fractivityDistribution?: { __typename?: 'FractivityDistribution', id: any, roomId: any, startTime: number, room: { __typename?: 'FractivityRoom', name: string, id: any, roomTypes: Array<RoomEnum> } } | null, allowedRooms: Array<{ __typename?: 'FractivityRoom', id: any, roomTypes: Array<RoomEnum>, name: string }> } | null };

export type DeleteFractivityPlanEntryMutationVariables = Exact<{
  fractivityEntryId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type DeleteFractivityPlanEntryMutation = { __typename?: 'Mutation', deleteFractivityEntry?: number | null };

export type DeleteFractivityPlanDistributionMutationVariables = Exact<{
  fractivityDistributionId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type DeleteFractivityPlanDistributionMutation = { __typename?: 'Mutation', deleteFractivityDistribution?: number | null };

export type UpdateFractivityEntryMutationVariables = Exact<{
  fractivityEntryId?: InputMaybe<Scalars['Uuid']['input']>;
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  topic?: InputMaybe<Scalars['String']['input']>;
  startDay?: InputMaybe<Scalars['Date']['input']>;
  preparationTime?: InputMaybe<Scalars['Int']['input']>;
  instructorExtensionUuids?: InputMaybe<Array<Scalars['Uuid']['input']> | Scalars['Uuid']['input']>;
  followUpTime?: InputMaybe<Scalars['Int']['input']>;
  duration?: InputMaybe<Scalars['Int']['input']>;
  allowedStarts?: InputMaybe<Array<Scalars['Int']['input']> | Scalars['Int']['input']>;
  allowedRooms?: InputMaybe<Array<Scalars['Uuid']['input']> | Scalars['Uuid']['input']>;
}>;


export type UpdateFractivityEntryMutation = { __typename?: 'Mutation', updateFractivityEntry?: { __typename?: 'FractivityEntry', topic: string, startDay: any, preparationTime: number, id: any, followUpTime: number, eventId: any, duration: number, allowedStarts: Array<number>, instructorExtensionsPublic?: Array<{ __typename?: 'InstructorExtensionPublic', id: any, name: string, instructorId: any }> | null, fractivityDistribution?: { __typename?: 'FractivityDistribution', id: any, roomId: any, startTime: number, room: { __typename?: 'FractivityRoom', name: string, id: any, roomTypes: Array<RoomEnum> } } | null, allowedRooms: Array<{ __typename?: 'FractivityRoom', id: any, roomTypes: Array<RoomEnum>, name: string }> } | null };

export type DistributeRestMutationMutationVariables = Exact<{
  fractivityIds?: InputMaybe<Array<Scalars['Uuid']['input']> | Scalars['Uuid']['input']>;
}>;


export type DistributeRestMutationMutation = { __typename?: 'Mutation', distributeNondistributedFractivities?: Array<{ __typename?: 'FractivityDistribution', id: any }> | null };

export type DistributeAllMutationMutationVariables = Exact<{
  fractivityIds?: InputMaybe<Array<Scalars['Uuid']['input']> | Scalars['Uuid']['input']>;
}>;


export type DistributeAllMutationMutation = { __typename?: 'Mutation', distributeFractivities?: Array<{ __typename?: 'FractivityDistribution', id: any }> | null };

export type InstructorQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type InstructorQueryQuery = { __typename?: 'Query', instructor?: { __typename?: 'Instructor', id: any, familyName?: string | null, callName: string, extensions: Array<{ __typename?: 'InstructorExtension', id: any, event?: { __typename?: 'Event', id: any, name: string, year: number, startDate: any } | null }> } | null };

export type InstructorsSearchQueryVariables = Exact<{ [key: string]: never; }>;


export type InstructorsSearchQuery = { __typename?: 'Query', allInstructors?: Array<{ __typename?: 'Instructor', id: any, givenName?: string | null, familyName?: string | null, callName: string, telephone?: string | null }> | null };

export type ParticipantQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ParticipantQueryQuery = { __typename?: 'Query', participant?: { __typename?: 'Participant', id: any, familyName: string, callName: string, extensions: Array<{ __typename?: 'Extension', id: any, event?: { __typename?: 'Event', id: any, name: string, year: number, startDate: any } | null }> } | null };

export type ParticipantsSearchQueryVariables = Exact<{ [key: string]: never; }>;


export type ParticipantsSearchQuery = { __typename?: 'Query', allParticipants?: Array<{ __typename?: 'Participant', id: any, familyName: string, callName: string, sex: SexEnum, extensions: Array<{ __typename?: 'Extension', emailSelf?: string | null, additionalEmails: Array<string>, contacts?: Array<{ __typename?: 'Contact', telephone: string }> | null }> }> | null };

export type InstructorEventsQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type InstructorEventsQueryQuery = { __typename?: 'Query', instructor?: { __typename?: 'Instructor', id: any, extensions: Array<{ __typename?: 'InstructorExtension', id: any, event?: { __typename?: 'Event', id: any } | null }> } | null };

export type EventNamesAndIdsForInstructorsQueryVariables = Exact<{ [key: string]: never; }>;


export type EventNamesAndIdsForInstructorsQuery = { __typename?: 'Query', allEvents?: Array<{ __typename?: 'Event', id: any, name: string, year: number, startDate: any }> | null };

export type AddEventToInstructorMutationMutationVariables = Exact<{
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  instructorId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type AddEventToInstructorMutationMutation = { __typename?: 'Mutation', createInstructorExtension?: { __typename?: 'InstructorExtension', id: any, instructor?: { __typename?: 'Instructor', id: any, extensions: Array<{ __typename?: 'InstructorExtension', id: any }> } | null } | null };

export type ParticipantEventsQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ParticipantEventsQueryQuery = { __typename?: 'Query', participant?: { __typename?: 'Participant', id: any, extensions: Array<{ __typename?: 'Extension', id: any, event?: { __typename?: 'Event', id: any } | null }> } | null };

export type EventNamesAndIdsQueryVariables = Exact<{ [key: string]: never; }>;


export type EventNamesAndIdsQuery = { __typename?: 'Query', allEvents?: Array<{ __typename?: 'Event', id: any, name: string, year: number, startDate: any, type: EventtypeEnum }> | null };

export type AddEventToParticipantMutationMutationVariables = Exact<{
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  participantId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type AddEventToParticipantMutationMutation = { __typename?: 'Mutation', createParticipantExtension?: { __typename?: 'Extension', id: any, participant?: { __typename?: 'Participant', id: any, extensions: Array<{ __typename?: 'Extension', id: any }> } | null } | null };

export type ParticipantBaseDataQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ParticipantBaseDataQueryQuery = { __typename?: 'Query', participant?: { __typename?: 'Participant', id: any, givenName: string, familyName: string, callName: string, callNameDirectReadout?: string | null, sex: SexEnum, gender: string, genderDirectReadout?: string | null, birthDate: any, street: string, streetNumber: string, postalCode: string, city: string, country: string, notes?: string | null, extensions: Array<{ __typename?: 'Extension', id: any, event?: { __typename?: 'Event', id: any, type: EventtypeEnum } | null }> } | null };

export type UpdateParticipantMutationMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
  givenName?: InputMaybe<Scalars['String']['input']>;
  familyName?: InputMaybe<Scalars['String']['input']>;
  callName?: InputMaybe<Scalars['String']['input']>;
  sex?: InputMaybe<SexEnum>;
  gender?: InputMaybe<Scalars['String']['input']>;
  birthDate?: InputMaybe<Scalars['Date']['input']>;
  street?: InputMaybe<Scalars['String']['input']>;
  streetNumber?: InputMaybe<Scalars['String']['input']>;
  postalCode?: InputMaybe<Scalars['String']['input']>;
  city?: InputMaybe<Scalars['String']['input']>;
  country?: InputMaybe<Scalars['String']['input']>;
  notes?: InputMaybe<Scalars['String']['input']>;
}>;


export type UpdateParticipantMutationMutation = { __typename?: 'Mutation', updateParticipant?: { __typename?: 'Participant', id: any, givenName: string, familyName: string, callName: string, callNameDirectReadout?: string | null, sex: SexEnum, gender: string, genderDirectReadout?: string | null, birthDate: any, street: string, streetNumber: string, postalCode: string, city: string, country: string, notes?: string | null, extensions: Array<{ __typename?: 'Extension', id: any, event?: { __typename?: 'Event', id: any, type: EventtypeEnum } | null }> } | null };

export type DeleteParticipantMutationMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type DeleteParticipantMutationMutation = { __typename?: 'Mutation', deleteParticipant?: number | null };

export type ParticipantExtensionDataQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ParticipantExtensionDataQueryQuery = { __typename?: 'Query', participantExtension?: { __typename?: 'Extension', id: any, certificate: boolean, participates: boolean, emailSelf?: string | null, additionalEmails: Array<string>, telephone?: string | null, notes?: string | null, timeOfSignoff?: any | null, reasonOfSignoff?: string | null, classYear?: ClassyearEnum | null, nutrition?: NutritionEnum | null, foodRestriction?: string | null, fee?: number | null, confirmed: boolean, registrationTimestamp?: any | null, arrival?: TraveltypeEnum | null, arrivalNotes?: string | null, departure?: TraveltypeEnum | null, departureNotes?: string | null, roomPartnerWishes: Array<string>, zirkelPartnerWishes: Array<string>, medicalNotes: Array<string>, topicWishes: Array<string>, fractivityWishes: Array<string>, leavingPremise: boolean, carpoolDataSharing: boolean, removeTicks: boolean, roomNumber?: string | null, instruments: Array<string>, pool: boolean, event?: { __typename?: 'Event', type: EventtypeEnum, id: any, zirkel?: Array<{ __typename?: 'Zirkel', name: string, id: any }> | null } | null, zirkel: Array<{ __typename?: 'Zirkel', name: string, id: any }>, contacts?: Array<{ __typename?: 'Contact', id: any, name: string, telephone: string, createdAt: string }> | null } | null };

export type ParticipantExtensionPhotoQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ParticipantExtensionPhotoQueryQuery = { __typename?: 'Query', participantExtension?: { __typename?: 'Extension', id: any, signupPhoto?: string | null } | null };

export type UpdateParticipantExtensionMutationMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
  zirkelIds?: InputMaybe<Array<Scalars['Uuid']['input']> | Scalars['Uuid']['input']>;
  roomNumber?: InputMaybe<Scalars['String']['input']>;
  certificate?: InputMaybe<Scalars['Boolean']['input']>;
  participates?: InputMaybe<Scalars['Boolean']['input']>;
  emailSelf?: InputMaybe<Scalars['String']['input']>;
  additionalEmails?: InputMaybe<Array<Scalars['String']['input']> | Scalars['String']['input']>;
  reasonOfSignoff?: InputMaybe<Scalars['String']['input']>;
  classYear?: InputMaybe<ClassyearEnum>;
  nutrition?: InputMaybe<NutritionEnum>;
  foodRestriction?: InputMaybe<Scalars['String']['input']>;
  fee?: InputMaybe<Scalars['Int']['input']>;
  confirmed?: InputMaybe<Scalars['Boolean']['input']>;
  arrival?: InputMaybe<TraveltypeEnum>;
  arrivalNotes?: InputMaybe<Scalars['String']['input']>;
  departure?: InputMaybe<TraveltypeEnum>;
  departureNotes?: InputMaybe<Scalars['String']['input']>;
  roomPartnerWishes?: InputMaybe<Array<Scalars['String']['input']> | Scalars['String']['input']>;
  zirkelPartnerWishes?: InputMaybe<Array<Scalars['String']['input']> | Scalars['String']['input']>;
  medicalNotes?: InputMaybe<Array<Scalars['String']['input']> | Scalars['String']['input']>;
  topicWishes?: InputMaybe<Array<Scalars['String']['input']> | Scalars['String']['input']>;
  fractivityWishes?: InputMaybe<Array<Scalars['String']['input']> | Scalars['String']['input']>;
  instruments?: InputMaybe<Array<Scalars['String']['input']> | Scalars['String']['input']>;
  leavingPremise?: InputMaybe<Scalars['Boolean']['input']>;
  carpoolDataSharing?: InputMaybe<Scalars['Boolean']['input']>;
  removeTicks?: InputMaybe<Scalars['Boolean']['input']>;
  pool?: InputMaybe<Scalars['Boolean']['input']>;
  telephone?: InputMaybe<Scalars['String']['input']>;
  notes?: InputMaybe<Scalars['String']['input']>;
  registrationTimestamp?: InputMaybe<Scalars['LocalDateTime']['input']>;
}>;


export type UpdateParticipantExtensionMutationMutation = { __typename?: 'Mutation', updateParticipantExtension?: { __typename?: 'Extension', id: any, certificate: boolean, participates: boolean, emailSelf?: string | null, additionalEmails: Array<string>, telephone?: string | null, notes?: string | null, timeOfSignoff?: any | null, reasonOfSignoff?: string | null, classYear?: ClassyearEnum | null, nutrition?: NutritionEnum | null, foodRestriction?: string | null, fee?: number | null, confirmed: boolean, registrationTimestamp?: any | null, arrival?: TraveltypeEnum | null, arrivalNotes?: string | null, departure?: TraveltypeEnum | null, departureNotes?: string | null, roomPartnerWishes: Array<string>, zirkelPartnerWishes: Array<string>, medicalNotes: Array<string>, topicWishes: Array<string>, fractivityWishes: Array<string>, leavingPremise: boolean, carpoolDataSharing: boolean, removeTicks: boolean, roomNumber?: string | null, instruments: Array<string>, pool: boolean, zirkel: Array<{ __typename?: 'Zirkel', name: string, id: any }>, contacts?: Array<{ __typename?: 'Contact', id: any, name: string, telephone: string, createdAt: string }> | null } | null };

export type DeleteParticipantExtensionMutationMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type DeleteParticipantExtensionMutationMutation = { __typename?: 'Mutation', deleteParticipantExtension?: number | null };

export type AllHashesQueryVariables = Exact<{ [key: string]: never; }>;


export type AllHashesQuery = { __typename?: 'Query', allHashes?: Array<{ __typename?: 'ReconnectionHash', hash: string, participantId: any }> | null };

export type CurrentHashesQueryVariables = Exact<{
  birthDate: Scalars['Date']['input'];
  familyName: Scalars['String']['input'];
  callName: Scalars['String']['input'];
  givenName: Scalars['String']['input'];
}>;


export type CurrentHashesQuery = { __typename?: 'Query', tryHashes: Array<string> };

export type InstructorBaseDataQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type InstructorBaseDataQueryQuery = { __typename?: 'Query', instructor?: { __typename?: 'Instructor', id: any, givenName?: string | null, familyName?: string | null, callName: string, sex?: SexEnum | null, gender: string, birthDate?: any | null, street?: string | null, streetNumber?: string | null, postalCode?: string | null, city?: string | null, country?: string | null, email?: string | null, keycloakId: string, keycloakUsername?: string | null, telephone?: string | null, pronoun: string, abbreviation?: string | null, iban?: string | null } | null };

export type InstructorExtensionDataQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type InstructorExtensionDataQueryQuery = { __typename?: 'Query', instructorExtension?: { __typename?: 'InstructorExtension', id: any, nutrition?: NutritionEnum | null, foodRestriction?: string | null, arrival?: TraveltypeEnum | null, arrivalNotes?: string | null, departure?: TraveltypeEnum | null, departureNotes?: string | null, hasContract: boolean, contractWith?: string | null, roomNumber?: string | null, faculty?: string | null, chair?: string | null, personnelNumber?: string | null, jobTitle?: string | null, emailWork?: string | null, telephoneWork?: string | null, startOfTravel?: any | null, arrivalAtBusinessLocation?: any | null, startOfBusinessActivities?: any | null, endOfBusinessActivities?: any | null, startOfReturnJourney?: any | null, endOfTravel?: any | null, accommodationCosts: number, arrivalAtBusinessLocationDirectReadout?: any | null, startOfBusinessActivitiesDirectReadout?: any | null, startOfReturnJourneyDirectReadout?: any | null, endOfTravelDirectReadout?: any | null, accommodationCostsDirectReadout?: number | null, event?: { __typename?: 'Event', id: any, type: EventtypeEnum } | null } | null };

export type UpdateInstructorExtensionMutationMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
  nutrition?: InputMaybe<NutritionEnum>;
  foodRestriction?: InputMaybe<Scalars['String']['input']>;
  arrival?: InputMaybe<TraveltypeEnum>;
  arrivalNotes?: InputMaybe<Scalars['String']['input']>;
  departure?: InputMaybe<TraveltypeEnum>;
  departureNotes?: InputMaybe<Scalars['String']['input']>;
  hasContract?: InputMaybe<Scalars['Boolean']['input']>;
  contractWith?: InputMaybe<Scalars['String']['input']>;
  roomNumber?: InputMaybe<Scalars['String']['input']>;
  faculty?: InputMaybe<Scalars['String']['input']>;
  chair?: InputMaybe<Scalars['String']['input']>;
  personnelNumber?: InputMaybe<Scalars['String']['input']>;
  jobTitle?: InputMaybe<Scalars['String']['input']>;
  emailWork?: InputMaybe<Scalars['String']['input']>;
  telephoneWork?: InputMaybe<Scalars['String']['input']>;
  startOfTravel?: InputMaybe<Scalars['LocalDateTime']['input']>;
  arrivalAtBusinessLocation?: InputMaybe<Scalars['LocalDateTime']['input']>;
  startOfBusinessActivities?: InputMaybe<Scalars['LocalDateTime']['input']>;
  endOfBusinessActivities?: InputMaybe<Scalars['LocalDateTime']['input']>;
  startOfReturnJourney?: InputMaybe<Scalars['LocalDateTime']['input']>;
  endOfTravel?: InputMaybe<Scalars['LocalDateTime']['input']>;
  accommodationCosts?: InputMaybe<Scalars['Float']['input']>;
}>;


export type UpdateInstructorExtensionMutationMutation = { __typename?: 'Mutation', updateInstructorExtension?: { __typename?: 'InstructorExtension', id: any, nutrition?: NutritionEnum | null, foodRestriction?: string | null, arrival?: TraveltypeEnum | null, arrivalNotes?: string | null, departure?: TraveltypeEnum | null, departureNotes?: string | null, hasContract: boolean, contractWith?: string | null, roomNumber?: string | null, faculty?: string | null, chair?: string | null, personnelNumber?: string | null, jobTitle?: string | null, emailWork?: string | null, telephoneWork?: string | null, startOfTravel?: any | null, arrivalAtBusinessLocation?: any | null, startOfBusinessActivities?: any | null, endOfBusinessActivities?: any | null, startOfReturnJourney?: any | null, endOfTravel?: any | null, accommodationCosts: number, arrivalAtBusinessLocationDirectReadout?: any | null, startOfBusinessActivitiesDirectReadout?: any | null, startOfReturnJourneyDirectReadout?: any | null, endOfTravelDirectReadout?: any | null, accommodationCostsDirectReadout?: number | null, event?: { __typename?: 'Event', id: any, type: EventtypeEnum } | null } | null };

export type DeleteInstructorExtensionMutationMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type DeleteInstructorExtensionMutationMutation = { __typename?: 'Mutation', deleteInstructorExtension?: number | null };

export type InstructorExtensionZirkelQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type InstructorExtensionZirkelQueryQuery = { __typename?: 'Query', instructorExtension?: { __typename?: 'InstructorExtension', id: any, event?: { __typename?: 'Event', id: any, zirkel?: Array<{ __typename?: 'Zirkel', id: any, name: string, instructorExtensionsPublic?: Array<{ __typename?: 'InstructorExtensionPublic', id: any }> | null }> | null } | null, zirkel?: Array<{ __typename?: 'Zirkel', id: any }> | null } | null };

export type LinkInstructorAndZirkelMutationVariables = Exact<{
  zirkelId?: InputMaybe<Scalars['Uuid']['input']>;
  instructorExtensionId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type LinkInstructorAndZirkelMutation = { __typename?: 'Mutation', setInstructorExtensionToZirkelRelation?: number | null };

export type UnlinkInstructorAndZirkelMutationVariables = Exact<{
  zirkelId?: InputMaybe<Scalars['Uuid']['input']>;
  instructorExtensionId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type UnlinkInstructorAndZirkelMutation = { __typename?: 'Mutation', unsetInstructorExtensionToZirkelRelation?: number | null };

export type InstructorEventsAndZirkelQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type InstructorEventsAndZirkelQueryQuery = { __typename?: 'Query', instructor?: { __typename?: 'Instructor', id: any, extensions: Array<{ __typename?: 'InstructorExtension', id: any, event?: { __typename?: 'Event', id: any, name: string, year: number } | null, zirkel?: Array<{ __typename?: 'Zirkel', id: any, name: string, event?: { __typename?: 'Event', year: number, name: string, id: any } | null }> | null }> } | null };

export type EventSpecificInstructorsQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type EventSpecificInstructorsQuery = { __typename?: 'Query', event?: { __typename?: 'Event', id: any, instructorExtensionsPublic?: Array<{ __typename?: 'InstructorExtensionPublic', id: any, instructorId: any, name: string, gender: string, roomNumber?: string | null, zirkel?: Array<{ __typename?: 'Zirkel', id: any, name: string }> | null }> | null } | null };

export type ZirkelSpecificInstructorsQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ZirkelSpecificInstructorsQuery = { __typename?: 'Query', zirkel?: { __typename?: 'Zirkel', id: any, instructorExtensionsPublic?: Array<{ __typename?: 'InstructorExtensionPublic', id: any, instructorId: any, name: string, gender: string, roomNumber?: string | null, zirkel?: Array<{ __typename?: 'Zirkel', id: any, name: string }> | null }> | null } | null };

export type UpdateInstructorOrgaFieldsMutationMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
  roomNumber?: InputMaybe<Scalars['String']['input']>;
}>;


export type UpdateInstructorOrgaFieldsMutationMutation = { __typename?: 'Mutation', updateInstructorExtension?: { __typename?: 'InstructorExtension', id: any } | null };

export type EventSpecificParticipantsQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type EventSpecificParticipantsQuery = { __typename?: 'Query', event?: { __typename?: 'Event', id: any, type: EventtypeEnum, extensions: Array<{ __typename?: 'Extension', id: any, classYear?: ClassyearEnum | null, roomNumber?: string | null, zirkelIds: Array<any>, confirmed: boolean, registrationTimestamp?: any | null, certificate: boolean, participates: boolean, emailSelf?: string | null, additionalEmails: Array<string>, reasonOfSignoff?: string | null, timeOfSignoff?: any | null, participant?: { __typename?: 'Participant', id: any, callName: string, familyName: string, sex: SexEnum, gender: string } | null, zirkel: Array<{ __typename?: 'Zirkel', name: string, id: any }>, contacts?: Array<{ __typename?: 'Contact', telephone: string }> | null }> } | null };

export type ZirkelSpecificParticipantsQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ZirkelSpecificParticipantsQuery = { __typename?: 'Query', zirkel?: { __typename?: 'Zirkel', id: any, extensions: Array<{ __typename?: 'Extension', id: any, classYear?: ClassyearEnum | null, roomNumber?: string | null, zirkelIds: Array<any>, confirmed: boolean, registrationTimestamp?: any | null, certificate: boolean, participates: boolean, emailSelf?: string | null, additionalEmails: Array<string>, reasonOfSignoff?: string | null, timeOfSignoff?: any | null, participant?: { __typename?: 'Participant', id: any, callName: string, familyName: string, sex: SexEnum, gender: string } | null, zirkel: Array<{ __typename?: 'Zirkel', name: string, id: any }>, contacts?: Array<{ __typename?: 'Contact', telephone: string }> | null }> } | null };

export type ListOfZirkelQueryVariables = Exact<{
  event_id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ListOfZirkelQuery = { __typename?: 'Query', event?: { __typename?: 'Event', id: any, zirkel?: Array<{ __typename?: 'Zirkel', id: any, name: string }> | null } | null };

export type UpdateOrgaFieldsMutationMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
  zirkelIds?: InputMaybe<Array<Scalars['Uuid']['input']> | Scalars['Uuid']['input']>;
  roomNumber?: InputMaybe<Scalars['String']['input']>;
  confirmed?: InputMaybe<Scalars['Boolean']['input']>;
  certificate?: InputMaybe<Scalars['Boolean']['input']>;
  participates?: InputMaybe<Scalars['Boolean']['input']>;
}>;


export type UpdateOrgaFieldsMutationMutation = { __typename?: 'Mutation', updateParticipantExtension?: { __typename?: 'Extension', id: any, zirkelIds: Array<any>, roomNumber?: string | null, confirmed: boolean, certificate: boolean, participates: boolean, zirkel: Array<{ __typename?: 'Zirkel', name: string, id: any }> } | null };

export type ParticipantExtensionContactsQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ParticipantExtensionContactsQueryQuery = { __typename?: 'Query', participantExtension?: { __typename?: 'Extension', id: any, contacts?: Array<{ __typename?: 'Contact', id: any, name: string, telephone: string, createdAt: string }> | null } | null };

export type CreateContactMutationVariables = Exact<{
  extensionId?: InputMaybe<Scalars['Uuid']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  telephone?: InputMaybe<Scalars['String']['input']>;
}>;


export type CreateContactMutation = { __typename?: 'Mutation', createContact?: { __typename?: 'Contact', id: any, extension?: { __typename?: 'Extension', id: any, contacts?: Array<{ __typename?: 'Contact', id: any, createdAt: string }> | null } | null } | null };

export type UpdateContactMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  telephone?: InputMaybe<Scalars['String']['input']>;
}>;


export type UpdateContactMutation = { __typename?: 'Mutation', updateContact?: { __typename?: 'Contact', id: any, extension?: { __typename?: 'Extension', id: any, contacts?: Array<{ __typename?: 'Contact', id: any, name: string, telephone: string, createdAt: string }> | null } | null } | null };

export type DeleteContactMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type DeleteContactMutation = { __typename?: 'Mutation', deleteContact?: number | null };

export type ZirkelPlanMetaQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ZirkelPlanMetaQueryQuery = { __typename?: 'Query', event?: { __typename?: 'Event', id: any, year: number, zirkel?: Array<{ __typename?: 'Zirkel', id: any, name: string, room?: string | null, topicWishes: Array<string>, numberOfParticipants: number }> | null, instructorExtensionsPublic?: Array<{ __typename?: 'InstructorExtensionPublic', id: any, name: string }> | null } | null, allEvents?: Array<{ __typename?: 'Event', id: any, year: number, zirkel?: Array<{ __typename?: 'Zirkel', id: any, name: string, topics: Array<string> }> | null }> | null };

export type ZirkelPlanSlotsQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ZirkelPlanSlotsQueryQuery = { __typename?: 'Query', event?: { __typename?: 'Event', id: any, zirkelPlanSlots?: Array<{ __typename?: 'ZirkelPlanSlot', id: any, name: string, startTime: any, endTime: any, certificate: boolean, beamer: boolean, zirkelPlanEntries?: Array<{ __typename?: 'ZirkelPlanEntry', id: any, referenceEntryId?: any | null, zirkelPlanSlotId: any, zirkelId: any, topic?: string | null, notes?: string | null, beamer: boolean, color: string, instructorExtensionsPublic?: Array<{ __typename?: 'InstructorExtensionPublic', id: any, name: string, instructorId: any }> | null }> | null }> | null } | null };

export type CreateZirkelPlanEntryMutationVariables = Exact<{
  zirkelId?: InputMaybe<Scalars['Uuid']['input']>;
  zirkelPlanSlotId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type CreateZirkelPlanEntryMutation = { __typename?: 'Mutation', createZirkelPlanEntry?: { __typename?: 'ZirkelPlanEntry', id: any, zirkelPlanSlotId: any, zirkelId: any } | null };

export type DeleteZirkelPlanEntryMutationVariables = Exact<{
  zirkelPlanEntryId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type DeleteZirkelPlanEntryMutation = { __typename?: 'Mutation', deleteZirkelPlanEntry?: number | null };

export type UpdateZirkelPlanEntryMutationVariables = Exact<{
  zirkelPlanEntryId?: InputMaybe<Scalars['Uuid']['input']>;
  beamer?: InputMaybe<Scalars['Boolean']['input']>;
  topic?: InputMaybe<Scalars['String']['input']>;
  notes?: InputMaybe<Scalars['String']['input']>;
}>;


export type UpdateZirkelPlanEntryMutation = { __typename?: 'Mutation', updateZirkelPlanEntry?: { __typename?: 'ZirkelPlanEntry', id: any, topic?: string | null, beamer: boolean, notes?: string | null } | null };

export type AddInstructorExtensionToEntryMutationVariables = Exact<{
  zirkelPlanEntryId?: InputMaybe<Scalars['Uuid']['input']>;
  instructorExtensionId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type AddInstructorExtensionToEntryMutation = { __typename?: 'Mutation', addInstructorExtensionToZirkelPlanEntry?: number | null };

export type RemoveInstructorExtensionFromEntryMutationVariables = Exact<{
  zirkelPlanEntryId?: InputMaybe<Scalars['Uuid']['input']>;
  instructorExtensionId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type RemoveInstructorExtensionFromEntryMutation = { __typename?: 'Mutation', removeInstructorExtensionFromZirkelPlanEntry?: number | null };

export type LinkEntriesMutationVariables = Exact<{
  sourceId?: InputMaybe<Scalars['Uuid']['input']>;
  targetId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type LinkEntriesMutation = { __typename?: 'Mutation', linkZirkelPlanEntry?: number | null };

export type UnLinkEntriesMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type UnLinkEntriesMutation = { __typename?: 'Mutation', unlinkZirkelPlanEntry?: number | null };

export type CreateZirkelPlanSlotMutationVariables = Exact<{
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type CreateZirkelPlanSlotMutation = { __typename?: 'Mutation', createZirkelPlanSlot?: { __typename?: 'ZirkelPlanSlot', id: any, name: string, startTime: any, endTime: any, certificate: boolean, beamer: boolean } | null };

export type DeleteZirkelPlanSlotMutationVariables = Exact<{
  zirkelPlanSlotId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type DeleteZirkelPlanSlotMutation = { __typename?: 'Mutation', deleteZirkelPlanSlot?: number | null };

export type UpdateZirkelPlanSlotMutationVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
  beamer?: InputMaybe<Scalars['Boolean']['input']>;
  certificate?: InputMaybe<Scalars['Boolean']['input']>;
  endTime?: InputMaybe<Scalars['LocalDateTime']['input']>;
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  startTime?: InputMaybe<Scalars['LocalDateTime']['input']>;
}>;


export type UpdateZirkelPlanSlotMutation = { __typename?: 'Mutation', updateZirkelPlanSlot?: { __typename?: 'ZirkelPlanSlot', id: any, name: string, startTime: any, endTime: any, certificate: boolean, beamer: boolean, zirkelPlanEntries?: Array<{ __typename?: 'ZirkelPlanEntry', id: any, referenceEntryId?: any | null, zirkelPlanSlotId: any, zirkelId: any, topic?: string | null, notes?: string | null, beamer: boolean, color: string, instructorExtensionsPublic?: Array<{ __typename?: 'InstructorExtensionPublic', id: any, name: string, instructorId: any }> | null }> | null } | null };

export type ZirkelQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type ZirkelQuery = { __typename?: 'Query', zirkel?: { __typename?: 'Zirkel', id: any, name: string, room?: string | null, topics: Array<string>, instructorExtensions: Array<{ __typename?: 'InstructorExtension', id: any, instructor?: { __typename?: 'Instructor', id: any, callName: string, familyName?: string | null } | null }>, event?: { __typename?: 'Event', id: any, type: EventtypeEnum } | null } | null };

export type EventQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type EventQuery = { __typename?: 'Query', event?: { __typename?: 'Event', id: any, name: string, startDate: any, type: EventtypeEnum, year: number, endDate: any, defaultFee: number, statistics: string, giveInstructorsFullReadAccess: boolean, zirkel?: Array<{ __typename?: 'Zirkel', id: any, name: string }> | null } | null };

export type NamesSelectionQueryVariables = Exact<{
  zirkelId?: InputMaybe<Scalars['Uuid']['input']>;
  eventId?: InputMaybe<Scalars['Uuid']['input']>;
  participantId?: InputMaybe<Scalars['Uuid']['input']>;
  instructorId?: InputMaybe<Scalars['Uuid']['input']>;
}>;


export type NamesSelectionQuery = { __typename?: 'Query', zirkel?: { __typename?: 'Zirkel', id: any, name: string, event?: { __typename?: 'Event', id: any, name: string, year: number } | null } | null, event?: { __typename?: 'Event', id: any, name: string, year: number } | null, participant?: { __typename?: 'Participant', id: any, callName: string, familyName: string } | null, instructor?: { __typename?: 'Instructor', id: any, callName: string, familyName?: string | null } | null };


export const CreateParticipantDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateParticipant"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"sex"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"SexEnum"}},"defaultValue":{"kind":"EnumValue","value":"MALE"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"street"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"streetNumber"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"postalCode"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"givenName"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"gender"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"familyName"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"country"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"city"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"callName"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"birthDate"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Date"}},"defaultValue":{"kind":"StringValue","value":"0001-01-01","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createParticipant"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"givenName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"givenName"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"familyName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"familyName"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"callName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"callName"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"sex"},"value":{"kind":"Variable","name":{"kind":"Name","value":"sex"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"gender"},"value":{"kind":"Variable","name":{"kind":"Name","value":"gender"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"birthDate"},"value":{"kind":"Variable","name":{"kind":"Name","value":"birthDate"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"street"},"value":{"kind":"Variable","name":{"kind":"Name","value":"street"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"streetNumber"},"value":{"kind":"Variable","name":{"kind":"Name","value":"streetNumber"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"postalCode"},"value":{"kind":"Variable","name":{"kind":"Name","value":"postalCode"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"city"},"value":{"kind":"Variable","name":{"kind":"Name","value":"city"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"country"},"value":{"kind":"Variable","name":{"kind":"Name","value":"country"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]} as unknown as DocumentNode<CreateParticipantMutation, CreateParticipantMutationVariables>;
export const SignupExtensionsQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"SignupExtensionsQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"signupExtensions"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"eventId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"participantId"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"hasSignedUp"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"hasSignupPhoto"}}]}}]}}]} as unknown as DocumentNode<SignupExtensionsQueryQuery, SignupExtensionsQueryQueryVariables>;
export const EventNameQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"eventNameQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"event"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"year"}}]}}]}}]} as unknown as DocumentNode<EventNameQueryQuery, EventNameQueryQueryVariables>;
export const SignupSelectionQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"SignupSelectionQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"signupExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"hasSignedUp"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"hasSignupPhoto"}},{"kind":"Field","name":{"kind":"Name","value":"signupPhoto"}},{"kind":"Field","name":{"kind":"Name","value":"roomNumber"}}]}}]}}]} as unknown as DocumentNode<SignupSelectionQueryQuery, SignupSelectionQueryQueryVariables>;
export const UpdateSignupMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateSignupMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"hasSignedUp"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signupPhoto"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateParticipantSignupExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"hasSignedUp"},"value":{"kind":"Variable","name":{"kind":"Name","value":"hasSignedUp"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"signupPhoto"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signupPhoto"}}}]}},{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"signupPhoto"}},{"kind":"Field","name":{"kind":"Name","value":"participantId"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"hasSignedUp"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"hasSignupPhoto"}},{"kind":"Field","name":{"kind":"Name","value":"roomNumber"}}]}}]}}]} as unknown as DocumentNode<UpdateSignupMutationMutation, UpdateSignupMutationMutationVariables>;
export const EventsAndZirkelNamesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"EventsAndZirkelNames"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"allEvents"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"year"}},{"kind":"Field","name":{"kind":"Name","value":"startDate"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]} as unknown as DocumentNode<EventsAndZirkelNamesQuery, EventsAndZirkelNamesQueryVariables>;
export const FractivityPlanMetaQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"FractivityPlanMetaQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"event"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"startDate"}},{"kind":"Field","name":{"kind":"Name","value":"endDate"}},{"kind":"Field","name":{"kind":"Name","value":"firstFractivityTime"}},{"kind":"Field","name":{"kind":"Name","value":"lastFractivityTime"}},{"kind":"Field","name":{"kind":"Name","value":"minIntervalsFractivity"}},{"kind":"Field","name":{"kind":"Name","value":"blockedFractivitySlots"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"startBlock"}},{"kind":"Field","name":{"kind":"Name","value":"endBlock"}},{"kind":"Field","name":{"kind":"Name","value":"nameBlock"}}]}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensionsPublic"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"instructorId"}}]}},{"kind":"Field","name":{"kind":"Name","value":"fractivityRooms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomTypes"}}]}}]}}]}}]} as unknown as DocumentNode<FractivityPlanMetaQueryQuery, FractivityPlanMetaQueryQueryVariables>;
export const FractivityPlanQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"FractivityPlanQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"event"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"fractivityEntries"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"topic"}},{"kind":"Field","name":{"kind":"Name","value":"startDay"}},{"kind":"Field","name":{"kind":"Name","value":"preparationTime"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"followUpTime"}},{"kind":"Field","name":{"kind":"Name","value":"eventId"}},{"kind":"Field","name":{"kind":"Name","value":"duration"}},{"kind":"Field","name":{"kind":"Name","value":"allowedStarts"}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensionsPublic"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"instructorId"}}]}},{"kind":"Field","name":{"kind":"Name","value":"fractivityDistribution"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomId"}},{"kind":"Field","name":{"kind":"Name","value":"startTime"}},{"kind":"Field","name":{"kind":"Name","value":"color"}},{"kind":"Field","name":{"kind":"Name","value":"room"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomTypes"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"allowedRooms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomTypes"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"fractivityDistributions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomId"}},{"kind":"Field","name":{"kind":"Name","value":"startTime"}},{"kind":"Field","name":{"kind":"Name","value":"color"}},{"kind":"Field","name":{"kind":"Name","value":"room"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomTypes"}}]}},{"kind":"Field","name":{"kind":"Name","value":"fractivityEntry"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"topic"}},{"kind":"Field","name":{"kind":"Name","value":"startDay"}},{"kind":"Field","name":{"kind":"Name","value":"preparationTime"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"followUpTime"}},{"kind":"Field","name":{"kind":"Name","value":"eventId"}},{"kind":"Field","name":{"kind":"Name","value":"duration"}},{"kind":"Field","name":{"kind":"Name","value":"allowedStarts"}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensionsPublic"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"instructorId"}}]}}]}}]}}]}}]}}]} as unknown as DocumentNode<FractivityPlanQueryQuery, FractivityPlanQueryQueryVariables>;
export const CreateFractivityEntryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateFractivityEntry"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"preparationTime"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"followUpTime"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"topic"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"startDay"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Date"}},"defaultValue":{"kind":"StringValue","value":"0001-01-01","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionUuids"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}}}},"defaultValue":{"kind":"ListValue","values":[]}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"duration"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"60"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"allowedStarts"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}},"defaultValue":{"kind":"ListValue","values":[]}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"allowedRooms"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}}}},"defaultValue":{"kind":"ListValue","values":[]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createFractivityEntry"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"eventId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"instructorExtensionUuids"},"value":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionUuids"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"topic"},"value":{"kind":"Variable","name":{"kind":"Name","value":"topic"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"duration"},"value":{"kind":"Variable","name":{"kind":"Name","value":"duration"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"allowedRooms"},"value":{"kind":"Variable","name":{"kind":"Name","value":"allowedRooms"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"allowedStarts"},"value":{"kind":"Variable","name":{"kind":"Name","value":"allowedStarts"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"followUpTime"},"value":{"kind":"Variable","name":{"kind":"Name","value":"followUpTime"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"startDay"},"value":{"kind":"Variable","name":{"kind":"Name","value":"startDay"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"preparationTime"},"value":{"kind":"Variable","name":{"kind":"Name","value":"preparationTime"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"topic"}},{"kind":"Field","name":{"kind":"Name","value":"startDay"}},{"kind":"Field","name":{"kind":"Name","value":"preparationTime"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"followUpTime"}},{"kind":"Field","name":{"kind":"Name","value":"eventId"}},{"kind":"Field","name":{"kind":"Name","value":"duration"}},{"kind":"Field","name":{"kind":"Name","value":"allowedStarts"}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensionsPublic"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"instructorId"}}]}},{"kind":"Field","name":{"kind":"Name","value":"fractivityDistribution"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomId"}},{"kind":"Field","name":{"kind":"Name","value":"startTime"}},{"kind":"Field","name":{"kind":"Name","value":"room"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomTypes"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"allowedRooms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomTypes"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]} as unknown as DocumentNode<CreateFractivityEntryMutation, CreateFractivityEntryMutationVariables>;
export const DeleteFractivityPlanEntryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"DeleteFractivityPlanEntry"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"fractivityEntryId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deleteFractivityEntry"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"fractivityEntryId"}}}]}]}}]} as unknown as DocumentNode<DeleteFractivityPlanEntryMutation, DeleteFractivityPlanEntryMutationVariables>;
export const DeleteFractivityPlanDistributionDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"DeleteFractivityPlanDistribution"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"fractivityDistributionId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deleteFractivityDistribution"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"fractivityDistributionId"}}}]}]}}]} as unknown as DocumentNode<DeleteFractivityPlanDistributionMutation, DeleteFractivityPlanDistributionMutationVariables>;
export const UpdateFractivityEntryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateFractivityEntry"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"fractivityEntryId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"topic"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"startDay"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Date"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"preparationTime"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionUuids"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}}}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"followUpTime"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"duration"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"allowedStarts"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"allowedRooms"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}}}},"defaultValue":{"kind":"NullValue"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateFractivityEntry"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"fractivityEntryId"}}},{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"eventId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"instructorExtensionUuids"},"value":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionUuids"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"topic"},"value":{"kind":"Variable","name":{"kind":"Name","value":"topic"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"duration"},"value":{"kind":"Variable","name":{"kind":"Name","value":"duration"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"allowedRooms"},"value":{"kind":"Variable","name":{"kind":"Name","value":"allowedRooms"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"allowedStarts"},"value":{"kind":"Variable","name":{"kind":"Name","value":"allowedStarts"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"followUpTime"},"value":{"kind":"Variable","name":{"kind":"Name","value":"followUpTime"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"startDay"},"value":{"kind":"Variable","name":{"kind":"Name","value":"startDay"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"preparationTime"},"value":{"kind":"Variable","name":{"kind":"Name","value":"preparationTime"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"topic"}},{"kind":"Field","name":{"kind":"Name","value":"startDay"}},{"kind":"Field","name":{"kind":"Name","value":"preparationTime"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"followUpTime"}},{"kind":"Field","name":{"kind":"Name","value":"eventId"}},{"kind":"Field","name":{"kind":"Name","value":"duration"}},{"kind":"Field","name":{"kind":"Name","value":"allowedStarts"}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensionsPublic"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"instructorId"}}]}},{"kind":"Field","name":{"kind":"Name","value":"fractivityDistribution"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomId"}},{"kind":"Field","name":{"kind":"Name","value":"startTime"}},{"kind":"Field","name":{"kind":"Name","value":"room"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomTypes"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"allowedRooms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"roomTypes"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]} as unknown as DocumentNode<UpdateFractivityEntryMutation, UpdateFractivityEntryMutationVariables>;
export const DistributeRestMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"DistributeRestMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"fractivityIds"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}}}},"defaultValue":{"kind":"ListValue","values":[]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"distributeNondistributedFractivities"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"fractivityIds"},"value":{"kind":"Variable","name":{"kind":"Name","value":"fractivityIds"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]} as unknown as DocumentNode<DistributeRestMutationMutation, DistributeRestMutationMutationVariables>;
export const DistributeAllMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"DistributeAllMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"fractivityIds"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}}}},"defaultValue":{"kind":"ListValue","values":[]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"distributeFractivities"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"fractivityIds"},"value":{"kind":"Variable","name":{"kind":"Name","value":"fractivityIds"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]} as unknown as DocumentNode<DistributeAllMutationMutation, DistributeAllMutationMutationVariables>;
export const InstructorQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"InstructorQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"instructor"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"year"}},{"kind":"Field","name":{"kind":"Name","value":"startDate"}}]}}]}}]}}]}}]} as unknown as DocumentNode<InstructorQueryQuery, InstructorQueryQueryVariables>;
export const InstructorsSearchDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"InstructorsSearch"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"allInstructors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"telephone"}}]}}]}}]} as unknown as DocumentNode<InstructorsSearchQuery, InstructorsSearchQueryVariables>;
export const ParticipantQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ParticipantQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"participant"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"year"}},{"kind":"Field","name":{"kind":"Name","value":"startDate"}}]}}]}}]}}]}}]} as unknown as DocumentNode<ParticipantQueryQuery, ParticipantQueryQueryVariables>;
export const ParticipantsSearchDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ParticipantsSearch"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"allParticipants"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"sex"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"emailSelf"}},{"kind":"Field","name":{"kind":"Name","value":"additionalEmails"}},{"kind":"Field","name":{"kind":"Name","value":"contacts"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"telephone"}}]}}]}}]}}]}}]} as unknown as DocumentNode<ParticipantsSearchQuery, ParticipantsSearchQueryVariables>;
export const InstructorEventsQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"InstructorEventsQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"instructor"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]}}]} as unknown as DocumentNode<InstructorEventsQueryQuery, InstructorEventsQueryQueryVariables>;
export const EventNamesAndIdsForInstructorsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"EventNamesAndIdsForInstructors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"allEvents"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"year"}},{"kind":"Field","name":{"kind":"Name","value":"startDate"}}]}}]}}]} as unknown as DocumentNode<EventNamesAndIdsForInstructorsQuery, EventNamesAndIdsForInstructorsQueryVariables>;
export const AddEventToInstructorMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"AddEventToInstructorMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"instructorId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createInstructorExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"instructorId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"instructorId"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"eventId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"instructor"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]}}]} as unknown as DocumentNode<AddEventToInstructorMutationMutation, AddEventToInstructorMutationMutationVariables>;
export const ParticipantEventsQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ParticipantEventsQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"participant"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]}}]} as unknown as DocumentNode<ParticipantEventsQueryQuery, ParticipantEventsQueryQueryVariables>;
export const EventNamesAndIdsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"EventNamesAndIds"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"allEvents"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"year"}},{"kind":"Field","name":{"kind":"Name","value":"startDate"}},{"kind":"Field","name":{"kind":"Name","value":"type"}}]}}]}}]} as unknown as DocumentNode<EventNamesAndIdsQuery, EventNamesAndIdsQueryVariables>;
export const AddEventToParticipantMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"AddEventToParticipantMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"participantId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createParticipantExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"participantId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"participantId"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"eventId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"participant"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]}}]} as unknown as DocumentNode<AddEventToParticipantMutationMutation, AddEventToParticipantMutationMutationVariables>;
export const ParticipantBaseDataQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ParticipantBaseDataQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"participant"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"callNameDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"sex"}},{"kind":"Field","name":{"kind":"Name","value":"gender"}},{"kind":"Field","name":{"kind":"Name","value":"genderDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"birthDate"}},{"kind":"Field","name":{"kind":"Name","value":"street"}},{"kind":"Field","name":{"kind":"Name","value":"streetNumber"}},{"kind":"Field","name":{"kind":"Name","value":"postalCode"}},{"kind":"Field","name":{"kind":"Name","value":"city"}},{"kind":"Field","name":{"kind":"Name","value":"country"}},{"kind":"Field","name":{"kind":"Name","value":"notes"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"type"}}]}}]}}]}}]}}]} as unknown as DocumentNode<ParticipantBaseDataQueryQuery, ParticipantBaseDataQueryQueryVariables>;
export const UpdateParticipantMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateParticipantMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"givenName"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"familyName"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"callName"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"sex"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"SexEnum"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"gender"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"birthDate"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Date"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"street"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"streetNumber"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"postalCode"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"city"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"country"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"notes"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateParticipant"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"givenName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"givenName"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"familyName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"familyName"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"callName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"callName"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"sex"},"value":{"kind":"Variable","name":{"kind":"Name","value":"sex"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"gender"},"value":{"kind":"Variable","name":{"kind":"Name","value":"gender"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"birthDate"},"value":{"kind":"Variable","name":{"kind":"Name","value":"birthDate"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"street"},"value":{"kind":"Variable","name":{"kind":"Name","value":"street"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"streetNumber"},"value":{"kind":"Variable","name":{"kind":"Name","value":"streetNumber"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"postalCode"},"value":{"kind":"Variable","name":{"kind":"Name","value":"postalCode"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"city"},"value":{"kind":"Variable","name":{"kind":"Name","value":"city"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"country"},"value":{"kind":"Variable","name":{"kind":"Name","value":"country"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"notes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"notes"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"callNameDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"sex"}},{"kind":"Field","name":{"kind":"Name","value":"gender"}},{"kind":"Field","name":{"kind":"Name","value":"genderDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"birthDate"}},{"kind":"Field","name":{"kind":"Name","value":"street"}},{"kind":"Field","name":{"kind":"Name","value":"streetNumber"}},{"kind":"Field","name":{"kind":"Name","value":"postalCode"}},{"kind":"Field","name":{"kind":"Name","value":"city"}},{"kind":"Field","name":{"kind":"Name","value":"country"}},{"kind":"Field","name":{"kind":"Name","value":"notes"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"type"}}]}}]}}]}}]}}]} as unknown as DocumentNode<UpdateParticipantMutationMutation, UpdateParticipantMutationMutationVariables>;
export const DeleteParticipantMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"DeleteParticipantMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deleteParticipant"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<DeleteParticipantMutationMutation, DeleteParticipantMutationMutationVariables>;
export const ParticipantExtensionDataQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ParticipantExtensionDataQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"participantExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"contacts"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"telephone"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"certificate"}},{"kind":"Field","name":{"kind":"Name","value":"participates"}},{"kind":"Field","name":{"kind":"Name","value":"emailSelf"}},{"kind":"Field","name":{"kind":"Name","value":"additionalEmails"}},{"kind":"Field","name":{"kind":"Name","value":"telephone"}},{"kind":"Field","name":{"kind":"Name","value":"notes"}},{"kind":"Field","name":{"kind":"Name","value":"timeOfSignoff"}},{"kind":"Field","name":{"kind":"Name","value":"reasonOfSignoff"}},{"kind":"Field","name":{"kind":"Name","value":"classYear"}},{"kind":"Field","name":{"kind":"Name","value":"nutrition"}},{"kind":"Field","name":{"kind":"Name","value":"foodRestriction"}},{"kind":"Field","name":{"kind":"Name","value":"fee"}},{"kind":"Field","name":{"kind":"Name","value":"confirmed"}},{"kind":"Field","name":{"kind":"Name","value":"registrationTimestamp"}},{"kind":"Field","name":{"kind":"Name","value":"arrival"}},{"kind":"Field","name":{"kind":"Name","value":"arrivalNotes"}},{"kind":"Field","name":{"kind":"Name","value":"departure"}},{"kind":"Field","name":{"kind":"Name","value":"departureNotes"}},{"kind":"Field","name":{"kind":"Name","value":"roomPartnerWishes"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelPartnerWishes"}},{"kind":"Field","name":{"kind":"Name","value":"medicalNotes"}},{"kind":"Field","name":{"kind":"Name","value":"topicWishes"}},{"kind":"Field","name":{"kind":"Name","value":"fractivityWishes"}},{"kind":"Field","name":{"kind":"Name","value":"leavingPremise"}},{"kind":"Field","name":{"kind":"Name","value":"carpoolDataSharing"}},{"kind":"Field","name":{"kind":"Name","value":"removeTicks"}},{"kind":"Field","name":{"kind":"Name","value":"roomNumber"}},{"kind":"Field","name":{"kind":"Name","value":"instruments"}},{"kind":"Field","name":{"kind":"Name","value":"pool"}}]}}]}}]} as unknown as DocumentNode<ParticipantExtensionDataQueryQuery, ParticipantExtensionDataQueryQueryVariables>;
export const ParticipantExtensionPhotoQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ParticipantExtensionPhotoQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"participantExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"signupPhoto"}}]}}]}}]} as unknown as DocumentNode<ParticipantExtensionPhotoQueryQuery, ParticipantExtensionPhotoQueryQueryVariables>;
export const UpdateParticipantExtensionMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateParticipantExtensionMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelIds"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}}}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"roomNumber"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"certificate"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"participates"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"emailSelf"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"additionalEmails"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"reasonOfSignoff"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"classYear"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ClassyearEnum"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"nutrition"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"NutritionEnum"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"foodRestriction"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"fee"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"confirmed"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"arrival"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"TraveltypeEnum"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"arrivalNotes"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"departure"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"TraveltypeEnum"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"departureNotes"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"roomPartnerWishes"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPartnerWishes"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"medicalNotes"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"topicWishes"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"fractivityWishes"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"instruments"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"leavingPremise"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"carpoolDataSharing"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"removeTicks"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"pool"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"telephone"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"notes"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"registrationTimestamp"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"LocalDateTime"}},"defaultValue":{"kind":"NullValue"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateParticipantExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"zirkelIds"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelIds"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"roomNumber"},"value":{"kind":"Variable","name":{"kind":"Name","value":"roomNumber"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"certificate"},"value":{"kind":"Variable","name":{"kind":"Name","value":"certificate"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"participates"},"value":{"kind":"Variable","name":{"kind":"Name","value":"participates"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"emailSelf"},"value":{"kind":"Variable","name":{"kind":"Name","value":"emailSelf"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"additionalEmails"},"value":{"kind":"Variable","name":{"kind":"Name","value":"additionalEmails"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"reasonOfSignoff"},"value":{"kind":"Variable","name":{"kind":"Name","value":"reasonOfSignoff"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"classYear"},"value":{"kind":"Variable","name":{"kind":"Name","value":"classYear"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"nutrition"},"value":{"kind":"Variable","name":{"kind":"Name","value":"nutrition"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"foodRestriction"},"value":{"kind":"Variable","name":{"kind":"Name","value":"foodRestriction"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"fee"},"value":{"kind":"Variable","name":{"kind":"Name","value":"fee"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"confirmed"},"value":{"kind":"Variable","name":{"kind":"Name","value":"confirmed"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"arrival"},"value":{"kind":"Variable","name":{"kind":"Name","value":"arrival"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"arrivalNotes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"arrivalNotes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"departure"},"value":{"kind":"Variable","name":{"kind":"Name","value":"departure"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"departureNotes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"departureNotes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"roomPartnerWishes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"roomPartnerWishes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"zirkelPartnerWishes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPartnerWishes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"medicalNotes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"medicalNotes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"topicWishes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"topicWishes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"fractivityWishes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"fractivityWishes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"instruments"},"value":{"kind":"Variable","name":{"kind":"Name","value":"instruments"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"leavingPremise"},"value":{"kind":"Variable","name":{"kind":"Name","value":"leavingPremise"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"carpoolDataSharing"},"value":{"kind":"Variable","name":{"kind":"Name","value":"carpoolDataSharing"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"removeTicks"},"value":{"kind":"Variable","name":{"kind":"Name","value":"removeTicks"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"pool"},"value":{"kind":"Variable","name":{"kind":"Name","value":"pool"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"telephone"},"value":{"kind":"Variable","name":{"kind":"Name","value":"telephone"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"notes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"notes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"registrationTimestamp"},"value":{"kind":"Variable","name":{"kind":"Name","value":"registrationTimestamp"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"contacts"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"telephone"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"certificate"}},{"kind":"Field","name":{"kind":"Name","value":"participates"}},{"kind":"Field","name":{"kind":"Name","value":"emailSelf"}},{"kind":"Field","name":{"kind":"Name","value":"additionalEmails"}},{"kind":"Field","name":{"kind":"Name","value":"telephone"}},{"kind":"Field","name":{"kind":"Name","value":"notes"}},{"kind":"Field","name":{"kind":"Name","value":"timeOfSignoff"}},{"kind":"Field","name":{"kind":"Name","value":"reasonOfSignoff"}},{"kind":"Field","name":{"kind":"Name","value":"classYear"}},{"kind":"Field","name":{"kind":"Name","value":"nutrition"}},{"kind":"Field","name":{"kind":"Name","value":"foodRestriction"}},{"kind":"Field","name":{"kind":"Name","value":"fee"}},{"kind":"Field","name":{"kind":"Name","value":"confirmed"}},{"kind":"Field","name":{"kind":"Name","value":"registrationTimestamp"}},{"kind":"Field","name":{"kind":"Name","value":"arrival"}},{"kind":"Field","name":{"kind":"Name","value":"arrivalNotes"}},{"kind":"Field","name":{"kind":"Name","value":"departure"}},{"kind":"Field","name":{"kind":"Name","value":"departureNotes"}},{"kind":"Field","name":{"kind":"Name","value":"roomPartnerWishes"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelPartnerWishes"}},{"kind":"Field","name":{"kind":"Name","value":"medicalNotes"}},{"kind":"Field","name":{"kind":"Name","value":"topicWishes"}},{"kind":"Field","name":{"kind":"Name","value":"fractivityWishes"}},{"kind":"Field","name":{"kind":"Name","value":"leavingPremise"}},{"kind":"Field","name":{"kind":"Name","value":"carpoolDataSharing"}},{"kind":"Field","name":{"kind":"Name","value":"removeTicks"}},{"kind":"Field","name":{"kind":"Name","value":"roomNumber"}},{"kind":"Field","name":{"kind":"Name","value":"instruments"}},{"kind":"Field","name":{"kind":"Name","value":"pool"}}]}}]}}]} as unknown as DocumentNode<UpdateParticipantExtensionMutationMutation, UpdateParticipantExtensionMutationMutationVariables>;
export const DeleteParticipantExtensionMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"DeleteParticipantExtensionMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deleteParticipantExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<DeleteParticipantExtensionMutationMutation, DeleteParticipantExtensionMutationMutationVariables>;
export const AllHashesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"AllHashes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"allHashes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"participantId"}}]}}]}}]} as unknown as DocumentNode<AllHashesQuery, AllHashesQueryVariables>;
export const CurrentHashesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"CurrentHashes"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"birthDate"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Date"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"familyName"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"callName"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"givenName"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"tryHashes"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"birthDate"},"value":{"kind":"Variable","name":{"kind":"Name","value":"birthDate"}}},{"kind":"Argument","name":{"kind":"Name","value":"familyName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"familyName"}}},{"kind":"Argument","name":{"kind":"Name","value":"givenName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"givenName"}}},{"kind":"Argument","name":{"kind":"Name","value":"callName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"callName"}}}]}]}}]} as unknown as DocumentNode<CurrentHashesQuery, CurrentHashesQueryVariables>;
export const InstructorBaseDataQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"InstructorBaseDataQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"instructor"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"givenName"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"sex"}},{"kind":"Field","name":{"kind":"Name","value":"gender"}},{"kind":"Field","name":{"kind":"Name","value":"birthDate"}},{"kind":"Field","name":{"kind":"Name","value":"street"}},{"kind":"Field","name":{"kind":"Name","value":"streetNumber"}},{"kind":"Field","name":{"kind":"Name","value":"postalCode"}},{"kind":"Field","name":{"kind":"Name","value":"city"}},{"kind":"Field","name":{"kind":"Name","value":"country"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"keycloakId"}},{"kind":"Field","name":{"kind":"Name","value":"keycloakUsername"}},{"kind":"Field","name":{"kind":"Name","value":"telephone"}},{"kind":"Field","name":{"kind":"Name","value":"pronoun"}},{"kind":"Field","name":{"kind":"Name","value":"abbreviation"}},{"kind":"Field","name":{"kind":"Name","value":"iban"}}]}}]}}]} as unknown as DocumentNode<InstructorBaseDataQueryQuery, InstructorBaseDataQueryQueryVariables>;
export const InstructorExtensionDataQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"InstructorExtensionDataQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"instructorExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"type"}}]}},{"kind":"Field","name":{"kind":"Name","value":"nutrition"}},{"kind":"Field","name":{"kind":"Name","value":"foodRestriction"}},{"kind":"Field","name":{"kind":"Name","value":"arrival"}},{"kind":"Field","name":{"kind":"Name","value":"arrivalNotes"}},{"kind":"Field","name":{"kind":"Name","value":"departure"}},{"kind":"Field","name":{"kind":"Name","value":"departureNotes"}},{"kind":"Field","name":{"kind":"Name","value":"hasContract"}},{"kind":"Field","name":{"kind":"Name","value":"contractWith"}},{"kind":"Field","name":{"kind":"Name","value":"roomNumber"}},{"kind":"Field","name":{"kind":"Name","value":"faculty"}},{"kind":"Field","name":{"kind":"Name","value":"chair"}},{"kind":"Field","name":{"kind":"Name","value":"personnelNumber"}},{"kind":"Field","name":{"kind":"Name","value":"jobTitle"}},{"kind":"Field","name":{"kind":"Name","value":"emailWork"}},{"kind":"Field","name":{"kind":"Name","value":"telephoneWork"}},{"kind":"Field","name":{"kind":"Name","value":"startOfTravel"}},{"kind":"Field","name":{"kind":"Name","value":"arrivalAtBusinessLocation"}},{"kind":"Field","name":{"kind":"Name","value":"startOfBusinessActivities"}},{"kind":"Field","name":{"kind":"Name","value":"endOfBusinessActivities"}},{"kind":"Field","name":{"kind":"Name","value":"startOfReturnJourney"}},{"kind":"Field","name":{"kind":"Name","value":"endOfTravel"}},{"kind":"Field","name":{"kind":"Name","value":"accommodationCosts"}},{"kind":"Field","name":{"kind":"Name","value":"arrivalAtBusinessLocationDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"startOfBusinessActivitiesDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"startOfReturnJourneyDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"endOfTravelDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"accommodationCostsDirectReadout"}}]}}]}}]} as unknown as DocumentNode<InstructorExtensionDataQueryQuery, InstructorExtensionDataQueryQueryVariables>;
export const UpdateInstructorExtensionMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateInstructorExtensionMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"nutrition"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"NutritionEnum"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"foodRestriction"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"arrival"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"TraveltypeEnum"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"arrivalNotes"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"departure"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"TraveltypeEnum"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"departureNotes"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"hasContract"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"contractWith"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"roomNumber"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"faculty"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"chair"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"personnelNumber"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"jobTitle"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"emailWork"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"telephoneWork"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"startOfTravel"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"LocalDateTime"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"arrivalAtBusinessLocation"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"LocalDateTime"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"startOfBusinessActivities"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"LocalDateTime"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"endOfBusinessActivities"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"LocalDateTime"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"startOfReturnJourney"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"LocalDateTime"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"endOfTravel"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"LocalDateTime"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"accommodationCosts"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Float"}},"defaultValue":{"kind":"NullValue"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateInstructorExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"nutrition"},"value":{"kind":"Variable","name":{"kind":"Name","value":"nutrition"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"foodRestriction"},"value":{"kind":"Variable","name":{"kind":"Name","value":"foodRestriction"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"arrival"},"value":{"kind":"Variable","name":{"kind":"Name","value":"arrival"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"arrivalNotes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"arrivalNotes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"departure"},"value":{"kind":"Variable","name":{"kind":"Name","value":"departure"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"departureNotes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"departureNotes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"hasContract"},"value":{"kind":"Variable","name":{"kind":"Name","value":"hasContract"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"contractWith"},"value":{"kind":"Variable","name":{"kind":"Name","value":"contractWith"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"roomNumber"},"value":{"kind":"Variable","name":{"kind":"Name","value":"roomNumber"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"faculty"},"value":{"kind":"Variable","name":{"kind":"Name","value":"faculty"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"chair"},"value":{"kind":"Variable","name":{"kind":"Name","value":"chair"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"personnelNumber"},"value":{"kind":"Variable","name":{"kind":"Name","value":"personnelNumber"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"jobTitle"},"value":{"kind":"Variable","name":{"kind":"Name","value":"jobTitle"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"emailWork"},"value":{"kind":"Variable","name":{"kind":"Name","value":"emailWork"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"telephoneWork"},"value":{"kind":"Variable","name":{"kind":"Name","value":"telephoneWork"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"startOfTravel"},"value":{"kind":"Variable","name":{"kind":"Name","value":"startOfTravel"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"arrivalAtBusinessLocation"},"value":{"kind":"Variable","name":{"kind":"Name","value":"arrivalAtBusinessLocation"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"startOfBusinessActivities"},"value":{"kind":"Variable","name":{"kind":"Name","value":"startOfBusinessActivities"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"endOfBusinessActivities"},"value":{"kind":"Variable","name":{"kind":"Name","value":"endOfBusinessActivities"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"startOfReturnJourney"},"value":{"kind":"Variable","name":{"kind":"Name","value":"startOfReturnJourney"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"endOfTravel"},"value":{"kind":"Variable","name":{"kind":"Name","value":"endOfTravel"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"accommodationCosts"},"value":{"kind":"Variable","name":{"kind":"Name","value":"accommodationCosts"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"type"}}]}},{"kind":"Field","name":{"kind":"Name","value":"nutrition"}},{"kind":"Field","name":{"kind":"Name","value":"foodRestriction"}},{"kind":"Field","name":{"kind":"Name","value":"arrival"}},{"kind":"Field","name":{"kind":"Name","value":"arrivalNotes"}},{"kind":"Field","name":{"kind":"Name","value":"departure"}},{"kind":"Field","name":{"kind":"Name","value":"departureNotes"}},{"kind":"Field","name":{"kind":"Name","value":"hasContract"}},{"kind":"Field","name":{"kind":"Name","value":"contractWith"}},{"kind":"Field","name":{"kind":"Name","value":"roomNumber"}},{"kind":"Field","name":{"kind":"Name","value":"faculty"}},{"kind":"Field","name":{"kind":"Name","value":"chair"}},{"kind":"Field","name":{"kind":"Name","value":"personnelNumber"}},{"kind":"Field","name":{"kind":"Name","value":"jobTitle"}},{"kind":"Field","name":{"kind":"Name","value":"emailWork"}},{"kind":"Field","name":{"kind":"Name","value":"telephoneWork"}},{"kind":"Field","name":{"kind":"Name","value":"startOfTravel"}},{"kind":"Field","name":{"kind":"Name","value":"arrivalAtBusinessLocation"}},{"kind":"Field","name":{"kind":"Name","value":"startOfBusinessActivities"}},{"kind":"Field","name":{"kind":"Name","value":"endOfBusinessActivities"}},{"kind":"Field","name":{"kind":"Name","value":"startOfReturnJourney"}},{"kind":"Field","name":{"kind":"Name","value":"endOfTravel"}},{"kind":"Field","name":{"kind":"Name","value":"accommodationCosts"}},{"kind":"Field","name":{"kind":"Name","value":"arrivalAtBusinessLocationDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"startOfBusinessActivitiesDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"startOfReturnJourneyDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"endOfTravelDirectReadout"}},{"kind":"Field","name":{"kind":"Name","value":"accommodationCostsDirectReadout"}}]}}]}}]} as unknown as DocumentNode<UpdateInstructorExtensionMutationMutation, UpdateInstructorExtensionMutationMutationVariables>;
export const DeleteInstructorExtensionMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"DeleteInstructorExtensionMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deleteInstructorExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<DeleteInstructorExtensionMutationMutation, DeleteInstructorExtensionMutationMutationVariables>;
export const InstructorExtensionZirkelQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"InstructorExtensionZirkelQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"instructorExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensionsPublic"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]} as unknown as DocumentNode<InstructorExtensionZirkelQueryQuery, InstructorExtensionZirkelQueryQueryVariables>;
export const LinkInstructorAndZirkelDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"LinkInstructorAndZirkel"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"setInstructorExtensionToZirkelRelation"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"zirkelId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelId"}}},{"kind":"Argument","name":{"kind":"Name","value":"instructorExtensionId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionId"}}}]}]}}]} as unknown as DocumentNode<LinkInstructorAndZirkelMutation, LinkInstructorAndZirkelMutationVariables>;
export const UnlinkInstructorAndZirkelDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UnlinkInstructorAndZirkel"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"unsetInstructorExtensionToZirkelRelation"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"zirkelId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelId"}}},{"kind":"Argument","name":{"kind":"Name","value":"instructorExtensionId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionId"}}}]}]}}]} as unknown as DocumentNode<UnlinkInstructorAndZirkelMutation, UnlinkInstructorAndZirkelMutationVariables>;
export const InstructorEventsAndZirkelQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"InstructorEventsAndZirkelQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"instructor"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"year"}}]}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"year"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]}}]}}]} as unknown as DocumentNode<InstructorEventsAndZirkelQueryQuery, InstructorEventsAndZirkelQueryQueryVariables>;
export const EventSpecificInstructorsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"EventSpecificInstructors"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"event"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensionsPublic"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"instructorId"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"gender"}},{"kind":"Field","name":{"kind":"Name","value":"roomNumber"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]}}]} as unknown as DocumentNode<EventSpecificInstructorsQuery, EventSpecificInstructorsQueryVariables>;
export const ZirkelSpecificInstructorsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ZirkelSpecificInstructors"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensionsPublic"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"instructorId"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"gender"}},{"kind":"Field","name":{"kind":"Name","value":"roomNumber"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]}}]} as unknown as DocumentNode<ZirkelSpecificInstructorsQuery, ZirkelSpecificInstructorsQueryVariables>;
export const UpdateInstructorOrgaFieldsMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateInstructorOrgaFieldsMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"roomNumber"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateInstructorExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"roomNumber"},"value":{"kind":"Variable","name":{"kind":"Name","value":"roomNumber"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]} as unknown as DocumentNode<UpdateInstructorOrgaFieldsMutationMutation, UpdateInstructorOrgaFieldsMutationMutationVariables>;
export const EventSpecificParticipantsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"EventSpecificParticipants"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"event"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"participant"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"sex"}},{"kind":"Field","name":{"kind":"Name","value":"gender"}}]}},{"kind":"Field","name":{"kind":"Name","value":"classYear"}},{"kind":"Field","name":{"kind":"Name","value":"roomNumber"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelIds"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"confirmed"}},{"kind":"Field","name":{"kind":"Name","value":"registrationTimestamp"}},{"kind":"Field","name":{"kind":"Name","value":"certificate"}},{"kind":"Field","name":{"kind":"Name","value":"participates"}},{"kind":"Field","name":{"kind":"Name","value":"emailSelf"}},{"kind":"Field","name":{"kind":"Name","value":"additionalEmails"}},{"kind":"Field","name":{"kind":"Name","value":"contacts"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"telephone"}}]}},{"kind":"Field","name":{"kind":"Name","value":"reasonOfSignoff"}},{"kind":"Field","name":{"kind":"Name","value":"timeOfSignoff"}}]}}]}}]}}]} as unknown as DocumentNode<EventSpecificParticipantsQuery, EventSpecificParticipantsQueryVariables>;
export const ZirkelSpecificParticipantsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ZirkelSpecificParticipants"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"extensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"participant"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}},{"kind":"Field","name":{"kind":"Name","value":"sex"}},{"kind":"Field","name":{"kind":"Name","value":"gender"}}]}},{"kind":"Field","name":{"kind":"Name","value":"classYear"}},{"kind":"Field","name":{"kind":"Name","value":"roomNumber"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelIds"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"confirmed"}},{"kind":"Field","name":{"kind":"Name","value":"registrationTimestamp"}},{"kind":"Field","name":{"kind":"Name","value":"certificate"}},{"kind":"Field","name":{"kind":"Name","value":"participates"}},{"kind":"Field","name":{"kind":"Name","value":"emailSelf"}},{"kind":"Field","name":{"kind":"Name","value":"additionalEmails"}},{"kind":"Field","name":{"kind":"Name","value":"contacts"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"telephone"}}]}},{"kind":"Field","name":{"kind":"Name","value":"reasonOfSignoff"}},{"kind":"Field","name":{"kind":"Name","value":"timeOfSignoff"}}]}}]}}]}}]} as unknown as DocumentNode<ZirkelSpecificParticipantsQuery, ZirkelSpecificParticipantsQueryVariables>;
export const ListOfZirkelDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ListOfZirkel"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"event_id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"event"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"event_id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]} as unknown as DocumentNode<ListOfZirkelQuery, ListOfZirkelQueryVariables>;
export const UpdateOrgaFieldsMutationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateOrgaFieldsMutation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelIds"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}}}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"roomNumber"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"confirmed"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"certificate"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"participates"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateParticipantExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"zirkelIds"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelIds"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"roomNumber"},"value":{"kind":"Variable","name":{"kind":"Name","value":"roomNumber"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"confirmed"},"value":{"kind":"Variable","name":{"kind":"Name","value":"confirmed"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"certificate"},"value":{"kind":"Variable","name":{"kind":"Name","value":"certificate"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"participates"},"value":{"kind":"Variable","name":{"kind":"Name","value":"participates"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelIds"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"roomNumber"}},{"kind":"Field","name":{"kind":"Name","value":"confirmed"}},{"kind":"Field","name":{"kind":"Name","value":"certificate"}},{"kind":"Field","name":{"kind":"Name","value":"participates"}}]}}]}}]} as unknown as DocumentNode<UpdateOrgaFieldsMutationMutation, UpdateOrgaFieldsMutationMutationVariables>;
export const ParticipantExtensionContactsQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ParticipantExtensionContactsQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"participantExtension"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"contacts"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"telephone"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}}]}}]}}]}}]} as unknown as DocumentNode<ParticipantExtensionContactsQueryQuery, ParticipantExtensionContactsQueryQueryVariables>;
export const CreateContactDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateContact"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"extensionId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"name"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"telephone"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"StringValue","value":"","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createContact"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"extensionId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"extensionId"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"name"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"telephone"},"value":{"kind":"Variable","name":{"kind":"Name","value":"telephone"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"extension"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"contacts"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}}]}}]}}]}}]}}]} as unknown as DocumentNode<CreateContactMutation, CreateContactMutationVariables>;
export const UpdateContactDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateContact"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"name"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"telephone"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateContact"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"name"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"telephone"},"value":{"kind":"Variable","name":{"kind":"Name","value":"telephone"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"extension"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"contacts"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"telephone"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}}]}}]}}]}}]}}]} as unknown as DocumentNode<UpdateContactMutation, UpdateContactMutationVariables>;
export const DeleteContactDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"DeleteContact"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deleteContact"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<DeleteContactMutation, DeleteContactMutationVariables>;
export const ZirkelPlanMetaQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ZirkelPlanMetaQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"event"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"year"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"room"}},{"kind":"Field","name":{"kind":"Name","value":"topicWishes"}},{"kind":"Field","name":{"kind":"Name","value":"numberOfParticipants"}}]}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensionsPublic"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"allEvents"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"year"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"topics"}}]}}]}}]}}]} as unknown as DocumentNode<ZirkelPlanMetaQueryQuery, ZirkelPlanMetaQueryQueryVariables>;
export const ZirkelPlanSlotsQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"ZirkelPlanSlotsQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"event"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelPlanSlots"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"startTime"}},{"kind":"Field","name":{"kind":"Name","value":"endTime"}},{"kind":"Field","name":{"kind":"Name","value":"certificate"}},{"kind":"Field","name":{"kind":"Name","value":"beamer"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelPlanEntries"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"referenceEntryId"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelPlanSlotId"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelId"}},{"kind":"Field","name":{"kind":"Name","value":"topic"}},{"kind":"Field","name":{"kind":"Name","value":"notes"}},{"kind":"Field","name":{"kind":"Name","value":"beamer"}},{"kind":"Field","name":{"kind":"Name","value":"color"}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensionsPublic"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"instructorId"}}]}}]}}]}}]}}]}}]} as unknown as DocumentNode<ZirkelPlanSlotsQueryQuery, ZirkelPlanSlotsQueryQueryVariables>;
export const CreateZirkelPlanEntryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateZirkelPlanEntry"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanSlotId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createZirkelPlanEntry"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"zirkelPlanSlotId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanSlotId"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"zirkelId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelId"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelPlanSlotId"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelId"}}]}}]}}]} as unknown as DocumentNode<CreateZirkelPlanEntryMutation, CreateZirkelPlanEntryMutationVariables>;
export const DeleteZirkelPlanEntryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"DeleteZirkelPlanEntry"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanEntryId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deleteZirkelPlanEntry"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanEntryId"}}}]}]}}]} as unknown as DocumentNode<DeleteZirkelPlanEntryMutation, DeleteZirkelPlanEntryMutationVariables>;
export const UpdateZirkelPlanEntryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateZirkelPlanEntry"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanEntryId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"beamer"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"topic"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"notes"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateZirkelPlanEntry"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanEntryId"}}},{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"topic"},"value":{"kind":"Variable","name":{"kind":"Name","value":"topic"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"beamer"},"value":{"kind":"Variable","name":{"kind":"Name","value":"beamer"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"notes"},"value":{"kind":"Variable","name":{"kind":"Name","value":"notes"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"topic"}},{"kind":"Field","name":{"kind":"Name","value":"beamer"}},{"kind":"Field","name":{"kind":"Name","value":"notes"}}]}}]}}]} as unknown as DocumentNode<UpdateZirkelPlanEntryMutation, UpdateZirkelPlanEntryMutationVariables>;
export const AddInstructorExtensionToEntryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"AddInstructorExtensionToEntry"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanEntryId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"addInstructorExtensionToZirkelPlanEntry"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"instructorExtensionId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionId"}}},{"kind":"Argument","name":{"kind":"Name","value":"zirkelPlanEntryId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanEntryId"}}}]}]}}]} as unknown as DocumentNode<AddInstructorExtensionToEntryMutation, AddInstructorExtensionToEntryMutationVariables>;
export const RemoveInstructorExtensionFromEntryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"RemoveInstructorExtensionFromEntry"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanEntryId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeInstructorExtensionFromZirkelPlanEntry"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"instructorExtensionId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"instructorExtensionId"}}},{"kind":"Argument","name":{"kind":"Name","value":"zirkelPlanEntryId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanEntryId"}}}]}]}}]} as unknown as DocumentNode<RemoveInstructorExtensionFromEntryMutation, RemoveInstructorExtensionFromEntryMutationVariables>;
export const LinkEntriesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"LinkEntries"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"sourceId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"targetId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"linkZirkelPlanEntry"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"sourceId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"sourceId"}}},{"kind":"Argument","name":{"kind":"Name","value":"targetId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"targetId"}}}]}]}}]} as unknown as DocumentNode<LinkEntriesMutation, LinkEntriesMutationVariables>;
export const UnLinkEntriesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UnLinkEntries"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"unlinkZirkelPlanEntry"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<UnLinkEntriesMutation, UnLinkEntriesMutationVariables>;
export const CreateZirkelPlanSlotDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"CreateZirkelPlanSlot"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createZirkelPlanSlot"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"endTime"},"value":{"kind":"StringValue","value":"1000-01-01 01:01:01","block":false}},{"kind":"ObjectField","name":{"kind":"Name","value":"eventId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"startTime"},"value":{"kind":"StringValue","value":"1000-01-01 01:01:01","block":false}},{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"StringValue","value":"","block":false}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"startTime"}},{"kind":"Field","name":{"kind":"Name","value":"endTime"}},{"kind":"Field","name":{"kind":"Name","value":"certificate"}},{"kind":"Field","name":{"kind":"Name","value":"beamer"}}]}}]}}]} as unknown as DocumentNode<CreateZirkelPlanSlotMutation, CreateZirkelPlanSlotMutationVariables>;
export const DeleteZirkelPlanSlotDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"DeleteZirkelPlanSlot"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanSlotId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deleteZirkelPlanSlot"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelPlanSlotId"}}}]}]}}]} as unknown as DocumentNode<DeleteZirkelPlanSlotMutation, DeleteZirkelPlanSlotMutationVariables>;
export const UpdateZirkelPlanSlotDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateZirkelPlanSlot"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"beamer"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"certificate"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"endTime"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"LocalDateTime"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"name"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"defaultValue":{"kind":"NullValue"}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"startTime"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"LocalDateTime"}},"defaultValue":{"kind":"NullValue"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateZirkelPlanSlot"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"values"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"beamer"},"value":{"kind":"Variable","name":{"kind":"Name","value":"beamer"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"certificate"},"value":{"kind":"Variable","name":{"kind":"Name","value":"certificate"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"endTime"},"value":{"kind":"Variable","name":{"kind":"Name","value":"endTime"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"eventId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"name"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"startTime"},"value":{"kind":"Variable","name":{"kind":"Name","value":"startTime"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"startTime"}},{"kind":"Field","name":{"kind":"Name","value":"endTime"}},{"kind":"Field","name":{"kind":"Name","value":"certificate"}},{"kind":"Field","name":{"kind":"Name","value":"beamer"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelPlanEntries"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"referenceEntryId"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelPlanSlotId"}},{"kind":"Field","name":{"kind":"Name","value":"zirkelId"}},{"kind":"Field","name":{"kind":"Name","value":"topic"}},{"kind":"Field","name":{"kind":"Name","value":"notes"}},{"kind":"Field","name":{"kind":"Name","value":"beamer"}},{"kind":"Field","name":{"kind":"Name","value":"color"}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensionsPublic"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"instructorId"}}]}}]}}]}}]}}]} as unknown as DocumentNode<UpdateZirkelPlanSlotMutation, UpdateZirkelPlanSlotMutationVariables>;
export const ZirkelDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"Zirkel"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"instructorExtensions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"instructor"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"room"}},{"kind":"Field","name":{"kind":"Name","value":"topics"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"type"}}]}}]}}]}}]} as unknown as DocumentNode<ZirkelQuery, ZirkelQueryVariables>;
export const EventDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"Event"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"event"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"startDate"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"year"}},{"kind":"Field","name":{"kind":"Name","value":"endDate"}},{"kind":"Field","name":{"kind":"Name","value":"defaultFee"}},{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"statistics"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"giveInstructorsFullReadAccess"}}]}}]}}]} as unknown as DocumentNode<EventQuery, EventQueryVariables>;
export const NamesSelectionDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"NamesSelection"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"zirkelId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"participantId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"instructorId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Uuid"}},"defaultValue":{"kind":"StringValue","value":"00000000-0000-0000-0000-000000000000","block":false}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"zirkel"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"zirkelId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"event"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"year"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"event"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"eventId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"year"}}]}},{"kind":"Field","name":{"kind":"Name","value":"participant"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"participantId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}}]}},{"kind":"Field","name":{"kind":"Name","value":"instructor"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"instructorId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"callName"}},{"kind":"Field","name":{"kind":"Name","value":"familyName"}}]}}]}}]} as unknown as DocumentNode<NamesSelectionQuery, NamesSelectionQueryVariables>;