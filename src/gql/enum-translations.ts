import { ClassyearEnum, EventtypeEnum, NutritionEnum, SexEnum, TraveltypeEnum, RoomEnum } from "./graphql";

// ! Keep in sync with backend: https://gitlab.com/mathezirkel/mathezirkel-app/backend/-/blob/main/src/models/enums.rs

export function ClassyearEnumTranslation(val: ClassyearEnum) {
    switch (val) {
        case ClassyearEnum.Class5:
            return "5. Klasse";
        case ClassyearEnum.Class6:
            return "6. Klasse";
        case ClassyearEnum.Class7:
            return "7. Klasse";
        case ClassyearEnum.Class8:
            return "8. Klasse";
        case ClassyearEnum.Class9:
            return "9. Klasse";
        case ClassyearEnum.Class10:
            return "10. Klasse";
        case ClassyearEnum.Class11:
            return "11. Klasse";
        case ClassyearEnum.Class12:
            return "12. Klasse";
        case ClassyearEnum.Class13:
            return "13. Klasse";
        case ClassyearEnum.ClassInfo:
            return "Info-Only";
        default:
            return "Translation Not Implemented";
    }
}

export function EventtypeEnumTranslation(val: EventtypeEnum) {
    switch (val) {
        case EventtypeEnum.Zirkel:
            return "Zirkel";
        case EventtypeEnum.MathDay:
            return "Mathe-Tag";
        case EventtypeEnum.WinterCamp:
            return "Wintercamp";
        case EventtypeEnum.MathCamp:
            return "Mathecamp";
        case EventtypeEnum.PreparationWeekend:
            return "Vorbereitungswochenende";
        case EventtypeEnum.Info:
            return "Nur Infonachrichten";
        default:
            return "Translation Not Implemented";
    }
}

export function NutritionEnumTranslation(val: NutritionEnum) {
    switch (val) {
        case NutritionEnum.Omnivore:
            return "fleischhaltig";
        case NutritionEnum.Vegetarian:
            return "vegetarisch";
        case NutritionEnum.Vegan:
            return "vegan";
        case NutritionEnum.Redacted:
            return "redacted";
        default:
            return "Translation Not Implemented";
    }
}

export function SexEnumTranslation(val: SexEnum) {
    switch (val) {
        case SexEnum.Male:
            return "männlich";
        case SexEnum.Female:
            return "weiblich";
        case SexEnum.Diverse:
            return "divers";
        case SexEnum.Redacted:
            return "redacted";
        default:
            return "Translation Not Implemented";
    }
}

export function TraveltypeEnumTranslation(val: TraveltypeEnum) {
    switch (val) {
        case TraveltypeEnum.Private:
            return "Privat (Auto, Kamel,...)";
        case TraveltypeEnum.Bus:
            return "Mathezirkel Bus";
        case TraveltypeEnum.Bicycle:
            return "Fahrrad";
        default:
            return "Translation Not Implemented";
    }
}

export function RoomEnumTranslation(val: RoomEnum) {
    switch (val) {
        case RoomEnum.Talk:
            return "Vortrag";
        case RoomEnum.Sport:
            return "Sport";
        case RoomEnum.Crafts:
            return "Werken";
        case RoomEnum.Programming:
            return "Programmieren";
        case RoomEnum.Outside:
            return "Draußen";
        case RoomEnum.Meetingpoint:
            return "Treffpunkt";
        case RoomEnum.Inside:
            return "Drinnen";
        case RoomEnum.Music:
            return "Musik";
        default:
            return "Translation Not Implemented";
    }
}
