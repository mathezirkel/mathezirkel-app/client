import useNavigationStore from "./../stores/navigation";
import { graphql } from "./../gql/index";
import { computed } from "vue";
import { useQuery } from "@vue/apollo-composable";
import { EventtypeEnum } from "../gql/graphql";

export function useDeps() {
    const navigationStore = useNavigationStore();

    // specific zirkel query
    const zirkelQueryVariables = computed(() => {
        return {
            id: navigationStore.zirkelId,
        };
    });
    const zirkelQuery = graphql(`
        query Zirkel($id: Uuid = "00000000-0000-0000-0000-000000000000") {
            zirkel(id: $id) {
                id
                instructorExtensions {
                    id
                    instructor {
                        id
                        callName
                        familyName
                    }
                }
                name
                room
                topics
                event {
                    id
                    type
                }
            }
        }
    `);
    const { result: zirkelResult } = useQuery(zirkelQuery, zirkelQueryVariables, {
        fetchPolicy: "cache-and-network",
    });
    const zirkel = computed(() => {
        return zirkelResult.value?.zirkel!;
    });

    // Compute the event, may be direct or come from the zirkel
    const logicalEventId = computed(() => {
        if (navigationStore.eventId != "00000000-0000-0000-0000-000000000000") {
            return navigationStore.eventId;
        }

        if (zirkel.value && zirkel.value != undefined && zirkel.value != null) {
            return zirkel.value.event?.id ?? "00000000-0000-0000-0000-000000000000";
        }

        return "00000000-0000-0000-0000-000000000000";
    });

    // specific event query
    const eventQueryVariables = computed(() => {
        return {
            id: logicalEventId.value,
        };
    });
    const eventQuery = graphql(`
        query Event($id: Uuid = "00000000-0000-0000-0000-000000000000") {
            event(id: $id) {
                id
                name
                startDate
                type
                year
                endDate
                defaultFee
                zirkel {
                    id
                    name
                }
                statistics
                type
                giveInstructorsFullReadAccess
            }
        }
    `);
    const { result: eventResult } = useQuery(eventQuery, eventQueryVariables, {
        fetchPolicy: "cache-and-network",
    });
    const event = computed(() => {
        return eventResult.value?.event!;
    });

    const isVBW = computed(() => {
        if (zirkel.value && zirkel.value != undefined && zirkel.value != null) {
            return (zirkel.value.event?.type ?? EventtypeEnum.MathCamp) == EventtypeEnum.PreparationWeekend;
        }

        if (event.value && event.value != undefined && event.value != null) {
            return event.value.type == EventtypeEnum.PreparationWeekend;
        }

        return false;
    });

    const isInfo = computed(() => {
        if (zirkel.value && zirkel.value != undefined && zirkel.value != null) {
            return (zirkel.value.event?.type ?? EventtypeEnum.MathCamp) == EventtypeEnum.Info;
        }

        if (event.value && event.value != undefined && event.value != null) {
            return event.value.type == EventtypeEnum.Info;
        }

        return false;
    });

    return { event, zirkel, logicalEventId, isVBW, isInfo };
}
