import { EventtypeEnum } from "../../gql/graphql";

export function fieldNeededLogic(extensionType: EventtypeEnum | undefined, comparisonType: EventtypeEnum) {
    switch (comparisonType) {
        case EventtypeEnum.Zirkel:
            return (
                extensionType == EventtypeEnum.Zirkel ||
                extensionType == EventtypeEnum.MathDay ||
                extensionType == EventtypeEnum.WinterCamp ||
                extensionType == EventtypeEnum.MathCamp ||
                extensionType == EventtypeEnum.PreparationWeekend
            );
        case EventtypeEnum.MathDay:
            return (
                extensionType == EventtypeEnum.MathDay ||
                extensionType == EventtypeEnum.WinterCamp ||
                extensionType == EventtypeEnum.MathCamp ||
                extensionType == EventtypeEnum.PreparationWeekend
            );
        case EventtypeEnum.WinterCamp:
            return (
                extensionType == EventtypeEnum.WinterCamp ||
                extensionType == EventtypeEnum.MathCamp ||
                extensionType == EventtypeEnum.PreparationWeekend
            );
        case EventtypeEnum.MathCamp:
            return extensionType == EventtypeEnum.MathCamp || extensionType == EventtypeEnum.PreparationWeekend;
        default:
            return false;
    }
}
