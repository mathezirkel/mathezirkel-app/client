import useNavigationStore from "../stores/navigation";
import { graphql } from "../gql/index";
import { computed } from "vue";
import { useQuery } from "@vue/apollo-composable";

export function useDeps() {
    const navigationStore = useNavigationStore();

    const eventId = computed(() => {
        return navigationStore.eventId ?? "00000000-0000-0000-0000-000000000000";
    });
    const zirkelId = computed(() => {
        return navigationStore.zirkelId ?? "00000000-0000-0000-0000-000000000000";
    });
    const participantId = computed(() => {
        return navigationStore.participantId ?? "00000000-0000-0000-0000-000000000000";
    });
    const instructorId = computed(() => {
        return navigationStore.instructorId ?? "00000000-0000-0000-0000-000000000000";
    });

    // specific zirkel query
    const namesSelectionQueryVariables = computed(() => {
        return {
            eventId: eventId.value,
            zirkelId: zirkelId.value,
            participantId: participantId.value,
            instructorId: instructorId.value,
        };
    });
    const namesSelectionQuery = graphql(`
        query NamesSelection(
            $zirkelId: Uuid = "00000000-0000-0000-0000-000000000000"
            $eventId: Uuid = "00000000-0000-0000-0000-000000000000"
            $participantId: Uuid = "00000000-0000-0000-0000-000000000000"
            $instructorId: Uuid = "00000000-0000-0000-0000-000000000000"
        ) {
            zirkel(id: $zirkelId) {
                id
                name
                event {
                    id
                    name
                    year
                }
            }
            event(id: $eventId) {
                id
                name
                year
            }
            participant(id: $participantId) {
                id
                callName
                familyName
            }
            instructor(id: $instructorId) {
                id
                callName
                familyName
            }
        }
    `);
    const { result: namesSelectionQueryResult } = useQuery(namesSelectionQuery, namesSelectionQueryVariables, {
        fetchPolicy: "cache-and-network",
    });

    const instructorName = computed(() => {
        const first = namesSelectionQueryResult.value?.instructor?.callName ?? null;
        const last = namesSelectionQueryResult.value?.instructor?.familyName ?? null;

        if (first == null || last == null) {
            return "Zirkelleitersuche";
        }

        return "Suche: " + first + " " + last.charAt(0) + ".";
    });

    const instructorSelected = computed(() => {
        return instructorId.value != "00000000-0000-0000-0000-000000000000";
    });

    const participantName = computed(() => {
        const first = namesSelectionQueryResult.value?.participant?.callName ?? null;
        const last = namesSelectionQueryResult.value?.participant?.familyName ?? null;

        if (first == null || last == null) {
            return "Teilnehmersuche";
        }

        return "Suche: " + first + " " + last.charAt(0) + ".";
    });

    const participantSelected = computed(() => {
        return participantId.value != "00000000-0000-0000-0000-000000000000";
    });

    const proceedingName = computed(() => {
        const zirkelName = namesSelectionQueryResult.value?.zirkel?.name ?? null;
        let eventName =
            (namesSelectionQueryResult.value?.event?.name ?? "") +
            ", " +
            String(namesSelectionQueryResult.value?.event?.year ?? "");

        if (zirkelName != null) {
            eventName =
                (namesSelectionQueryResult.value?.zirkel?.event?.name ?? "") +
                ", " +
                (String(namesSelectionQueryResult.value?.zirkel?.event?.year) ?? "");

            return "Zirkel: " + (zirkelName ?? "") + " (" + eventName + ")";
        }

        if (eventName.length < 4) {
            return "Veranstaltung wählen";
        }

        return "Event: " + eventName;
    });

    const proceedingSelected = computed(() => {
        return (
            eventId.value != "00000000-0000-0000-0000-000000000000" || zirkelId.value != "00000000-0000-0000-0000-000000000000"
        );
    });

    return {
        proceedingName,
        proceedingSelected,
        instructorName,
        participantName,
        instructorSelected,
        participantSelected,
    };
}
