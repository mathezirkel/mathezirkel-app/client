export function participantQueryString(participant: any | null | undefined, extension: any | null | undefined): string {
    let workOnExtensions = [] as any[];

    // TODO this will become possibly VERY slow if there are too many participants in the database

    if (
        participant != null &&
        participant != undefined &&
        participant.extensions != undefined &&
        Array.isArray(participant.extensions)
    ) {
        workOnExtensions.push(...participant.extensions);
    }
    if (extension != null && extension != undefined) {
        workOnExtensions.push(extension);
    }

    let queryString = "";
    if (participant.sex != null && participant.sex != undefined) {
        queryString +=
            "%%sex=" +
            String(participant.sex ?? "")
                .replace("FEMALE", "w")
                .replace("MALE", "m")
                .replace("DIVERSE", "d")
                .replace("REDACTED", "r");
    }

    workOnExtensions.forEach((ext: any) => {
        if (ext.emailSelf != null && ext.emailSelf != undefined) {
            queryString += "%%mail=" + (ext.emailSelf ?? "");
        }
        (ext.additionalEmails ?? []).forEach((mail: any) => {
            queryString += "%%mail=" + (mail ?? "");
        });
        (ext.contacts ?? []).forEach((contact: any) => {
            if (contact.telephone != null && contact.telephone != undefined) {
                queryString += "%%tel=" + (contact.telephone ?? "");
            }
        });
        if (ext.classYear != null && ext.classYear != undefined) {
            queryString +=
                "%%class=" +
                String(ext.classYear ?? "")
                    .replace("CLASS_", "")
                    .replace("CLASS", "");
        }
    });

    return queryString;
}

export const tooltipText =
    "Filter nach Freitext-Eingabe. Erlaubt außerdem gezieltes Filtern nach Attributen: sex=m ; sex=w ; sex=d ; sex=r ; mail=xxxxxx ; tel=xxxxxx ; class=5 ; class=... ; class=13 ; class=info (Aktuell nur ein Filter auf einmal unterstützt)";
