import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import vuetify from "vite-plugin-vuetify";
import ViteWebfontDownload from "vite-plugin-webfont-dl";
import checker from "vite-plugin-checker";
import packageConfFile from "./package.json";
import path from "path";
import { NodePackageImporter } from "sass-embedded";

// https://vitejs.dev/config/
export default defineConfig((props) => {
    let env: Record<string, string | boolean> = loadEnv(props.mode, process.cwd(), "VITE_");
    env.PACKAGE_APP_VERSION = String(packageConfFile.version);

    const hostOnGithubPages = false;
    let webBaseString = "/";
    if (hostOnGithubPages) {
        webBaseString = "/app-building-releases/";
    }

    const serviceworkerVersion = packageConfFile["sw-version"];
    const serviceWorkerFilename = "serviceworker-v" + String(serviceworkerVersion) + ".js";

    env.VITE_BASE_STRING = String(process.env.VITE_BUILD_PWA ?? false ? webBaseString : "/");
    env.VITE_SERVICEWORKER_FILENAME = serviceWorkerFilename;
    env.DEV = props.mode != "production";
    env.PROD = props.mode == "production";
    const envWithProcessPrefix = {
        "process.env": `${JSON.stringify(env)}`,
    };

    return {
        build: {
            target: "es2022",
            rollupOptions: {
                input: {
                    main: path.resolve(__dirname, "index.html"),
                    serviceworker: path.resolve(__dirname, "dist-sw/serviceworker.js"),
                },
                output: {
                    entryFileNames: (assetInfo) => {
                        if (assetInfo.name.includes("serviceworker")) {
                            return serviceWorkerFilename;
                        }
                        return "assets/[name]-[hash].js";
                    },
                },
            },
        },
        base: env.VITE_BASE_STRING,
        define: envWithProcessPrefix,
        plugins: [
            vue(),
            vuetify(),
            ViteWebfontDownload(
                [
                    "https://fonts.googleapis.com/css?family=Nunito",
                    "https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0",
                ],
                { injectAsStyleTag: true }
            ),
            checker({
                typescript: true,
            }),
        ],
        clearScreen: false,
        // expects a fixed port, fail if that port is not available
        server: {
            port: 1420,
            strictPort: true,
        },
        envPrefix: ["VITE_"],
        css: {
            preprocessorOptions: {
                scss: {
                    api: "modern-compiler", // or "modern", "legacy"
                    importers: [new NodePackageImporter()],
                },
            },
        },
    };
});
