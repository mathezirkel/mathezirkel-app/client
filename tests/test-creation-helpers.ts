import { graphqlQuery } from "./test-graphql-interface";

export function nullWrapperHelper(inp: string | null) {
    if (inp == null) {
        return null;
    }
    return '"' + inp + '"';
}

export async function createEvent(from: string, to: string): Promise<string> {
    return (
        await graphqlQuery(`
        mutation {
            createEvent(
                values: {type: WINTER_CAMP, year: 2023, name: "Test Event", defaultFee: 1000, endDate: "${to}", startDate: "${from}"}
            ) {
                id
            }
        }
        `)
    ).createEvent.id;
}

export async function createParticipant(sex: "MALE" | "FEMALE" | "DIVERSE" | "REDACTED"): Promise<string> {
    return (
        await graphqlQuery(`
        mutation {
            createParticipant(
                values: {streetNumber: "TEST", street: "TEST", sex: ${sex}, postalCode: "88888", notes: "", familyName: "TEST", country: "TEST", city: "TEST", birthDate: "2022-01-01", givenName: "TEST"}
            ) {
                id
            }
        }
        `)
    ).createParticipant.id;
}

export async function createParticipantExtension(
    participantId: string,
    eventId: string,
    confirmed: boolean,
    classYear:
        | "CLASS5"
        | "CLASS6"
        | "CLASS7"
        | "CLASS8"
        | "CLASS9"
        | "CLASS10"
        | "CLASS11"
        | "CLASS12"
        | "CLASS13"
        | "CLASS_INFO",
    timeOfSignoff: string | null
): Promise<string> {
    return (
        await graphqlQuery(`
        mutation {
            createParticipantExtension(
                values: {participantId: "${participantId}", eventId: "${eventId}", confirmed: ${confirmed}, classYear: ${classYear}, timeOfSignoff: ${nullWrapperHelper(
            timeOfSignoff
        )}}
            ) {
                id
            }
        }
        `)
    ).createParticipantExtension.id;
}

export async function updateParticipantExtension(
    participantExtensionId: string,
    confirmed: boolean,
    classYear: "CLASS5" | "CLASS6" | "CLASS7" | "CLASS8" | "CLASS9" | "CLASS10" | "CLASS11" | "CLASS12" | "CLASS13" | "CLASS_INFO"
): Promise<string> {
    return (
        await graphqlQuery(`
        mutation MyMutation {
            updateParticipantExtension(
                id: "${participantExtensionId}"
                values: {confirmed: ${confirmed}, classYear: ${classYear}}
            ) {
                id
            }
        }
        `)
    ).updateParticipantExtension.id;
}

export async function deleteParticipantExtension(participantExtensionId: string): Promise<void> {
    return await graphqlQuery(`
        mutation MyMutation {
            deleteParticipantExtension(
                id: "${participantExtensionId}"
            )
        }
        `);
}
