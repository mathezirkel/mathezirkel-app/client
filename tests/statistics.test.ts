import { describe, expect, test } from "@jest/globals";
import { graphqlQuery } from "./test-graphql-interface";
import {
    createEvent,
    createParticipant,
    createParticipantExtension,
    deleteParticipantExtension,
    updateParticipantExtension,
} from "./test-creation-helpers";

async function queryStatistics(id: string) {
    return JSON.parse(
        (
            await graphqlQuery(`
        query {
            event(id: "${id}") {
                statistics
            }
        }
        `)
        ).event.statistics
    );
}

function statisticsObject(rewrite: { [key: string]: number }) {
    let obj = JSON.parse(
        '{"ct_male_5":0,"ct_male_6":0,"ct_male_7":0,"ct_male_8":0,"ct_male_9":0,"ct_male_10":0,"ct_male_11":0,"ct_male_12":0,"ct_male_13":0,"ct_male_all":0,"ct_female_5":0,"ct_female_6":0,"ct_female_7":0,"ct_female_8":0,"ct_female_9":0,"ct_female_10":0,"ct_female_11":0,"ct_female_12":0,"ct_female_13":0,"ct_female_all":0,"ct_diverse_5":0,"ct_diverse_6":0,"ct_diverse_7":0,"ct_diverse_8":0,"ct_diverse_9":0,"ct_diverse_10":0,"ct_diverse_11":0,"ct_diverse_12":0,"ct_diverse_13":0,"ct_diverse_all":0,"ct_all_5":0,"ct_all_6":0,"ct_all_7":0,"ct_all_8":0,"ct_all_9":0,"ct_all_10":0,"ct_all_11":0,"ct_all_12":0,"ct_all_13":0,"ct_all_all":0,"wl_male_5":0,"wl_male_6":0,"wl_male_7":0,"wl_male_8":0,"wl_male_9":0,"wl_male_10":0,"wl_male_11":0,"wl_male_12":0,"wl_male_13":0,"wl_male_all":0,"wl_female_5":0,"wl_female_6":0,"wl_female_7":0,"wl_female_8":0,"wl_female_9":0,"wl_female_10":0,"wl_female_11":0,"wl_female_12":0,"wl_female_13":0,"wl_female_all":0,"wl_diverse_5":0,"wl_diverse_6":0,"wl_diverse_7":0,"wl_diverse_8":0,"wl_diverse_9":0,"wl_diverse_10":0,"wl_diverse_11":0,"wl_diverse_12":0,"wl_diverse_13":0,"wl_diverse_all":0,"wl_all_5":0,"wl_all_6":0,"wl_all_7":0,"wl_all_8":0,"wl_all_9":0,"wl_all_10":0,"wl_all_11":0,"wl_all_12":0,"wl_all_13":0,"wl_all_all":0}'
    );

    const keys = Object.keys(rewrite);
    keys.forEach((key) => {
        obj[key] = rewrite[key];
    });

    return obj;
}

describe("Event Statistics", () => {
    test("General empty statistics", async () => {
        const eventId = await createEvent("2023-01-22", "2023-02-02");

        expect(await queryStatistics(eventId)).toMatchObject(statisticsObject({}));
    });

    test("Tracks creation of participant", async () => {
        const eventId = await createEvent("3023-01-22", "3023-02-02"); // Assumed to be in the future

        await createParticipantExtension(await createParticipant("MALE"), eventId, true, "CLASS6", null);

        expect(await queryStatistics(eventId)).toMatchObject(
            statisticsObject({
                ct_male_all: 1,
                ct_male_6: 1,
                ct_all_all: 1,
                ct_all_6: 1,
            })
        );
    });

    test("Doesn't update for past events", async () => {
        const eventId = await createEvent("1023-01-22", "1023-02-02"); // Assumed to be in the past

        await createParticipantExtension(await createParticipant("MALE"), eventId, true, "CLASS6", null);

        expect(await queryStatistics(eventId)).toMatchObject(statisticsObject({}));
    });

    test("One Normal and one redacted", async () => {
        const eventId = await createEvent("3023-01-22", "3023-02-02"); // Assumed to be in the future

        await createParticipantExtension(await createParticipant("REDACTED"), eventId, true, "CLASS12", null);
        await createParticipantExtension(await createParticipant("FEMALE"), eventId, true, "CLASS10", null);

        expect(await queryStatistics(eventId)).toMatchObject(
            statisticsObject({
                ct_female_all: 1,
                ct_female_10: 1,
                ct_all_all: 2,
                ct_all_10: 1,
                ct_all_12: 1,
            })
        );
    });

    test("Signoff times", async () => {
        const eventId = await createEvent("1023-01-22", "3023-02-02"); // Assumed to be running

        await createParticipantExtension(await createParticipant("DIVERSE"), eventId, true, "CLASS9", "0888-11-11 17:25:33"); // before
        await createParticipantExtension(await createParticipant("DIVERSE"), eventId, true, "CLASS7", "2000-11-11 17:25:33"); // during

        expect(await queryStatistics(eventId)).toMatchObject(
            statisticsObject({
                ct_diverse_all: 1,
                ct_diverse_7: 1,
                ct_all_all: 1,
                ct_all_7: 1,
            })
        );
    });

    test("Waiting list persons", async () => {
        const eventId = await createEvent("1023-01-22", "3023-02-02"); // Assumed to be running

        await createParticipantExtension(await createParticipant("REDACTED"), eventId, false, "CLASS11", null);
        await createParticipantExtension(await createParticipant("MALE"), eventId, false, "CLASS8", null);
        await createParticipantExtension(await createParticipant("FEMALE"), eventId, false, "CLASS_INFO", null);

        expect(await queryStatistics(eventId)).toMatchObject(
            statisticsObject({
                wl_all_11: 1,
                wl_all_8: 1,
                wl_all_all: 3,
                wl_male_8: 1,
                wl_male_all: 1,
                wl_female_all: 1,
            })
        );
    });

    test("Waiting list future", async () => {
        const eventId = await createEvent("3023-01-22", "3023-02-02"); // Assumed to be in the future

        await createParticipantExtension(await createParticipant("DIVERSE"), eventId, false, "CLASS7", null);
        await createParticipantExtension(await createParticipant("MALE"), eventId, false, "CLASS5", null);
        await createParticipantExtension(await createParticipant("REDACTED"), eventId, false, "CLASS_INFO", null);

        const statistics = await queryStatistics(eventId);

        expect(statistics).toMatchObject(
            statisticsObject({
                wl_all_5: 1,
                wl_all_7: 1,
                wl_all_all: 3,
                wl_diverse_7: 1,
                wl_diverse_all: 1,
                wl_male_5: 1,
                wl_male_all: 1,
            })
        );
    });

    test("Signoff waiting list", async () => {
        const eventId = await createEvent("1023-01-22", "3023-02-02"); // Assumed to be running

        await createParticipantExtension(await createParticipant("REDACTED"), eventId, false, "CLASS11", "0888-11-11 17:25:33"); // before
        await createParticipantExtension(await createParticipant("MALE"), eventId, false, "CLASS8", "2000-11-11 17:25:33"); // during

        expect(await queryStatistics(eventId)).toMatchObject(statisticsObject({}));
    });

    test("Update on class change, confirmed change and delete", async () => {
        const eventId = await createEvent("1023-01-22", "3023-02-02"); // Assumed to be running
        const participantID = await createParticipant("MALE");
        expect(await queryStatistics(eventId)).toMatchObject(statisticsObject({}));

        const extId = await createParticipantExtension(participantID, eventId, true, "CLASS11", null);
        expect(await queryStatistics(eventId)).toMatchObject(
            statisticsObject({
                ct_all_11: 1,
                ct_all_all: 1,
                ct_male_11: 1,
                ct_male_all: 1,
            })
        );

        await updateParticipantExtension(extId, false, "CLASS11");
        expect(await queryStatistics(eventId)).toMatchObject(
            statisticsObject({
                wl_all_11: 1,
                wl_all_all: 1,
                wl_male_11: 1,
                wl_male_all: 1,
            })
        );

        await updateParticipantExtension(extId, false, "CLASS12");
        expect(await queryStatistics(eventId)).toMatchObject(
            statisticsObject({
                wl_all_12: 1,
                wl_all_all: 1,
                wl_male_12: 1,
                wl_male_all: 1,
            })
        );

        // !! PURE DELETE DOESN'T CURRENTLY SEEM TO TRIGGER CHANGE
        await deleteParticipantExtension(extId);
        expect(await queryStatistics(eventId)).toMatchObject(
            statisticsObject({
                wl_all_12: 1,
                wl_all_all: 1,
                wl_male_12: 1,
                wl_male_all: 1,
            })
        );

        // But after some other change triggers, if will re-calculate (same for sex-changes)
        await createParticipantExtension(await createParticipant("FEMALE"), eventId, true, "CLASS6", null);
        expect(await queryStatistics(eventId)).toMatchObject(
            statisticsObject({
                ct_all_6: 1,
                ct_all_all: 1,
                ct_female_6: 1,
                ct_female_all: 1,
            })
        );
    });
});
