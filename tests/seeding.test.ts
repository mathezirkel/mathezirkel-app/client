import { describe, test } from "@jest/globals";
import { graphqlQuery } from "./test-graphql-interface";

async function createStuff(from: string, to: string, prefix: string) {
    // events
    const types = ["ZIRKEL", "MATH_DAY", "WINTER_CAMP", "MATH_CAMP", "PREPARATION_WEEKEND", "INFO"];
    for (const event_type_index in types) {
        const event_type = types[event_type_index];
        const name = prefix + " " + event_type;

        const event_id = (
            await graphqlQuery(`
                mutation CreateEventMutation($endDate: Date = "${to}", $startDate: Date = "${from}", $type: EventtypeEnum = ${event_type},
                                             $year: Int = ${from.split("-")[0]}, $name: String = "${name}") {
                    createEvent(
                        values: {type: $type, year: $year, name: $name, startDate: $startDate, endDate: $endDate, defaultFee: 10}
                    ) {
                        id
                    }
                }
            `)
        ).createEvent.id;

        const zirkel_pres = ["a in ", "b in "];
        for (const zirkel_pre_index in zirkel_pres) {
            const zirkel_pre = zirkel_pres[zirkel_pre_index];
            const zirkel_id = (
                await graphqlQuery(`
                    mutation CreateZirkelMutation($name: String = "${zirkel_pre + name}", $eventId: Uuid = "${event_id}") {
                        createZirkel(values: {eventId: $eventId, name: $name}) {
                            id
                        }
                    }
                `)
            ).createZirkel.id;
            zirkel_id;
        }
    }
}

async function createParticipants() {
    // participants
    for (const i in [0, 1, 2, 3]) {
        const name = "person" + i;
        const last_name = "nachname " + i;
        const sex = {
            0: "MALE",
            1: "FEMALE",
            2: "DIVERSE",
            3: "REDACTED",
        }[i];

        const participant_id = (
            await graphqlQuery(`
                mutation CreateParticipantMutation($givenName: String = "${name}", $familyName: String = "${last_name}", $sex: SexEnum = ${sex}) {
                    createParticipant(
                        values: {familyName: $familyName, givenName: $givenName, birthDate: "0010-01-01", sex: $sex, street: "Teststraße", streetNumber: "000", postalCode: "08080", city: "Teststadt", country: "Testland"}
                    ) {
                        id
                    }
                }              
            `)
        ).createParticipant.id;
        participant_id;
    }
}

describe("Seed the application with a useful set of content", () => {
    test("Seed Past Set", async () => {
        await createStuff("2019-01-01", "2019-02-01", "Past");
    });

    test("Seed Ongoing Set", async () => {
        await createStuff("2020-01-01", "3020-02-01", "Ongoing");
    });

    test("Seed Future Set", async () => {
        await createStuff("3020-01-01", "3020-02-01", "Future");
    });

    test("Seed Participants", async () => {
        await createParticipants();
    });
});
