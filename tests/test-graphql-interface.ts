import axios from "axios";

// testing access to axios
// rather duplicate the axios settings code, than mocking es-modules (because NOTHING works properly in jest)
const axiosTestingInstance = axios.create();
axiosTestingInstance.interceptors.request.use(
    async (config) => {
        const auth = `Bearer development_token`;

        config.baseURL = "http://localhost:8080/";
        config.headers["Authorization"] = auth;
        return config;
    },
    (error) => Promise.reject(error)
);

export async function graphqlQuery(query: string): Promise<any> {
    let res = await axiosTestingInstance
        .post("graphql", {
            query: query,
        })
        .catch((error) => {
            console.error(error);
        });

    return (res as any).data.data;
}
